import { config } from '@vue/test-utils';
import { beforeAll } from 'vitest';

const DataTestIdPlugin = (wrapper) => {
  function findByTestId(testId) {
    return wrapper.find(`[data-test-id="${testId}"]`);
  }

  return {
    findByTestId,
  };
};

beforeAll(() => {
  config.plugins.VueWrapper.install(DataTestIdPlugin);
});
