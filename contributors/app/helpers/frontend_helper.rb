# frozen_string_literal: true

module FrontendHelper
  delegate :contributor_level, :points, to: :user

  def vue_data
    {
      contributorLevel: contributor_level,
      contributorPoints: points,
      hasDiscordMessages: discord_messages?,
      userAdmin: user_admin?,
      userId: user_id,
      username:
    }
  end

  def user_admin?
    !!session[:admin]
  end

  def username
    session[:username]
  end

  def user_id
    session[:user_id]
  end

  def discord_messages?
    user_id.present? && DiscordMessage.exists?(user_id:)
  end

  private

  def user
    @user ||= User.find_by(id: user_id) || User.new
  end
end
