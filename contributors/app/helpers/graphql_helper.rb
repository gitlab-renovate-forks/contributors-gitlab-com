# frozen_string_literal: true

module GraphqlHelper
  def id_from_graphql_id(graphql_id)
    graphql_id.split('/').last.to_i
  end
end
