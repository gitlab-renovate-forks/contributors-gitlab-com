# frozen_string_literal: true

module Api
  module V1
    class ManageMergeRequestController < ApiApplicationController
      include SessionUserConcern

      before_action :ensure_logged_in

      def index
        render json: {
          authorized: authorized?,
          randomAvailableCoach: TeamMember.random_available_coach
        }
      end

      def request_review
        message = <<~MESSAGE
          Hey @#{username}, thanks for requesting a review!
          #{
            if include_duo?
              "\nFeel free to address any feedback by @GitLabDuo, or wait for another reviewer to validate it first!\n"
            end
          }
          Hey #{reviewer_mentions} :wave:

          - Do you have capacity and domain expertise to review this? If not, find one or more [reviewers](https://gitlab-org.gitlab.io/gitlab-roulette/) and assign to them.
          - If you've reviewed it, add the ~"workflow::in dev" label if these changes need more work before the next review.
          - Please ensure the group's Product Manager has validated the linked issue.

          /ready
          /label ~"workflow::ready for review"
          /request_review #{reviewer_mentions}
        MESSAGE
        response = gitlab_rest_service.start_thread(
          project_id,
          merge_request_iid,
          'merge_requests',
          message
        )

        if response.code == 201
          head :ok
        else
          head :internal_server_error
        end
      end

      private

      def gitlab_rest_service
        @gitlab_rest_service ||= GitlabRestService.new(ENV.fetch('GITLAB_BOT_API_TOKEN'))
      end

      def authorized?
        # We also allow author, assignee, or reviewer but make a GraphQL call from the UI for those
        approved_community_member? || team_member?
      end

      def approved_community_member?
        CommunityMember.exists?(session_user[:id])
      end

      def team_member?
        TeamMember.exists?(username)
      end

      def username
        session_user[:username]
      end

      def project_id
        params[:project_id]
      end

      def merge_request_iid
        params[:merge_request_iid]
      end

      def include_duo?
        [true, 'true'].include?(params[:include_duo])
      end

      def reviewer_mentions
        @reviewer_mentions ||= begin
          reviewers = params[:reviewers]
          reviewers << 'GitLabDuo' if include_duo?
          reviewers.map do |username|
            # Strip all characters unless alphanumeric, -, _ or .
            # https://docs.gitlab.com/user/profile/#change-your-username
            "@#{username.gsub(/[^a-zA-Z0-9\-_\.]/, '')}"
          end.join(' ')
        end
      end
    end
  end
end
