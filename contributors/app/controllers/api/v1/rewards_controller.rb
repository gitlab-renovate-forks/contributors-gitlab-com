# frozen_string_literal: true

module Api
  module V1
    class RewardsController < ApiApplicationController
      include PaginationConcern

      before_action :ensure_logged_in, except: :index
      before_action :ensure_admin, except: :index

      def index
        rewards = Reward.includes(:user, :awarded_by_user)
          .order(created_at: :desc).page(page).per(page_size)
        render_paginated_json(
          ::RewardsSerializer.new(rewards, { admin: admin? })
        )
      end

      def issue
        user_id = params[:user_id].to_i
        RewardsService.new(gitlab_access_token).issue(
          user_id:,
          credits:,
          reason:,
          note:,
          awarded_by_user_id: current_user.id
        )

        head :ok
      end

      def bulk_issue
        usernames = params[:usernames]
        if usernames.size > 100
          return render json: { error: 'Please provide 100 usernames or less' }, status: :bad_request
        end

        # It might take a while to issue all the rewards and we don't want the token to expire
        token = gitlab_access_token(force_refresh: true)
        BulkIssueRewardsJob.perform_later(token, usernames, credits, reason, note, current_user.id)

        head :ok
      end

      def credits
        params[:credits].to_i
      end

      def reason
        params[:reason]
      end

      def note
        params[:note]
      end
    end
  end
end
