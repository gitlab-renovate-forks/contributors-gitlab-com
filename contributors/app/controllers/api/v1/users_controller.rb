# frozen_string_literal: true

module Api
  module V1
    class UsersController < ApiApplicationController # rubocop:disable Metrics/ClassLength
      include DateParams

      before_action :ensure_logged_in, except: %i[show]
      before_action :ensure_admin, except: %i[show]

      HAS_LINKED_ISSUE_SQL = 'CASE WHEN labels @> \'["linked-issue"]\' THEN true ELSE false END as has_linked_issue'

      def show
        head :not_found and return if user.nil?

        data = { id: user.id, username: user.username, contributor_level: user.contributor_level, points: user.points }
        data[:bonus_points] = BonusPoints
          .awarded_between(from_date, to_date)
          .where(user_id:)
          .select('points', 'reason', 'created_at', 'activity_type')
        data[:merged_merge_requests] = MergeRequest
          .merged_between(from_date, to_date)
          .where(user_id:)
          .select(
            'title',
            'opened_date',
            'merged_date',
            'web_url',
            HAS_LINKED_ISSUE_SQL
          )
        data[:opened_merge_requests] = MergeRequest
          .opened_between(from_date, to_date)
          .where(user_id:)
          .select(
            'title',
            'opened_date',
            'state_id',
            'web_url',
            HAS_LINKED_ISSUE_SQL
          )
        data[:merged_commits] = MergeRequest
          .merged_between(from_date, to_date)
          .with_commits
          .where(commits: { user_id: })
          .select(
            'sha',
            'committed_date',
            'merged_date',
            'title as merge_request_title',
            'web_url as merge_request_web_url'
          )
        data[:opened_issues] = Issue
          .opened_between(from_date, to_date)
          .where(user_id:)
          .select('title', 'opened_date', 'state_id', 'web_url')
        data[:added_notes] = Note
          .added_between(from_date, to_date)
          .where(user_id:)
          .left_joins(:issue, :merge_request)
          .select(
            'id',
            'added_date',
            'coalesce(issues.title, merge_requests.title) as noteable_title',
            'coalesce(issues.web_url, merge_requests.web_url) as noteable_web_url'
          ).map do |note|
            {
              added_date: note.added_date,
              noteable_title: note.noteable_title,
              web_url: "#{note.noteable_web_url}#note_#{note.id}"
            }
          end
        data[:discord_messages] = DiscordMessage
          .added_between(from_date, to_date)
          .where(user_id:)
          .select('id', 'added_date', 'reply', 'channel_id')
          .map do |msg|
            {
              web_url: msg.web_url,
              added_date: msg.added_date,
              reply: msg.reply,
              channel: msg.channel
            }
          end
        data[:forum_posts] = ForumPost
          .added_between(from_date, to_date)
          .where(user_id:)
          .map do |post|
            {
              web_url: post.web_url,
              added_date: post.added_date,
              reply: post.reply,
              topic: post.topic_title
            }
          end
        data[:rewards] = rewards

        render json: data
      end

      def edit
        user = User.find(id)
        render json:
        {
          id: user.id,
          username: user.username,
          community_member: user.community_member,
          community_member_override: user&.user_profile&.community_member_override
        }
      end

      def update
        user_profile = UserProfile.find_or_initialize_by(user_id: id)
        user_profile.community_member_override = params[:community_member_override]

        if user_profile.save
          ActivitySummaryRefreshJob.perform_later
          render json: { message: 'User updated successfully' }
        else
          render json: { errors: user_profile.errors.full_messages }, status: :unprocessable_entity
        end
      end

      def add_points
        points = params[:points].to_i
        reason = params[:reason]
        activity_type = params[:activity_type]

        BonusPoints.create!(
          user_id: id,
          points:,
          reason:,
          activity_type:,
          awarded_by_user_id: current_user.id
        )
        render json: { message: 'User updated successfully' }, status: :created
      end

      private

      def id
        params[:id]
      end

      def numeric?(str)
        /^\d+$/.match?(str)
      end

      def user
        return @user if @user

        @user = User.find_by(id:) if numeric?(id)
        @user ||= User.find_by('LOWER(username) = ?', id.downcase)
      end

      def user_id
        user.id
      end

      def rewards
        return nil unless admin? || session[:user_id] == user_id

        Reward.where(user_id:).map do |reward|
          {
            reason: reward.reason,
            credits: reward.credits,
            gift_code: reward.gift_code,
            issue_web_url: reward.issue_web_url,
            awarded_by: reward.awarded_by_user.username,
            awarded_by_web_url: reward.awarded_by_user.web_url,
            created_at: reward.created_at
          }
        end
      end
    end
  end
end
