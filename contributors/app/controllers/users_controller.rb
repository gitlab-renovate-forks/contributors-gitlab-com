# frozen_string_literal: true

class UsersController < ApplicationController
  def login
    reset_session
    session[:return] = params[:return]

    redirect_to GitlabOauthService.oauth_authorize_url, allow_other_host: true
  end

  def logout
    reset_session

    redirect_to root_path
  end

  def oauth_callback
    user = GitlabOauthService.authorize(params['code'])
    session.merge!(user)
    session.merge!(admin: admin(user))
    Users::TrackActivityService.track_first_login!(user[:user_id])

    return_to = session.delete(:return) || root_path

    redirect_to return_to
  end

  private

  def admin(user)
    contributor_success_user_ids = FetchContributorSuccessUsersService.execute
    contributor_success_user_ids.include?(user[:user_id])
  end
end
