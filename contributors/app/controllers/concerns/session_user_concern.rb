# frozen_string_literal: true

module SessionUserConcern
  def session_user
    @session_user ||= {
      id: session[:user_id],
      username: session[:username],
      admin: session[:admin],
      access_token: gitlab_access_token
    }
  end
end
