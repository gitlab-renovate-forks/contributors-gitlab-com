# frozen_string_literal: true

module PaginationConcern
  DEFAULT_PAGE_SIZE = 20

  def page_size
    value = params[:page_size]
    value.present? && value.match?(/^\d+$/) ? value.to_i : DEFAULT_PAGE_SIZE
  end

  def page
    (params[:page].presence || 1).to_i
  end

  def render_paginated_json(records)
    render json: { records:, metadata: metadata(records.total_count) }
  end

  def metadata(total_count)
    {
      current_page: page,
      per_page: page_size,
      total_count:
    }
  end
end
