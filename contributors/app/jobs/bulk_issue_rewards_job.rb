# frozen_string_literal: true

class BulkIssueRewardsJob < ApplicationJob
  queue_as :default

  def perform(token, usernames, credits, reason, note, awarded_by_user_id) # rubocop: disable Metrics/ParameterLists
    service = RewardsService.new(token)

    usernames.each do |username|
      username = username[1..] if username.start_with?('@')
      user_id = User.find_by!('LOWER(username) = ?', username.downcase).id
      service.issue(
        user_id:,
        credits:,
        reason:,
        note:,
        awarded_by_user_id:,
        username:
      )
    rescue ActiveRecord::RecordNotFound
      Rails.logger.error { "Unable to find user: #{username} to issue reward" }
    end
  end
end
