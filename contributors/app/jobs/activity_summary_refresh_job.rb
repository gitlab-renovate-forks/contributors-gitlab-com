# frozen_string_literal: true

class ActivitySummaryRefreshJob < ApplicationJob
  queue_as :default

  def perform
    Views::ActivitySummary.refresh
    Views::RecentActivitySummary.refresh
  end
end
