# frozen_string_literal: true

class Project < ApplicationRecord
  has_many :issues, dependent: :restrict_with_exception
  has_many :merge_requests, dependent: :restrict_with_exception

  REQUIRED_FIELDS = %i[
    id
  ].freeze
  validates(*REQUIRED_FIELDS, presence: true)
end
