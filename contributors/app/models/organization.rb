# frozen_string_literal: true

class Organization < ApplicationRecord
  self.table_name = 'users'
  self.primary_key = nil
  default_scope lambda {
    select('users.organization AS name, SUM(view_activity_summaries.points) AS score,
      COUNT(DISTINCT users.id) AS contributors_count')
      .joins(:view_activity_summaries)
      .where.not(organization: [nil, ''])
      .group('users.organization')
  }

  has_many :view_activity_summaries, class_name: 'Views::ActivitySummary', primary_key: :id, foreign_key: :user_id,
    inverse_of: false, dependent: nil

  def read_only?
    true
  end
end
