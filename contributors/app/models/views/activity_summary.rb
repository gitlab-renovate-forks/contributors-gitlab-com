# frozen_string_literal: true

module Views
  class ActivitySummary < ApplicationView
    include ActivitySummarizable
  end
end
