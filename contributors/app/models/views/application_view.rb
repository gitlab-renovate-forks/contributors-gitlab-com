# frozen_string_literal: true

module Views
  class ApplicationView < ::ApplicationRecord
    self.abstract_class = true
    self.table_name_prefix = 'view_'

    def readonly?
      true
    end

    # TODO: This is only applicable to materialized views (should we move it?)
    def self.refresh(concurrently: true)
      Scenic.database.refresh_materialized_view(table_name, concurrently:, cascade: false)
    end
  end
end
