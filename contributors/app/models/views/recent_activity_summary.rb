# frozen_string_literal: true

module Views
  class RecentActivitySummary < ApplicationView
    include ActivitySummarizable
  end
end
