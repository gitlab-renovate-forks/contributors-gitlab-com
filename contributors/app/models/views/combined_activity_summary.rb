# frozen_string_literal: true

module Views
  class CombinedActivitySummary < ApplicationView
    include ActivitySummarizable
  end
end
