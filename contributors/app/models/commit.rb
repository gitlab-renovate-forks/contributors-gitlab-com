# frozen_string_literal: true

class Commit < ApplicationRecord
  belongs_to :merge_request
  belongs_to :user

  REQUIRED_FIELDS = %i[
    committed_date
    merge_request_id
    sha
    user_id
  ].freeze
  validates(*REQUIRED_FIELDS, presence: true)

  validates :sha, uniqueness: true
end
