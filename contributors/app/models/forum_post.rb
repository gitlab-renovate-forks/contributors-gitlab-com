# frozen_string_literal: true

class ForumPost < ApplicationRecord
  belongs_to :user

  REQUIRED_FIELDS = %i[
    added_date
    id
    post_number
    topic_id
    topic_title
    user_id
  ].freeze
  validates(*REQUIRED_FIELDS, presence: true)
  validates :reply, inclusion: [true, false]

  scope :added_between, ->(from_date, to_date) { where(added_date: from_date..to_date) }
  scope :user_counts, -> { group(:user_id).count }

  def web_url
    "https://forum.gitlab.com/t/#{topic_id}/#{post_number}"
  end
end
