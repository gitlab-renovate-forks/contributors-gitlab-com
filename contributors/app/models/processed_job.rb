# frozen_string_literal: true

class ProcessedJob < ApplicationRecord
  REQUIRED_FIELDS = %i[
    group
    issuable_type
    processed_records
    elapsed
    activity_date
  ].freeze
  validates(*REQUIRED_FIELDS, presence: true)
end
