# frozen_string_literal: true

class Reward < ApplicationRecord
  belongs_to :awarded_by_user, class_name: 'User'
  belongs_to :user

  # TODO: Add reason, link and cc to allow more info in the issue description?

  REQUIRED_FIELDS = %i[
    awarded_by_user_id
    gift_code
    issue_iid
    credits
    reason
    user_id
  ].freeze
  validates(*REQUIRED_FIELDS, presence: true)

  # NOTE: Update `app/javascript/common/Constants.js` when updating this list
  enum :reason, {
    other: 0,
    first_contribution: 1,
    hackathon: 2,
    notable_contributor: 3,
    level_up: 4
  }, prefix: true

  def issue_web_url
    "https://gitlab.com/gitlab-org/developer-relations/contributor-success/rewards/-/issues/#{issue_iid}"
  end
end
