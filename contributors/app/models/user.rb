# frozen_string_literal: true

class User < ApplicationRecord
  has_many :issues, dependent: :delete_all
  has_many :merge_requests, dependent: :delete_all
  has_many :notes, dependent: :delete_all
  has_many :bonus_points, class_name: 'BonusPoints', dependent: nil
  has_many :awarded_bonus_points, class_name: 'BonusPoints', inverse_of: :awarded_by_user, dependent: nil
  has_many :discord_messages, dependent: nil
  has_many :forum_posts, dependent: nil
  has_many :rewards, dependent: nil
  has_many :awarded_rewards, class_name: 'Reward', inverse_of: :awarded_by_user, dependent: nil

  has_one :user_profile, dependent: nil

  REQUIRED_FIELDS = %i[
    id
    username
  ].freeze
  validates(*REQUIRED_FIELDS, presence: true)
  validates :community_member, inclusion: [true, false]

  def self.wider_community
    left_joins(:user_profile)
      .where('community_member_override IS TRUE OR (community_member_override IS NULL AND community_member IS TRUE)')
  end

  def self.other_members
    left_joins(:user_profile)
      .where('community_member_override IS FALSE OR (community_member_override IS NULL AND community_member IS FALSE)')
  end

  def community_member
    override = user_profile&.community_member_override

    override.nil? ? self[:community_member] : override
  end

  def user_community_member
    self[:community_member]
  end

  def web_url
    "https://gitlab.com/#{username}"
  end
end
