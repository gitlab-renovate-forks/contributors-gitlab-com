# frozen_string_literal: true

class Setting < RailsSettings::Base
  cache_prefix { 'v4' }

  scope :points do
    field :created_issue_points, type: :integer, default: 5
    field :created_mr_author_points, type: :integer, default: 20
    field :discord_message_points, type: :integer, default: 1
    field :discord_reply_points, type: :integer, default: 2
    field :forum_post_points, type: :integer, default: 1
    field :forum_reply_points, type: :integer, default: 2
    field :merged_mr_author_points, type: :integer, default: 60
    field :merged_mr_committer_points, type: :integer, default: 20
    field :merged_mr_with_issue_points, type: :integer, default: 30
    field :note_points, type: :integer, default: 1
  end
end
