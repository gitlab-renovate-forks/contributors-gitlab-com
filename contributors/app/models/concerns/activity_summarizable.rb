# frozen_string_literal: true

module ActivitySummarizable
  extend ActiveSupport::Concern

  included do
    scope :between, ->(from_date, to_date) { where(activity_date: from_date..to_date) }
    scope :user_counts, lambda {
      select(
        <<-SQL.squish
          user_id,
          username,
          MAX(contributor_level) AS contributor_level,
          SUM(opened_mrs) AS opened_merge_requests,
          SUM(merged_mrs) AS merged_merge_requests,
          SUM(merged_mrs_with_linked_issues) AS merged_with_issues,
          SUM(opened_issues) AS opened_issues,
          SUM(merged_commits) AS merged_commits,
          SUM(added_notes) AS added_notes,
          SUM(bonus_points) AS bonus_points,
          SUM(points) AS score
        SQL
      ).group(:user_id, :username)
    }
    scope :username_search, ->(username) { where("username ILIKE '%#{sanitize_sql_like(username)}%'") }
    scope :wider_community, -> { where(community_member: true) }
  end
end
