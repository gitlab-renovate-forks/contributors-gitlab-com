export const numberProps = {
  formatter: value => value.toLocaleString(),
  class: 'text-end',
};

export const dateFormatter = (value) => {
  const date = new Date(value);
  return `${date.toLocaleDateString()} ${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}`;
};
