import { mount } from '@vue/test-utils';
import { BButton, BForm, BFormSelect, BInput, BFormTextarea, createBootstrap } from 'bootstrap-vue-next';

import IssueRewardModal from './IssueRewardModal.vue';

const bootstrap = createBootstrap();
let wrapper;
let wrapperWrapper;
const createWrapper = (props = {}) => {
  wrapperWrapper = mount(IssueRewardModal, {
    props: {
      modelValue: true,
      ...props,
    },
    global: { plugins: [bootstrap] },
  });
  wrapper = wrapperWrapper.findComponent(BForm);
};

const fillForm = async () => {
  await wrapper.findComponent(BInput).setValue(10);
  await wrapper.findComponent(BFormSelect).setValue('notable_contributor');
};
const submitForm = async () => {
  await wrapper.findComponent(BButton).trigger('click');
};

beforeEach(() => {
  global.fetch = vi.fn(() => Promise.resolve({ json: () => ({}) }));
});

describe('fork issuing individual rewards', () => {
  beforeEach(() => {
    createWrapper({ userId: 123 });
  });

  it('renders the modal with correct content', () => {
    expect(wrapper.text()).toBe('Credits:Reason (not be added to the contributor issue):First contributionHackathonNotable contributor (MVP)Level upOtherNote (added to the contributor issue, use markdown): Issue reward');
  });

  it('cannot be submitted without filling in the form', async () => {
    await submitForm();

    expect(wrapper.emitted('submit')).toBeFalsy();
  });

  it('can be submitted when filled out', async () => {
    await fillForm();
    await submitForm();

    expect(wrapper.emitted('submit')).toBeTruthy();
  });

  it('calls issue reward API when form is submitted', async () => {
    await fillForm();
    await wrapper.findAllComponents(BFormTextarea).at(0).setValue('See link');
    await submitForm();

    expect(global.fetch).toHaveBeenCalledWith('/api/v1/rewards/issue', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ usernames: [], note: 'See link', credits: 10, reason: 'notable_contributor', user_id: 123 }),
    });
  });

  it('emits rewardIssued event', async () => {
    await fillForm();
    await submitForm();

    expect(wrapperWrapper.emitted('rewardIssued')).toBeTruthy();
  });
});

describe('fork bulk issuing rewards', () => {
  beforeEach(() => {
    createWrapper();
  });

  it('renders the modal with correct content', () => {
    expect(wrapper.text()).toBe('Users (one per line):Credits:Reason (not be added to the contributor issue):First contributionHackathonNotable contributor (MVP)Level upOtherNote (added to the contributor issue, use markdown): Issue reward');
  });

  it('cannot be submitted without filling in the form', async () => {
    await submitForm();

    expect(wrapper.emitted('submit')).toBeFalsy();
  });

  it('can be submitted when filled out', async () => {
    await fillForm();
    await wrapper.findAllComponents(BFormTextarea).at(0).setValue('username');
    await submitForm();

    expect(wrapper.emitted('submit')).toBeTruthy();
  });

  it('calls bulk issue reward API when form is submitted', async () => {
    await fillForm();
    await wrapper.findAllComponents(BFormTextarea).at(0).setValue('user1\nuser2');
    await wrapper.findAllComponents(BFormTextarea).at(1).setValue('See link');
    await submitForm();

    expect(global.fetch).toHaveBeenCalledWith('/api/v1/rewards/bulk_issue', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ usernames: ['user1', 'user2'], note: 'See link', credits: 10, reason: 'notable_contributor' }),
    });
  });
});
