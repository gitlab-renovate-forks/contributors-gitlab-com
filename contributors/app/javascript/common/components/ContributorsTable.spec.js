import { mount, shallowMount } from '@vue/test-utils';
import { BTable, createBootstrap } from 'bootstrap-vue-next';

import ContributorsTable from './ContributorsTable.vue';
import PointsDocumentationTooltip from './PointsDocumentationTooltip.vue';

let bootstrap = createBootstrap();
let wrapper;
const contributors = [
  {
    id: 5211906,
    username: 'csouthard',
    merged_merge_requests: 4,
    merged_with_issues: 1,
    opened_merge_requests: 4,
    opened_issues: 0,
    merged_commits: 4,
    added_notes: 0,
    score: 430,
    contributor_level: 1,
    bonus_points: 0,
  },
  {
    id: 8198564,
    username: 'rkadam3',
    merged_merge_requests: 2,
    merged_with_issues: 0,
    opened_merge_requests: 3,
    opened_issues: 0,
    merged_commits: 2,
    added_notes: 1,
    score: 221,
    contributor_level: 1,
    bonus_points: 0,
  },
];

const createWrapper = ({ mountFunction = shallowMount, busy = true } = {}) => {
  wrapper = mountFunction(ContributorsTable, {
    props: { busy, contributors },
    global: {
      plugins: [bootstrap],
      stubs: { TableWithBusyState: false },
    },
  });
};

it('renders TableWithBusyState with expected fields', async () => {
  createWrapper();

  const fieldLabels = wrapper.findComponent(BTable).props('fields').map(field => field.label);

  expect(fieldLabels).toStrictEqual(['Username', 'Level', 'Open', 'Commit', 'Merge', 'Link', 'Issue', 'Note', 'Bonus', 'Score']);
});

it('renders TableWithBusyState with busy state', () => {
  createWrapper();

  expect(wrapper.findComponent(BTable).props('busy')).toBe(true);
});

describe('when busy is false', () => {
  it('renders TableWithBusyState with non-busy state', () => {
    createWrapper({ busy: false });

    expect(wrapper.findComponent(BTable).props('busy')).toBe(false);
  });
});

it('renders PointsDocumentationTooltip', () => {
  createWrapper({ mountFunction: mount });

  expect(wrapper.findComponent(PointsDocumentationTooltip).exists()).toBe(true);
});
