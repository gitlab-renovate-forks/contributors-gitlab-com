import { mount } from '@vue/test-utils';
import { BPagination } from 'bootstrap-vue-next';
import { reactive } from 'vue';

import RoutePagination from './RoutePagination.vue';

let wrapper;
let reactiveRoute;

const createWrapper = (propsData) => {
  wrapper = mount(RoutePagination, {
    props: propsData,
  });
};

const findPage2Button = () => wrapper.findComponent(BPagination).find('button[aria-posinset="2"]');

beforeEach(() => {
  reactiveRoute = reactive({ query: { } });
  vi.mock('vue-router', async () => {
    return {
      useRoute: vi.fn(() => reactiveRoute),
      useRouter: vi.fn(() => ({
        push: vi.fn(({ query }) => {
          reactiveRoute.query = { ...reactiveRoute.query, ...query };
        }),
      })),
    };
  });
});

describe('on mount', () => {
  it('renders BPagination for multiple pages', () => {
    createWrapper({ totalRows: 10, perPage: 5 });

    expect(wrapper.findComponent(BPagination).exists()).toBe(true);
  });

  it('does not render BPagination for single page', () => {
    createWrapper({ totalRows: 5, perPage: 5 });

    expect(wrapper.findComponent(BPagination).exists()).toBe(false);
  });

  it('initializes current page based on route query', async () => {
    reactiveRoute.query = { page: 2 };
    createWrapper({ totalRows: 10, perPage: 5 });

    expect(wrapper.vm.currentPage).toBe(2);
  });

  it('defaults to first page', async () => {
    createWrapper({ totalRows: 10, perPage: 5 });

    expect(wrapper.vm.currentPage).toBe(1);
  });

  it('emits current page', async () => {
    reactiveRoute.query = { page: 2 };
    createWrapper({ totalRows: 10, perPage: 5 });

    expect(wrapper.emitted('update:modelValue')).toStrictEqual([[2]]);
  });
});

describe('changing page', () => {
  beforeEach(() => {
    createWrapper({ totalRows: 10, perPage: 5 });
  });

  it('updates route', async () => {
    findPage2Button().trigger('click');

    expect(reactiveRoute.query.page).toBe(2);
  });

  it('updates current page', async () => {
    findPage2Button().trigger('click');

    expect(wrapper.vm.currentPage).toBe(2);
  });
});
