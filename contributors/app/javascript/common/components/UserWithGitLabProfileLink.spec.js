import { config, mount } from '@vue/test-utils';
import { BLink, createBootstrap } from 'bootstrap-vue-next';

import UserWithGitLabProfileLink from './UserWithGitLabProfileLink.vue';

const bootstrap = createBootstrap({ directives: true });
const defaultProps = {
  username: 'testuser',
  filterData: {
    fromDate: '2024-01-01',
    toDate: '2024-12-31',
  },
};

let wrapper;
const createWrapper = () => {
  const el = document.createElement('div');
  el.id = 'app';
  document.body.appendChild(el);

  wrapper = mount(UserWithGitLabProfileLink, {
    global: {
      plugins: [bootstrap],
    },
    props: {
      ...defaultProps,
    },
    attachTo: document.getElementById('app'),
  });
};

describe('UserWithGitLabProfileLink', () => {
  beforeAll(() => {
    config.global.renderStubDefaultSlot = true;
  });

  beforeEach(() => {
    vi.useFakeTimers();
    createWrapper();
  });

  it('sets correct props for internal link', () => {
    const internalLink = wrapper.findComponent(BLink);

    expect(internalLink.props('to')).toEqual({
      name: 'user',
      params: { userHandle: defaultProps.username },
      query: {
        fromDate: defaultProps.filterData.fromDate,
        toDate: defaultProps.filterData.toDate,
      },
    });
    expect(internalLink.text()).toBe(defaultProps.username);
  });

  it('sets correct props for external link', async () => {
    const externalLink = wrapper.find(`a[href="https://gitlab.com/${defaultProps.username}"`);

    expect(externalLink.attributes('target')).toBe('_blank');
  });

  it('renders tooltip with correct content', async () => {
    const tooltip = document.querySelector('.tooltip');

    expect(tooltip.textContent).toBe('View on GitLab.com');
  });

  it('renders external link icon', () => {
    const icon = wrapper.find('.external-link-icon');

    expect(icon.exists()).toBe(true);
  });
});
