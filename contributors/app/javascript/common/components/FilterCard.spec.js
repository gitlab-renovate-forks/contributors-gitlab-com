import { mount } from '@vue/test-utils';
import { BButton } from 'bootstrap-vue-next';
import { reactive } from 'vue';

import FilterCard from './FilterCard.vue';

let wrapper;
let reactiveRoute;
const createWrapper = ({ showCommunityOnly = false, showSearch = false } = {}) => {
  const modelValue = {
    fromDate: '',
    toDate: '',
    communityOnly: true,
    search: '',
  };
  wrapper = mount(FilterCard, {
    props: { modelValue, showCommunityOnly, showSearch },
  });
};
const findInWrapper = testId => wrapper.findByTestId(`filter-card-${testId}`);

beforeEach(() => {
  reactiveRoute = reactive({ query: { fromDate: '2024-04-06', toDate: '' } });
  vi.mock('vue-router', async () => {
    return {
      useRoute: vi.fn(() => reactiveRoute),
      useRouter: vi.fn(() => ({
        push: vi.fn(({ query }) => {
          reactiveRoute.query = { ...reactiveRoute.query, ...query };
        }),
      })),
    };
  });
});

it('does not render community only checkbox', async () => {
  createWrapper();

  expect(wrapper.text()).not.include('Community only');
});

describe('with community-only prop', () => {
  it('renders community only checkbox', async () => {
    createWrapper({ showCommunityOnly: true });

    expect(wrapper.text()).include('Community only');
  });
});

it('does not render search input', async () => {
  createWrapper();

  expect(findInWrapper('search-input').exists()).toBe(false);
});

describe('with show-search prop', () => {
  it('renders search input', async () => {
    createWrapper({ showSearch: true });

    expect(findInWrapper('search-input').exists()).toBe(true);
  });
});

describe('clicking filter', () => {
  describe('with no changes', () => {
    it('does not emit filter', async () => {
      createWrapper();

      await wrapper.findComponent(BButton).trigger('click');

      expect(wrapper.emitted('filter')).toStrictEqual([[], []]);
    });
  });

  describe('with modified values', () => {
    it('emits expected events and data', async () => {
      createWrapper({ showCommunityOnly: true, showSearch: true });

      findInWrapper('from-date').setValue('2022-01-01');
      findInWrapper('to-date').setValue('2023-01-01');
      findInWrapper('community-only').setChecked(false);
      findInWrapper('search-input').setValue('GitLab');
      await findInWrapper('search-button').trigger('click');

      expect(wrapper.emitted('update:modelValue')[1][0]).toEqual({
        fromDate: '2022-01-01',
        toDate: '2023-01-01',
        search: 'GitLab',
        communityOnly: false,
        currentPage: 1,
      });
      expect(wrapper.emitted('filter')).toBeTruthy();
    });
  });
});

describe('period dropdown', () => {
  beforeEach(async () => {
    const mockDate = new Date(2024, 6, 4);
    vi.useFakeTimers();
    vi.setSystemTime(mockDate);

    createWrapper();
  });

  afterEach(() => {
    vi.useRealTimers();
  });

  const periodDateRanges = {
    'Past 6 months': { fromDate: '2024-01-06', toDate: '' },
    'Past year': { fromDate: '2023-07-05', toDate: '' },
    'This month': { fromDate: '2024-07-01', toDate: '' },
    'Last month': { fromDate: '2024-06-01', toDate: '2024-07-01' },
  };

  Object.keys(periodDateRanges).forEach((period) => {
    it(`emits expected dates for ${period} and emits filter`, async () => {
      await findInWrapper('named-period').setValue(period);

      const periodDateRange = periodDateRanges[period];
      expect(wrapper.emitted('update:modelValue')[1][0]).toEqual({
        fromDate: periodDateRange.fromDate,
        toDate: periodDateRange.toDate,
        communityOnly: true,
        search: '',
        currentPage: 1,
      });
      expect(wrapper.emitted('filter')).toStrictEqual([[], [], []]);
    });
  });

  it('sets period to custom but does not emit filter when random dates selected', async () => {
    await findInWrapper('from-date').setValue('2022-01-01');
    await findInWrapper('to-date').setValue('2023-01-01');

    expect(findInWrapper('named-period').element.value).toBe('Custom');
    expect(wrapper.emitted('filter')).toStrictEqual([[], []]);
  });

  it('sets period to last month but does not emit filter when dates selected', async () => {
    findInWrapper('from-date').setValue('2024-06-01');
    await findInWrapper('to-date').setValue('2024-07-01');

    expect(findInWrapper('named-period').element.value).toBe('Last month');
    expect(wrapper.emitted('filter')).toStrictEqual([[], []]);
  });
});
