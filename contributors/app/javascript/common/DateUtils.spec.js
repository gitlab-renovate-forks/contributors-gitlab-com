import { daysAgo, firstOf } from './DateUtils';

describe('daysAgo', () => {
  it('should return the date 90 days ago in YYYY-MM-DD format', () => {
    const mockDate = new Date(2024, 5, 29);
    vi.useFakeTimers();
    vi.setSystemTime(mockDate);

    expect(daysAgo(90)).toBe('2024-03-31');

    vi.useRealTimers();
  });
});

describe('firstOf', () => {
  beforeEach(() => {
    const mockDate = new Date(2024, 6, 4);
    vi.useFakeTimers();
    vi.setSystemTime(mockDate);
  });

  afterEach(() => {
    vi.useRealTimers();
  });

  it('returns the first of this month', () => {
    expect(firstOf()).toEqual('2024-07-01');
  });

  describe('with an offset', () => {
    it('returns the first of the relevant month', () => {
      expect(firstOf(2)).toEqual('2024-05-01');
    });
  });
});
