import { mount } from '@vue/test-utils';
import { createBootstrap } from 'bootstrap-vue-next';

import LevelProgress from '~/common/components/LevelProgress.vue';

import ActionCard from './home-page/ActionCard.vue';
import HomePage from './HomePage.vue';

const bootstrap = createBootstrap();
let wrapper;
const createWrapper = (vueData = {}) => {
  wrapper = mount(
    HomePage,
    {
      global: {
        plugins: [bootstrap],
        provide: { vueData: { contributorPoints: 1, ...vueData } },
      },
    },
  );
};

const actionCards = () => wrapper.findAllComponents(ActionCard);

it('contains the expected number of cards', () => {
  createWrapper();

  expect(actionCards()).toHaveLength(6);
});

it('does not render level progress when user signed out', () => {
  createWrapper();

  expect(wrapper.findComponent(LevelProgress).exists()).toBe(false);
});

describe('when logged in', () => {
  it('contains the expected number of cards', () => {
    createWrapper({ username: 'lee', contributorLevel: 1 });

    expect(actionCards()).toHaveLength(5);
  });

  it('renders level progress', () => {
    createWrapper({ username: 'lee' });

    expect(wrapper.findComponent(LevelProgress).exists()).toBe(true);
  });

  describe('when the user has discord messages', () => {
    it('contains the expected number of cards', () => {
      createWrapper({ username: 'lee', contributorLevel: 1, hasDiscordMessages: true });

      expect(actionCards()).toHaveLength(4);
    });
  });

  describe('when the user has a contributor level below 2', () => {
    it('contains the expected number of cards', () => {
      createWrapper({ username: 'lee', contributorLevel: 1 });

      expect(actionCards()).toHaveLength(5);
    });
  });

  describe('when the user has a contributor level of 2', () => {
    it('contains the expected number of cards', () => {
      createWrapper({ username: 'lee', contributorLevel: 2 });

      expect(actionCards()).toHaveLength(4);
    });
  });
});
