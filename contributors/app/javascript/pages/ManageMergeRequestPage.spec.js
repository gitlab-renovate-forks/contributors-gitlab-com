import { useQuery } from '@vue/apollo-composable';
import { shallowMount } from '@vue/test-utils';
import { BOverlay } from 'bootstrap-vue-next';
import { nextTick } from 'vue';

import ManageMergeRequestPage from './ManageMergeRequestPage.vue';

let wrapper;
const createWrapper = ({
  apiAuthorized = true,
  randomAvailableCoach = 'coach',
  username = 'username',
  withReviewers = true,
  mrState = 'opened',
} = {}) => {
  global.fetch = vi.fn(() =>
    Promise.resolve({
      ok: true,
      json: () => Promise.resolve({ authorized: apiAuthorized, randomAvailableCoach }),
    }),
  );

  useQuery.mockImplementation(() => graphqlResponse({ withReviewers, mrState }));

  wrapper = shallowMount(ManageMergeRequestPage, {
    global: {
      provide: {
        vueData: { username },
      },
    },
  });
};

vi.mock('vue-router', () => ({
  useRoute: vi.fn(() => ({ query: { mergeRequestIid: '123', projectId: '999' } })),
}));

vi.mock('@vue/apollo-composable', () => ({
  __esModule: true,
  useQuery: vi.fn(),
}));

const graphqlResponse = ({ withReviewers, mrState }) => {
  const reviewers = withReviewers
    ? [
        { username: 'reviewer1' },
        { username: 'reviewer2' },
      ]
    : [];
  return {
    result: {
      value: {
        projects: {
          nodes: [{
            mergeRequest: {
              state: mrState,
              iid: 123,
              title: 'Merge Request Title',
              webUrl: 'http://example.com/mr',
              project: {
                id: 'gid://gitlab/Project/999',
                name: 'Project Name',
                webUrl: 'http://example.com/project',
              },
              labels: {
                nodes: [
                  { title: 'Label 1' },
                  { title: 'Label 2' },
                ],
              },
              reviewers: {
                nodes: reviewers,
              },
              assignees: {
                nodes: [
                  { username: 'assignee1' },
                  { username: 'assignee2' },
                ],
              },
              author: {
                username: 'author',
              },
            },
          }],
        },
      },
    },
  };
};

it('renders loading spinner', () => {
  createWrapper();

  expect(wrapper.findComponent(BOverlay).props('show')).toBe(true);
  expect(wrapper.text()).toBe('');
});

describe('once loaded', () => {
  beforeEach(async () => {
    createWrapper();
    await nextTick();
  });

  it('displays merge request details', () => {
    expect(wrapper.text()).toContain('Project Name');
    expect(wrapper.text()).toContain('Merge Request Title');
    expect(wrapper.text()).toContain('Author: author');
    expect(wrapper.text()).toContain('Reviewers: reviewer1, reviewer2');
    expect(wrapper.text()).toContain('Assignees: assignee1, assignee2');
  });

  it('makes API request to fetch data', () => {
    expect(global.fetch).toHaveBeenCalledWith('/api/v1/manage_merge_request');
  });

  it('shows review option', () => {
    expect(wrapper.text()).toContain('Request review');
  });

  it('contains links to project and merge request', () => {
    const links = wrapper.findAll('a');

    expect(links.some(link => link.attributes('href') === 'http://example.com/project')).toBe(true);
    expect(links.some(link => link.attributes('href') === 'http://example.com/mr')).toBe(true);
  });
});

describe('when user is not authorized via API', () => {
  it('shows unauthorized messaging', async () => {
    createWrapper({ apiAuthorized: false });
    await nextTick();
    await nextTick();

    expect(wrapper.text()).toContain('To manage merge requests you must be: The authorAn assigneeA reviewerAn approved community memberA GitLab team member');
  });

  describe.each([
    ['author', 'author'],
    ['assignee', 'assignee1'],
    ['reviewer', 'reviewer2'],
  ])('when user is authorized as %s', (role, username) => {
    it('shows review option', async () => {
      createWrapper({ apiAuthorized: false, username });
      await nextTick();
      await nextTick();

      expect(wrapper.text()).toContain('Request review');
    });
  });
});

describe('when there are current reviewers', () => {
  it('shows both review options', async () => {
    createWrapper();
    await nextTick();
    await nextTick();

    expect(wrapper.text()).toContain('Request review from current reviewers');
    expect(wrapper.text()).toContain('Request review from merge request coach coach');
    expect(wrapper.text()).toContain('Request review from someone else');
  });
});

describe('when there are no reviewers', () => {
  it('shows only one review option', async () => {
    createWrapper({ withReviewers: false });
    await nextTick();
    await nextTick();

    expect(wrapper.text()).not.toContain('Request review from current reviewers');
    expect(wrapper.text()).toContain('Request review from merge request coach coach');
    expect(wrapper.text()).toContain('Request review from someone else');
  });
});

describe('when the merge request is not open', () => {
  it('shows status message', async () => {
    createWrapper({ mrState: 'merged' });
    await nextTick();
    await nextTick();

    expect(wrapper.text()).toContain('You can only manage merge requests that are open');
  });
});
