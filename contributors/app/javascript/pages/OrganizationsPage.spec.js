import { mount } from '@vue/test-utils';
import { BButton, BTable, createBootstrap } from 'bootstrap-vue-next';
import { reactive } from 'vue';

import FilterCard from '~/common/components/FilterCard.vue';
import PointsDocumentationTooltip from '~/common/components/PointsDocumentationTooltip.vue';
import RoutePagination from '~/common/components/RoutePagination.vue';
import ThemedOverlay from '~/common/components/ThemedOverlay.vue';
import { daysAgo } from '~/common/DateUtils';

import OrganizationsPage from './OrganizationsPage.vue';

const bootstrap = createBootstrap();
let wrapper;
let reactiveRoute;
const createWrapper = () => {
  wrapper = mount(OrganizationsPage, {
    global: { plugins: [bootstrap] },
  });
};

const mockResponse = {
  metadata: {
    total_count: 2,
    current_page: 1,
    per_page: 50,
  },
  records: [
    {
      name: 'Foo',
      contributors_count: 2,
      score: 100,
    },
    {
      name: 'Bar',
      contributors_count: 1,
      score: 50,
    },
  ],
};

beforeEach(() => {
  reactiveRoute = reactive({ query: {}, params: { organizationName: 'GitLab' } });
  vi.mock('vue-router', async () => {
    return {
      useRoute: vi.fn(() => reactiveRoute),
      useRouter: vi.fn(() => ({
        push: vi.fn(({ query }) => {
          reactiveRoute.query = { ...reactiveRoute.query, ...query };
        }),
      })),
    };
  });
});

describe('while loading', () => {
  beforeEach(() => {
    global.fetch = vi.fn(() => new Promise(() => {}));

    createWrapper();
  });

  it('renders overlay with loading state', () => {
    expect(wrapper.findComponent(ThemedOverlay).props('show')).toBe(true);
  });

  it('sends the correct default api query', () => {
    const url = new URL('/api/v1/organizations', window.location.href);
    url.searchParams.append('from_date', daysAgo(90));
    url.searchParams.append('to_date', '');
    url.searchParams.append('search', '');
    url.searchParams.append('page', 1);

    expect(global.fetch).toHaveBeenCalledWith(url);
  });

  describe('with modified params', () => {
    it('sends the correct api query', async () => {
      wrapper.findByTestId('filter-card-from-date').setValue('2022-01-01');
      wrapper.findByTestId('filter-card-to-date').setValue('2023-01-01');
      wrapper.findByTestId('filter-card-search-input').setValue('Bob');
      const filterButton = wrapper.findComponent(BButton);
      await filterButton.trigger('click');

      const url = new URL('/api/v1/organizations', window.location.href);
      url.searchParams.append('from_date', '2022-01-01');
      url.searchParams.append('to_date', '2023-01-01');
      url.searchParams.append('search', 'Bob');
      url.searchParams.append('page', 1);

      expect(global.fetch).toHaveBeenCalledWith(url);
    });
  });
});

describe('once loaded', () => {
  beforeEach(async () => {
    global.fetch = vi.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve(mockResponse),
      }),
    );

    reactiveRoute.query = { page: 2 };
    createWrapper();
  });

  it('renders FilterCard', () => {
    expect(wrapper.findComponent(FilterCard).exists()).toBe(true);
  });

  it('renders BTable with organizations', () => {
    const organizationsTable = wrapper.findComponent(BTable);

    expect(organizationsTable.exists()).toBe(true);
    expect(organizationsTable.props('items')).toEqual(mockResponse.records);
  });

  it('renders PointsDocumentationTooltip', () => {
    expect(wrapper.findComponent(PointsDocumentationTooltip).exists()).toBe(true);
  });

  it('renders pagination', () => {
    const pagination = wrapper.findComponent(RoutePagination);

    expect(pagination.exists()).toBe(true);
    expect(pagination.props('totalRows')).toEqual(2);
    expect(pagination.props('perPage')).toEqual(50);
  });

  it('sends the correct api query with changed page', async () => {
    const url = new URL('/api/v1/organizations', window.location.href);
    url.searchParams.append('from_date', daysAgo(90));
    url.searchParams.append('to_date', '');
    url.searchParams.append('search', '');
    url.searchParams.append('page', 2);

    expect(global.fetch).toHaveBeenCalledWith(url);
  });
});
