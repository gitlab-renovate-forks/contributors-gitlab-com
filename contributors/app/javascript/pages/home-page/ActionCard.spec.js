import { mount } from '@vue/test-utils';
import { BButton, BCard, BCardImg } from 'bootstrap-vue-next';

import ActionCard from './ActionCard.vue';

let wrapper;
const createWrapper = (props = { logoPath: '/test' }) => {
  wrapper = mount(
    ActionCard,
    {
      propsData: {
        ...props,
        title: 'Login title',
        actionName: 'Login',
        actionLink: '/login',
      },
      slots: {
        default: [
          '<div>Please login to help us tailor your experience.</div>',
        ],
      },
      global: {
        provide: { vueData: {} },
      },
    },
  );
};

const findActionButton = () => wrapper.findComponent(BButton);

it('contains card', () => {
  createWrapper();

  const card = wrapper.findComponent(BCard);
  expect(card.exists()).toEqual(true);
  expect(card.text()).toContain('Login title');
  expect(card.text()).toContain('Please login to help us tailor your experience.');
});

it('contains action button with target blank', () => {
  createWrapper();

  const actionButton = findActionButton();

  expect(actionButton.exists()).toEqual(true);
  expect(actionButton.attributes().href).toEqual('/login');
  expect(actionButton.attributes().target).toEqual('_blank');
  expect(actionButton.text()).toEqual('Login');
});

describe('when targetBlank prop is false', () => {
  it('contains action button with no target attribute', () => {
    createWrapper({ targetBlank: false });

    const actionButton = findActionButton();

    expect(actionButton.exists()).toEqual(true);
    expect(actionButton.attributes().href).toEqual('/login');
    expect(actionButton.attributes().target).toBe(undefined);
    expect(actionButton.text()).toEqual('Login');
  });
});

it('contains image', () => {
  createWrapper();

  const image = wrapper.findComponent(BCardImg);
  expect(image.exists()).toEqual(true);
  expect(image.attributes().src).toEqual('/test');
});

describe('when logoPath prop is null', () => {
  it('renders no image', () => {
    createWrapper({});

    const image = wrapper.findComponent(BCardImg);
    expect(image.exists()).toEqual(false);
  });
});
