import { shallowMount } from '@vue/test-utils';
import { nextTick } from 'vue';

import DocumentationPage from './DocumentationPage.vue';
import NotFoundPage from './NotFoundPage.vue';

global.fetch = vi.fn();

const mockMarkdown = `
# Test Header

- [anchor link](#anchor-link)
- [absolute link](https://google.com)
- [relative markdown link](relative-link.md)
- [relative markdown nested link](nested/relative-link.md)
- ![image](image.png)
`;

let wrapper;
const createWrapper = () => {
  fetch.mockResolvedValueOnce({
    text: () => Promise.resolve(mockMarkdown),
    headers: {
      get: vi.fn().mockImplementation(() => 'text/plain'),
    },
  });

  wrapper = shallowMount(
    DocumentationPage,
    {
      props: {
        page: 'test-page',
      },
    },
  );
};

const renderedHtml = () => wrapper.find('.markdown-content').html();

it('initially shows loading documentation...', () => {
  createWrapper();

  expect(wrapper.text()).toBe('Loading documentation...');
});

describe('once loaded', () => {
  beforeEach(async () => {
    createWrapper();

    await nextTick();
  });

  it('does not render the not found page component', async () => {
    await createWrapper();

    expect(wrapper.findComponent(NotFoundPage).exists()).toBe(false);
  });

  it('loads expected static markdown asset', async () => {
    expect(fetch).toHaveBeenCalledWith('/docs/test-page.md');
  });

  it('renders header with expected anchor/id', async () => {
    expect(renderedHtml()).toContain('<h1 id="test-header">Test Header</h1>');
  });

  it('does not modify anchor links', () => {
    expect(renderedHtml()).toContain('<a href="#anchor-link">anchor link</a>');
  });

  it('adds target="_blank" to absolute links', () => {
    expect(renderedHtml()).toContain('<a href="https://google.com" target="_blank">absolute link</a>');
  });

  it('strips .md suffix from relative markdown links', () => {
    expect(renderedHtml()).toContain('<a href="relative-link">relative markdown link</a>');
    expect(renderedHtml()).toContain('<a href="nested/relative-link">relative markdown nested link</a>');
  });

  it('updates image path', () => {
    expect(renderedHtml()).toContain('<img src="/docs/image.png" alt="image">');
  });

  it('updates document title', () => {
    expect(document.title).toBe('Test Header - GitLab Contributor Platform');
  });
});

describe('when markdown asset is not found', () => {
  it('renders the not found page component', async () => {
    fetch.mockResolvedValueOnce({
      headers: {
        get: vi.fn().mockImplementation(() => 'text/html'),
      },
    });

    await createWrapper();

    expect(wrapper.findComponent(NotFoundPage).exists()).toBe(true);
  });
});
