import { config, mount, shallowMount } from '@vue/test-utils';
import { BButton, BTable, createBootstrap } from 'bootstrap-vue-next';
import { nextTick, reactive } from 'vue';

import ContributorLevelBadge from '~/common/components/ContributorLevelBadge.vue';
import IssueRewardModal from '~/common/components/IssueRewardModal.vue';
import LevelProgress from '~/common/components/LevelProgress.vue';

import AddEventModal from './user-page/AddEventModal.vue';
import AddPointsModal from './user-page/AddPointsModal.vue';
import PagedAccordionTable from './user-page/PagedAccordionTable.vue';
import UserPage from './UserPage.vue';

const bootstrap = createBootstrap();
let wrapper;
let reactiveRoute;
const createWrapper = ({
  apiStatusCode = 200,
  mountFunction = shallowMount,
  rewards = null,
  vueDataOptions = {},
} = {}) => {
  const userAdmin = vueDataOptions.userAdmin || false;
  const userId = vueDataOptions.userId || 123;
  const username = vueDataOptions.username || 'lee';

  global.fetch = vi.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve(mockData),
      status: apiStatusCode,
    }),
  );

  wrapper = mountFunction(UserPage, {
    global: {
      plugins: [bootstrap],
      provide: { vueData: { userAdmin, userId, username } },
      stubs: { TableWithBusyState: false },
    },
  });

  mockData.rewards = rewards;
};

const mockMergeRequest = {
  title: 'Merge request title',
  opened_date: '2022-01-01',
  merged_date: '2022-01-02',
  web_url: 'https://gitlab.com/gitlab-org/gitlab/-/merge_requests/1',
};
const mockIssue = {
  title: 'Issue title',
  opened_date: '2022-01-01',
  closed_date: '2022-01-02',
  web_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/1',
};
const mockCommit = {
  sha: '0123456789',
  committed_date: '2022-01-01',
  merge_request_iid: 1,
  merge_request_title: 'Merge request title',
  merge_request_web_url: 'https://gitlab.com/gitlab-org/gitlab/-/merge_requests/1',
};
const mockNote = {
  added_date: '2022-01-01',
  noteable_title: 'Issue title',
  noteable_web_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/1',
};
const mockBonusPoints = {
  activity_type: 'other',
  reason: 'Legend!',
  created_at: '2022-01-01',
  points: 20,
};
const mockEvent = {
  activity_type: 'event',
  reason: 'Event https://gitlab.com/gitlab-org/developer-relations/contributor-success/rewards/-/issues/118',
  created_at: '2022-01-01',
  points: 500,
};
const mockDiscordMessage = {
  added_date: '2022-01-01',
  reply: true,
  channel_id: 'channel-1',
  web_url: 'https://discord.gg/gitlab',
};
const mockForumPost = {
  added_date: '2022-01-01',
  reply: true,
  topic: 'Hackathon results!',
  web_url: 'https://forum.gitlab.com',
};
const mockReward = {
  reason: 'notable_contributor',
  credits: 4,
  gift_code: 'XFDROUVJIWMTBNGY',
  issue_web_url: 'https://gitlab.com/gitlab-org/developer-relations/contributor-success/rewards/-/issues/957',
  awarded_by: 'leetickett-gitlab',
  awarded_by_web_url: 'https://gitlab.com/leetickett-gitlab',
  created_at: '2024-10-18T13:40:58.387Z',
};
const mockData = {
  id: 123,
  username: 'lee',
  contributor_level: 1,
  points: 100,
  merged_merge_requests: Array(1).fill(mockMergeRequest),
  opened_merge_requests: Array(2).fill(mockMergeRequest),
  merged_commits: Array(3).fill(mockCommit),
  opened_issues: Array(4).fill(mockIssue),
  added_notes: Array(5).fill(mockNote),
  bonus_points: [...Array(5).fill(mockBonusPoints), mockEvent],
  discord_messages: Array(6).fill(mockDiscordMessage),
  forum_posts: Array(7).fill(mockForumPost),
  rewards: [],
};

const findButton = (text) => {
  return wrapper.findAllComponents(BButton).find(button => button.text() === text);
};

beforeAll(() => {
  config.global.renderStubDefaultSlot = true;
});

beforeEach(() => {
  reactiveRoute = reactive({ params: { userHandle: '123' }, query: { } });
  vi.mock('vue-router', async () => {
    return {
      useRoute: vi.fn(() => reactiveRoute),
      useRouter: vi.fn(() => ({
        push: vi.fn(({ params, query }) => {
          reactiveRoute.query = { ...reactiveRoute.query, ...query };
          reactiveRoute.params = { ...reactiveRoute.params, ...params };
        }),
      })),
    };
  });
});

it('renders all tables with busy state', () => {
  createWrapper();

  const tables = wrapper.findAllComponents(BTable);
  tables.forEach((table) => {
    expect(table.props('busy')).toBe(true);
  });
});

const testTableFields = (tableIndex, expectedFields) => {
  it(`renders table ${tableIndex} with expected fields`, () => {
    const fieldLabels = wrapper.findAllComponents(PagedAccordionTable)[tableIndex].props('fields').map(field => field.label);
    expect(fieldLabels).toStrictEqual(expectedFields);
  });
};

testTableFields(0, ['Details', 'Added date', 'Points']);
testTableFields(1, ['Activity type', 'Reason', 'Awarded date', 'Points']);
testTableFields(2, ['Title', 'Open date', 'Merge date', 'Linked issue']);
testTableFields(3, ['Title', 'Open date', 'State', 'Linked issue']);
testTableFields(4, ['Merge request title', 'SHA', 'Commit date', 'Merge date']);
testTableFields(5, ['Title', 'Open date', 'State']);
testTableFields(6, ['Noteable title', 'Added date']);
testTableFields(7, ['Added date', 'Reply', 'Channel']);
testTableFields(8, ['Topic', 'Reply', 'Added date']);

const testTableSort = (tableIndex, expectedKey) => {
  it(`sorts table ${tableIndex} by expected key`, () => {
    const key = wrapper.findAllComponents(PagedAccordionTable)[tableIndex].props('sortKey');
    expect(key).toBe(expectedKey);
  });
};

testTableSort(0, 'created_at');
testTableSort(1, 'created_at');
testTableSort(2, 'merged_date');
testTableSort(3, 'opened_date');
testTableSort(4, 'committed_date');
testTableSort(5, 'opened_date');
testTableSort(6, 'added_date');
testTableSort(7, 'added_date');
testTableSort(8, 'added_date');

it('sends the correct default api query', () => {
  const mockDate = new Date(2024, 5, 29);
  vi.useFakeTimers();
  vi.setSystemTime(mockDate);

  createWrapper({ mountFunction: mount });

  const url = new URL('/api/v1/users/123', window.location.href);
  url.searchParams.append('from_date', '2024-03-31');
  url.searchParams.append('to_date', '');

  expect(global.fetch).toHaveBeenCalledWith(url);

  vi.useRealTimers();
});

it('renders default placeholders', () => {
  createWrapper({ mountFunction: mount });

  const placeholders = ['Events ()', 'Bonus Points ()', 'Merged Merge Requests ()', 'Opened Merge Requests ()', 'Commits ()', 'Opened Issues ()', 'Added Notes ()', 'Discord Messages ()', 'Forum Posts ()'];
  for (const placeholder of placeholders) {
    expect(wrapper.text()).toContain(placeholder);
  }
});

describe('with modified params', () => {
  it('sends the correct api query', async () => {
    createWrapper({ mountFunction: mount });

    wrapper.find('#from-date').setValue('2022-01-01');
    wrapper.find('#to-date').setValue('2023-01-01');
    await wrapper.findComponent(BButton).trigger('click');

    const url = new URL('/api/v1/users/123', window.location.href);
    url.searchParams.append('from_date', '2022-01-01');
    url.searchParams.append('to_date', '2023-01-01');

    expect(global.fetch).toHaveBeenCalledWith(url);
  });
});

describe('once loaded', () => {
  beforeEach(async () => {
    createWrapper({ mountFunction: mount });
  });

  it('renders all tables with not busy state', async () => {
    const tables = wrapper.findAllComponents(BTable);
    tables.forEach((table) => {
      expect(table.props('busy')).toBe(false);
    });
  });

  it('populates placeholders', () => {
    expect(wrapper.text()).toContain('Events (1)');
    expect(wrapper.text()).toContain('Bonus Points (5)');
    expect(wrapper.text()).toContain('Merged Merge Requests (1)');
    expect(wrapper.text()).toContain('Opened Merge Requests (2)');
    expect(wrapper.text()).toContain('Commits (3)');
    expect(wrapper.text()).toContain('Opened Issues (4)');
    expect(wrapper.text()).toContain('Added Notes (5)');
    expect(wrapper.text()).toContain('Discord Messages (6)');
  });

  describe('when regular user observes own profile', () => {
    beforeEach(async () => {
      await createWrapper({ mountFunction: mount, rewards: [] });
    });

    it('does not render edit user button', () => {
      expect(wrapper.text()).not.toContain('Edit user');
    });

    it('does not render add points button', () => {
      expect(wrapper.text()).not.toContain('Add points');
    });

    it('does not render issue reward button', () => {
      expect(wrapper.text()).not.toContain('Issue reward');
    });

    it('does not render add points modal', async () => {
      createWrapper();

      expect(wrapper.findComponent(AddPointsModal).exists()).toBe(false);
    });

    it('does not render issue reward modal', async () => {
      createWrapper();

      expect(wrapper.findComponent(IssueRewardModal).exists()).toBe(false);
    });

    it('renders level bar progress with expected props', async () => {
      const levelProgress = wrapper.findComponent(LevelProgress);

      expect(levelProgress.props('contributorData')).toMatchObject({
        contributorLevel: 1,
        contributorPoints: 100,
      });
    });
  });

  const testTableItems = (tableIndex, dataKey) => {
    it(`renders ${dataKey} table with expected items`, () => {
      const items = wrapper.findAllComponents(BTable)[tableIndex].props('items');

      // if dataKey is a string
      if (typeof dataKey === 'string') {
        expect(items).toStrictEqual(mockData[dataKey]);
      } else {
        expect(items).toStrictEqual(dataKey);
      }
    });
  };

  describe('when rewards returns an array', () => {
    beforeEach(() => {
      createWrapper({ mountFunction: mount, rewards: [] });
    });

    testTableItems(0, 'rewards');
  });

  describe('using the #add-event hash', () => {
    it('sets the modal true', async () => {
      location.hash = '#add-event';
      await createWrapper({ mountFunction: mount, vueDataOptions: { userAdmin: true } });

      await nextTick();
      await nextTick();

      const modal = await wrapper.findComponent(AddEventModal);
      expect(modal.props('modelValue')).toBe(true);
    });
  });

  describe('when admin', () => {
    beforeEach(async () => {
      await createWrapper({ mountFunction: mount, vueDataOptions: { userAdmin: true }, rewards: Array(8).fill(mockReward) });
    });

    it('renders edit user button', async () => {
      expect(wrapper.text()).toContain('Edit user');
    });

    it('renders add points button', async () => {
      expect(wrapper.text()).toContain('Add points');
    });

    it('clicking the add points button sets the modal true', async () => {
      const modal = wrapper.findComponent(AddPointsModal);
      expect(modal.props('modelValue')).toBe(false);

      await findButton('Add points').trigger('click');

      expect(modal.props('modelValue')).toBe(true);
    });

    it('renders issue reward button', async () => {
      expect(wrapper.text()).toContain('Issue reward');
    });

    it('opens event link in a new tab', async () => {
      const link = 'https://gitlab.com/gitlab-org/developer-relations/contributor-success/rewards/-/issues/118';
      expect(wrapper.html()).toContain(`<a target="_blank" href="${link}">${link}</a>`);
    });

    it('clicking the issue rewards button sets the modal true', async () => {
      const modal = wrapper.findComponent(IssueRewardModal);
      expect(modal.props('modelValue')).toBe(false);

      await findButton('Issue reward').trigger('click');

      expect(modal.props('modelValue')).toBe(true);
    });

    it('clicking the add event button sets the modal true', async () => {
      const modal = wrapper.findComponent(AddEventModal);
      expect(modal.props('modelValue')).toBe(false);

      await findButton('Add event').trigger('click');
      expect(modal.props('modelValue')).toBe(true);
    });

    describe('when not own profile', () => {
      it('does not render add event button and modal', async () => {
        createWrapper({ vueDataOptions: { userId: 999 } });

        expect(findButton('Add event')).toBe(undefined);
        expect(wrapper.findComponent(AddEventModal).exists()).toBe(false);
      });
    });

    testTableFields(0, ['Reason', 'Reward issue', 'Credits', 'Issued by', 'Issued date']);

    testTableItems(0, 'rewards');
  });

  testTableItems(0, [mockEvent]);
  testTableItems(1, mockData['bonus_points'].slice(0, -1));
  testTableItems(2, 'merged_merge_requests');
  testTableItems(3, 'opened_merge_requests');
  testTableItems(4, 'merged_commits');
  testTableItems(5, 'opened_issues');
  testTableItems(6, 'added_notes');
  testTableItems(7, 'discord_messages');
  testTableItems(8, 'forum_posts');

  it('renders username link and contributorLevelBadge', () => {
    const contributorLevelBadge = wrapper.findComponent(ContributorLevelBadge);

    const usernameLink = wrapper.find('a[href="https://gitlab.com/lee"]');
    expect(usernameLink.text()).toBe('lee');

    expect(contributorLevelBadge.props('contributorLevel')).toBe(1);
  });
});

describe('when route userHandle = "me"', () => {
  beforeEach(() => {
    const mockDate = new Date(2024, 5, 29);
    vi.useFakeTimers();
    vi.setSystemTime(mockDate);

    reactiveRoute.params.userHandle = 'me';

    createWrapper({ mountFunction: mount });
  });

  it('updates route using logged in username', () => {
    expect(reactiveRoute.params.userHandle).toBe('lee');
  });

  it('makes API call using logged in username', () => {
    const url = new URL('/api/v1/users/lee', window.location.href);
    url.searchParams.append('from_date', '2024-03-31');
    url.searchParams.append('to_date', '');

    expect(global.fetch).toHaveBeenCalledWith(url);
  });
});

describe('when route userHandle is a username', () => {
  beforeEach(() => {
    const mockDate = new Date(2024, 5, 29);
    vi.useFakeTimers();
    vi.setSystemTime(mockDate);

    reactiveRoute.params.userHandle = 'lee';

    createWrapper({ mountFunction: mount });
  });

  it('makes API call using username', () => {
    const url = new URL('/api/v1/users/lee', window.location.href);
    url.searchParams.append('from_date', '2024-03-31');
    url.searchParams.append('to_date', '');

    expect(global.fetch).toHaveBeenCalledWith(url);
  });
});

describe('when user not found', () => {
  it('renders user not found message', async () => {
    await createWrapper({ apiStatusCode: 404, mountFunction: mount });

    await nextTick();

    expect(wrapper.text()).toBe('User not found.If the user has no recent activity, or has not yet been imported, you can view their GitLab profile directly.');
  });
});

describe('when server error', () => {
  it('renders something went wrong message', async () => {
    await createWrapper({ apiStatusCode: 500, mountFunction: mount });

    await nextTick();

    expect(wrapper.text()).toBe('Something went wrong.Please try again later. If the problem persists, please raise an issue in the contributors-gitlab-com issue tracker.');
  });
});

describe('when navigating to a different user', () => {
  beforeEach(() => {
    const mockDate = new Date(2024, 5, 29);
    vi.useFakeTimers();
    vi.setSystemTime(mockDate);
  });

  afterEach(() => {
    vi.useRealTimers();
  });

  it('makes expeceted API query', async () => {
    createWrapper({ mountFunction: mount });

    let url = new URL('/api/v1/users/123', window.location.href);
    url.searchParams.append('from_date', '2024-03-31');
    url.searchParams.append('to_date', '');

    expect(global.fetch).toHaveBeenCalledWith(url);

    reactiveRoute.params = { userHandle: 'lee' };

    await nextTick();

    url = new URL('/api/v1/users/lee', window.location.href);
    url.searchParams.append('from_date', '2024-03-31');
    url.searchParams.append('to_date', '');

    expect(global.fetch).toHaveBeenCalledWith(url);
  });
});
