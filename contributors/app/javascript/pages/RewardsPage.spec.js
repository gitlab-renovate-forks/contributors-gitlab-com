import { mount, shallowMount } from '@vue/test-utils';
import { BButton, BTable, createBootstrap } from 'bootstrap-vue-next';

import IssueRewardModal from '~/common/components/IssueRewardModal.vue';

import RewardsPage from './RewardsPage.vue';

const bootstrap = createBootstrap();
let wrapper;
const createWrapper = ({ userAdmin = false, mountFunction = shallowMount } = {}) => {
  wrapper = mountFunction(RewardsPage, {
    global: {
      plugins: [bootstrap],
      renderStubDefaultSlot: true,
      provide: { vueData: { userAdmin } },
    },
  });
};

const findIssueRewardsButton = () => wrapper.findByTestId('issue-rewards-button');

const mockData = {
  records: [
    {
      username: 'leetickett-gitlab',
      user_id: 12687636,
      reason: 'first_contribution',
      credits: 100,
      gift_code: 'IBWPEOTVDUMGZQCNAYFJ',
      issue_web_url: 'https://gitlab.com/gitlab-org/developer-relations/contributor-success/rewards/-/issues/51',
      awarded_by: 'leetickett-gitlab',
      awarded_by_web_url: 'https://gitlab.com/leetickett-gitlab',
      created_at: '2024-10-26T08:05:25.545Z',
    },
  ],
  metadata: {
    current_page: 3,
    per_page: 50,
    total_count: 101,
  },
};

describe('on mount', () => {
  beforeEach(() => {
    global.fetch = vi.fn(() => new Promise(() => {}));

    vi.mock('vue-router', async () => {
      return {
        useRoute: vi.fn(() => ({ query: {} })),
        useRouter: vi.fn(() => ({
          push: vi.fn(),
        })),
      };
    });

    createWrapper({ mountFunction: mount });
  });

  it('renders reward store messaging and link', () => {
    const rewardStoreLink = wrapper.find('a[role="button"]');
    expect(wrapper.text()).toContain(
      'Earn credits by contributing, levelling up, and supporting the community. Redeem your credits for rewards you\'ll love.');
    expect(rewardStoreLink.attributes('href')).toBe('https://rewards.gitlab.com/');
  });

  it('renders BTable with expected fields', () => {
    const fieldLabels = wrapper.findComponent(BTable).props('fields').map(field => field.label);
    expect(fieldLabels).toStrictEqual(['Username', 'Reason', 'Credits', 'Issued by', 'Issued date']);
  });

  it('sends the correct default api query', () => {
    const url = new URL('/api/v1/rewards', window.location.href);
    url.searchParams.append('page', 1);

    expect(global.fetch).toHaveBeenCalledWith(url);
  });

  it('does not render issue rewards button', () => {
    expect(findIssueRewardsButton().exists()).toBe(false);
  });
});

describe('when admin', () => {
  beforeEach(() => {
    global.fetch = vi.fn(() => new Promise(() => {}));

    createWrapper({ userAdmin: true });
  });

  it('renders issue rewards button', () => {
    expect(findIssueRewardsButton().exists()).toBe(true);
  });

  it('renders BTable with expected fields', () => {
    const fieldLabels = wrapper.findComponent(BTable).props('fields').map(field => field.label);
    expect(fieldLabels).toStrictEqual(['Username', 'Reason', 'Reward issue', 'Credits', 'Issued by', 'Issued date']);
  });

  describe('once loaded', () => {
    it('clickling the issue rewards button sets the modal true', async () => {
      const modal = wrapper.findComponent(IssueRewardModal);
      expect(modal.props('modelValue')).toBe(false);

      await wrapper.findComponent(BButton).trigger('click');

      expect(modal.props('modelValue')).toBe(true);
    });
  });
});

describe('once loaded', () => {
  beforeEach(async () => {
    global.fetch = vi.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve(mockData),
      }),
    );

    createWrapper({ mountFunction: mount });
  });

  it('renders BTable with expected items', () => {
    const items = wrapper.findComponent(BTable).props('items');
    expect(items).toStrictEqual(mockData.records);
  });
});
