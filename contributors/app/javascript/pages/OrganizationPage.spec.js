import { mount } from '@vue/test-utils';
import { createBootstrap } from 'bootstrap-vue-next';
import { reactive } from 'vue';

import ContributorsTable from '~/common/components/ContributorsTable.vue';
import FilterCard from '~/common/components/FilterCard.vue';
import { daysAgo } from '~/common/DateUtils';

import OrganizationPage from './OrganizationPage.vue';

const bootstrap = createBootstrap();
let wrapper;
const reactiveRoute = reactive({ query: {}, params: { organizationName: 'GitLab' } });
const createWrapper = () => {
  wrapper = mount(OrganizationPage, {
    global: { plugins: [bootstrap] },
  });
};

const mockResponse = {
  name: 'GitLab',
  contributors: [
    {
      user_id: 5211906,
      username: 'csouthard',
      merged_merge_requests: 4,
      merged_with_issues: 1,
      opened_merge_requests: 4,
      opened_issues: 0,
      merged_commits: 4,
      added_notes: 0,
      score: 430,
      contributor_level: 1,
      bonus_points: 0,
    },
    {
      user_id: 8198564,
      username: 'rkadam3',
      merged_merge_requests: 2,
      merged_with_issues: 0,
      opened_merge_requests: 3,
      opened_issues: 0,
      merged_commits: 2,
      added_notes: 1,
      score: 221,
      contributor_level: 1,
      bonus_points: 0,
    },
  ],
};

beforeEach(async () => {
  global.fetch = vi.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve(mockResponse),
    }),
  );

  vi.mock('vue-router', async () => {
    return {
      useRoute: vi.fn(() => reactiveRoute),
      useRouter: vi.fn(() => ({
        push: vi.fn(({ query }) => {
          reactiveRoute.query = { ...reactiveRoute.query, ...query };
        }),
      })),
    };
  });

  await createWrapper();
});

it('renders FilterCard', () => {
  expect(wrapper.findComponent(FilterCard).exists()).toBe(true);
});

it('renders ContributorsTable with expected props', () => {
  const contributorsTable = wrapper.findComponent(ContributorsTable);

  expect(contributorsTable.exists()).toBe(true);
  expect(contributorsTable.props('contributors')).toEqual(mockResponse.contributors);
  expect(contributorsTable.props('totals')).toEqual({
    merged_merge_requests: 6,
    merged_with_issues: 1,
    opened_merge_requests: 7,
    opened_issues: 0,
    merged_commits: 6,
    added_notes: 1,
    bonus_points: 0,
    score: 651,
  });
  expect(contributorsTable.props('filterData')).toMatchObject({
    fromDate: daysAgo(90),
    toDate: '',
    communityOnly: true,
    search: '',
  });
  expect(contributorsTable.props('busy')).toEqual(false);
});
