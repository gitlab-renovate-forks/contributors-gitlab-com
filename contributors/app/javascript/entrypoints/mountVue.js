import { ApolloClient, InMemoryCache, createHttpLink } from '@apollo/client/core';
import { DefaultApolloClient } from '@vue/apollo-composable';
import { createBootstrap } from 'bootstrap-vue-next';
import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';

// bootstrap-vue-next
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue-next/dist/bootstrap-vue-next.css';

import RootApp from '~/App.vue';
import routes from '~/routes';

const httpLink = createHttpLink({
  uri: 'https://gitlab.com/api/graphql',
});

const apolloClient = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache(),
});

const router = createRouter({
  history: createWebHistory('/'),
  routes,
});
const bootstrap = createBootstrap({ directives: true });

const el = document.getElementById('app');
const vueData = JSON.parse(el.getAttribute('data'));

createApp(RootApp)
  .provide(DefaultApolloClient, apolloClient)
  .provide('vueData', vueData)
  .use(router)
  .use(bootstrap)
  .mount('#app');
