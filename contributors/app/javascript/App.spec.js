import { mount, shallowMount } from '@vue/test-utils';
import { BContainer } from 'bootstrap-vue-next';
import { createRouter, createMemoryHistory } from 'vue-router';

import Component from './App.vue';
import FooterNote from './common/components/FooterNote.vue';
import NavigationMenu from './common/components/NavigationMenu.vue';
import NotFoundPage from './pages/NotFoundPage.vue';

let router;
let wrapper;

const createWrapper = async ({
  mountFunction = shallowMount,
  routeName = 'home',
  userId = null,
  userAdmin = false,
} = {}) => {
  const component = { template: '<div />' };
  const footerNote = { customText: 'Hello!', learnMoreHref: 'www.example.com' };

  router = createRouter({
    history: createMemoryHistory(),
    routes: [
      { path: '/', name: 'home', component },
      { path: '/users/me', name: 'profile', component, meta: { requireLogin: true } },
      { path: '/merge-request', name: 'manage-merge-request', component, meta: { requireAdmin: true } },
      { path: '/organizations', name: 'organizations', component, meta: { footerNote } },
    ],
  });

  wrapper = mountFunction(Component, {
    global: {
      plugins: [router],
      provide: { vueData: { userId, userAdmin } },
      stubs: {
        NavigationMenu: { template: '<div>NavigationMenu</div>' },
      },
    },
  });

  router.push({ name: routeName });
  await router.isReady();

  delete window.location;
  window.location = {
    href: '',
  };
};

it('renders NavigationMenu', async () => {
  await createWrapper();

  expect(wrapper.findComponent(NavigationMenu).exists()).toBe(true);
});

it('renders BContainer', async () => {
  await createWrapper();

  expect(wrapper.findComponent(BContainer).exists()).toBe(true);
});

it('does not render the not found page component', async () => {
  await createWrapper({ mountFunction: mount });

  expect(wrapper.findComponent(NotFoundPage).exists()).toBe(false);
});

describe('when navigating to a route that does not exist', () => {
  it('renders the not found page component', async () => {
    await createWrapper({ mountFunction: mount });

    await router.push({ path: '/not-found' });

    expect(wrapper.findComponent(NotFoundPage).exists()).toBe(true);
  });
});

describe('with meta.requireLogin', () => {
  describe('when not logged in', () => {
    it('redirects to /login', async () => {
      await createWrapper();

      await router.push({ name: 'profile' });

      expect(window.location.href).toBe('/login?return=/users/me');
    });
  });

  describe('when logged in', () => {
    it('does not redirect', async () => {
      await createWrapper({ userId: 1 });

      await router.push({ name: 'profile' });

      expect(window.location.href).toBe('');
      expect(router.currentRoute.value.fullPath).toBe('/users/me');
    });
  });
});

describe('with meta.requireAdmin', () => {
  describe('when not logged in', () => {
    it('redirects to /login', async () => {
      await createWrapper();

      await router.push({ name: 'manage-merge-request' });

      expect(window.location.href).toBe('/login?return=/merge-request');
    });
  });

  describe('when logged in as non admin', () => {
    it('redirects to /', async () => {
      await createWrapper({ userId: 1 });

      await router.push({ name: 'manage-merge-request' });

      expect(window.location.href).toBe('');
      expect(router.currentRoute.value.fullPath).toBe('/');
    });
  });

  describe('when not logged in as admin', () => {
    it('does not redirect', async () => {
      await createWrapper({ userId: 1, userAdmin: true });

      await router.push({ name: 'manage-merge-request' });

      expect(window.location.href).toBe('');
      expect(router.currentRoute.value.fullPath).toBe('/merge-request');
    });
  });
});

describe('with FooterNote', () => {
  it('renders without learn more link', async () => {
    await createWrapper({ mountFunction: mount });

    const footerNote = wrapper.findComponent(FooterNote);
    expect(footerNote.exists()).toBe(true);
    expect(footerNote.props('learnMoreHref')).toBe(null);
  });

  it('renders with learn more link if injected', async () => {
    await createWrapper({ mountFunction: mount, routeName: 'organizations' });

    const footerNote = wrapper.findComponent(FooterNote);
    expect(footerNote.exists()).toBe(true);
    expect(footerNote.props('learnMoreHref')).toEqual('www.example.com');
    expect(footerNote.props('customText')).toEqual('Hello!');
  });
});
