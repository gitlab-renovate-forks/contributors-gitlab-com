import routes from './routes';

describe('meta.title', () => {
  const routeNames = val => routes.filter(({ meta: { title } = {} }) => val ? title : !title).map(route => route.name);

  it('contains the expected titled routes', () => {
    expect(routeNames(true)).toEqual([
      'leaderboard', 'manage-merge-request', 'rewards', 'organizations', 'documentation',
    ]);
  });

  it('contains the expected untitled routes', () => {
    expect(routeNames(false)).toEqual([
      'home', 'user', 'user-edit', 'organization',
    ]);
  });
});

describe('meta.requireLogin', () => {
  const routeNames = val => routes.filter(({ meta: { requireLogin } = {} }) => val ? requireLogin : !requireLogin).map(route => route.name);

  it('contains the expected routes requiring login', () => {
    expect(routeNames(true)).toEqual(['manage-merge-request']);
  });

  it('contains the expected routes not requiring login', () => {
    expect(routeNames(false)).toEqual([
      'home', 'leaderboard', 'rewards', 'user', 'user-edit', 'organizations', 'organization', 'documentation',
    ]);
  });
});

describe('meta.requireAdmin', () => {
  const routeNames = val => routes.filter(({ meta: { requireAdmin } = {} }) => val ? requireAdmin : !requireAdmin).map(route => route.name);

  it('contains the expected routes requiring admin', () => {
    expect(routeNames(true)).toEqual([
      'user-edit',
    ]);
  });

  it('contains the expected routes not requiring admin', () => {
    expect(routeNames(false)).toEqual([
      'home', 'leaderboard', 'manage-merge-request', 'rewards', 'user', 'organizations', 'organization', 'documentation',
    ]);
  });
});

it('contains the expected routes with custom footer note', () => {
  const withCustomFooter = routes.filter(({ meta = {} }) => meta.footerNote).map(route => route.name);

  expect(withCustomFooter).toEqual(['organizations', 'organization']);
});
