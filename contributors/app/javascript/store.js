import { reactive } from 'vue';

export const store = reactive({
  theme: 'light',
  setTheme(newTheme) {
    this.theme = newTheme;
  },
});
