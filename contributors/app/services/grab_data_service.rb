# frozen_string_literal: true

require 'gitlab/username_bot_identifier'

class GrabDataService # rubocop:disable Metrics/ClassLength
  include GraphqlHelper

  EARLIEST_DATE = Date.new(2024, 1, 1)
  GROUPS = %w[
    gitlab-com
    gitlab-org
    gitlab-community
    components
  ].freeze
  ISSUABLE_TYPES = %w[
    issues
    mergeRequests
  ].freeze

  def execute
    GROUPS.each { |group| process_group(group) }
  end

  def process_group(group)
    ISSUABLE_TYPES.each { |issuable_type| process_issuable_type(group, issuable_type) }
  end

  def process_issuable_type(group, issuable_type)
    activity_date = next_date(group, issuable_type)
    processed_records = 0
    start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    after = nil

    loop do
      result = process_page(group, issuable_type, activity_date, after)
      processed_records += result[:processed_records]

      break unless result[:has_next_page]

      after = result[:after]
    end

    elapsed = Process.clock_gettime(Process::CLOCK_MONOTONIC) - start_time
    Rails.logger.info do
      "[#{group} / #{issuable_type} / #{activity_date}] Processed #{processed_records} in #{elapsed}s"
    end
    ProcessedJob.create!(group:, issuable_type:, activity_date:, processed_records:, elapsed:)
  end

  def next_date(group, issuable_type)
    return Time.zone.yesterday if ENV.fetch('MOP_UP', '0') == '1'
    return Time.zone.today if incremental?

    last_activity = ProcessedJob.where(group:, issuable_type:).order(:activity_date, :created_at).last
    return EARLIEST_DATE if last_activity.nil?

    date = last_activity.activity_date
    return date if date.today?
    return date if date.yesterday? && last_activity.updated_at.yesterday?

    date + 1.day
  end

  def process_page(group, issuable_type, activity_date, after)
    page = fetch_page(group, issuable_type, activity_date, after)
    nodes = page.dig('data', 'group', issuable_type, 'nodes')
    page_info = page.dig('data', 'group', issuable_type, 'pageInfo')

    process_nodes(nodes, issuable_type)

    {
      processed_records: nodes.count,
      after: page_info['endCursor'],
      has_next_page: page_info['hasNextPage']
    }
  end

  def process_nodes(nodes, issuable_type)
    users = {}
    issuables = []
    commits = []
    notes = []

    nodes.each do |node|
      result = process_node(node, issuable_type)

      users.merge!(result[:users])
      issuables << result[:issuable]
      commits.concat(result[:commits])
      notes.concat(result[:notes])
    end

    upsert_records(User, users.values)
    upsert_records(issuable_type == 'issues' ? Issue : MergeRequest, issuables)
    purge_commits(issuables) unless issuable_type == 'issues'
    upsert_records(Commit, commits.uniq { |commit| commit[:sha] }, :sha)
    upsert_records(Note, notes.uniq { |note| note[:id] })
  end

  def purge_commits(issuables)
    # TODO: We shouldn't need this now we only pull commits for merged merge requests?
    Commit.where(merge_request_id: issuables.pluck(:id)).delete_all
  end

  def process_node(node, issuable_type)
    author = process_user(node['author'])
    issuable = process_issuable(node, issuable_type, author)
    commits = process_commits(node, issuable[:id])
    notes = process_notes(node, issuable_type)
    users = notes[:users].merge(commits[:users])

    users[author[:username]] = author
    { users:, issuable:, notes: notes[:notes], commits: commits[:commits] }
  end

  def process_commits(node, merge_request_id)
    # TODO: Should we include closed too?
    return { commits: [], users: {} } unless node['state'] == 'merged'

    users = {}
    commits = []

    node.dig('commits', 'nodes').each do |commit|
      next if commit['author'].nil?

      author = process_user(commit['author'])
      users[author[:username]] = author
      commits << {
        sha: commit['sha'],
        committed_date: DateTime.parse(commit['committedDate']),
        merge_request_id:,
        user_id: author[:id]
      }
    end

    { commits:, users: }
  end

  def process_notes(issuable_node, issuable_type)
    users = {}
    notes = []

    issuable_id = id_from_graphql_id(issuable_node['id'])
    issuable_node.dig('notes', 'nodes').each do |node|
      result = process_note(node, issuable_type, issuable_id)
      next if result[:note][:added_date] < EARLIEST_DATE

      author = result[:author]
      users[author[:username]] = author
      notes << result[:note]
    end

    { users:, notes: }
  end

  def process_note(node, issuable_type, issuable_id)
    author = process_user(node['author'])

    note = {
      id: id_from_graphql_id(node['id']),
      user_id: author[:id],
      issue_id: issuable_type == 'issues' ? issuable_id : nil,
      merge_request_id: issuable_type == 'issues' ? nil : issuable_id,
      added_date: DateTime.parse(node['createdAt'])
    }

    { author:, note: }
  end

  def process_user(user)
    id = id_from_graphql_id(user['id'])
    username = user['username']
    organization = user['organization']
    community_member =
      user['state'] != 'blocked' &&
      !username.end_with?('-ext') &&
      !Gitlab::UsernameBotIdentifier.new(username).ignorable_account? &&
      team_members.exclude?(username.downcase)

    { id:, username:, community_member:, organization: }
  end

  def team_members
    @team_members ||= TeamMemberService.new.team_members
  end

  def process_issuable(node, issuable_type, author)
    {
      id: id_from_graphql_id(node['id']),
      title: node['title'],
      state_id: node['state'],
      user_id: author[:id],
      project_id: nil, # project_id
      web_url: node['webUrl'],
      opened_date: DateTime.parse(node['createdAt']),
      upvotes: node['upvotes'],
      labels: node['labels']['nodes'].pluck('title'),
      **additional_props(node, issuable_type)
    }
  end

  def additional_props(node, issuable_type)
    if issuable_type == 'issues'
      closed_at = node['closedAt']
      { closed_date: closed_at.nil? ? nil : DateTime.parse(closed_at) }
    else
      merged_at = node['mergedAt']
      { merged_date: merged_at.nil? ? nil : DateTime.parse(merged_at) }
    end
  end

  def upsert_records(type, records, key = nil)
    type.upsert_all(records, unique_by: key || :id)
  end

  def fetch_page(group, issuable_type, activity_date, after)
    page_size = ENV.fetch('PAGE_SIZE', 20).to_i

    loop do
      query = graphql_query(group, issuable_type, activity_date, after, page_size)
      result = graphql_service.execute(query)
      nodes = result.dig('data', 'group', issuable_type, 'nodes')
      return result unless nodes.nil?

      Rails.logger.error { "nodes are nil- response was:\n\n#{result}" }
      page_size /= 2
      return if page_size < 1
    end
  end

  def graphql_service
    @graphql_service ||= GitlabGraphqlService
  end

  def graphql_query(group_name, issuable_type, activity_date, after, page_size = 20)
    updated_before = activity_date.today? ? '' : "updatedBefore: \"#{activity_date + 1.day}\""
    activity_date = 11.minutes.ago.strftime('%Y-%m-%d %H:%M') if incremental?
    Rails.logger.info { "Getting #{issuable_type} for #{group_name} since #{activity_date} after #{after}" }
    additional_fields = issuable_type == 'issues' ? 'closedAt' : <<~GRAPHQL
      mergedAt
      commits {
        nodes {
          sha
          committedDate
          author {
            id
            username
            state
            organization
          }
        }
      }
    GRAPHQL

    <<~GRAPHQL
      query {
        group(fullPath: "#{group_name}") {
          #{issuable_type}(
            includeSubgroups: true
            # TODO: If it's "today" we probably need to go back X hours from "now" or 1.day?
            # Otherwise we might miss some events at "the end of the day"
            updatedAfter: "#{activity_date}"
            #{updated_before}
            after: "#{after}"
            first: #{page_size}
          ) {
            nodes {
              id
              title
              state
              createdAt
              webUrl
              upvotes
              #{additional_fields}
              author {
                id
                username
                state
                organization
              }
              labels {
                nodes {
                  title
                }
              }
              notes(
                filter: ONLY_COMMENTS
                last: 100
              ) {
                nodes {
                  id
                  createdAt
                  author {
                    id
                    username
                    state
                    organization
                  }
                }
              }
            }
            pageInfo {
              endCursor
              hasNextPage
            }
          }
        }
      }
    GRAPHQL
  end
end

def incremental?
  @incremental ||= ENV.fetch('INCREMENTAL', '0') == '1'
end
