# frozen_string_literal: true

require 'httparty'

class DiscourseService
  BASE_URL = 'https://forum.gitlab.com'

  def initialize
    @user_map = {}
  end

  def fetch_posts(start_date, end_date, page = 0)
    query = {
      q: "after:#{start_date} before:#{end_date}",
      page:
    }

    response = HTTParty.get("#{BASE_URL}/search.json", query:)

    return response.parsed_response unless response.code == 429

    sleep(response.headers['retry-after'].to_i)
    fetch_posts(start_date, end_date, page)
  end

  def fetch_user_id(username)
    return @user_map[username] if @user_map.key?(username)

    @user_map[username] = nil
    response = HTTParty.get("#{BASE_URL}/u/#{username}.json")
    gitlab_username = response.parsed_response.dig('user', 'user_fields', '2')
    return unless gitlab_username

    @user_map[username] = User.find_by(username: gitlab_username)&.id
  end

  def process_posts(posts, topics_map)
    posts.map do |post|
      user_id = fetch_user_id(post['username'])
      next unless user_id

      topic_id = post['topic_id']
      {
        id: post['id'],
        user_id:,
        post_number: post['post_number'],
        reply: post['post_number'] > 1,
        topic_id:,
        topic_title: topics_map[topic_id],
        added_date: DateTime.parse(post['created_at'])
      }
    end.compact
  end

  def fetch_all_posts(start_date, end_date)
    all_posts = []
    page = 1

    loop do
      result = fetch_posts(start_date, end_date, page)
      posts = result['posts']
      break if posts.empty?

      topic_map = result['topics'].to_h { |topic| [topic['id'], topic['title']] }
      posts = process_posts(posts, topic_map)

      all_posts.concat(posts)
      break unless result.dig('grouped_search_result', 'more_full_page_results')

      Rails.logger.info { "Page: #{page} (record: #{all_posts.size})" }
      page += 1
    end

    Rails.logger.info { "Upserting #{all_posts.size} records" }
    ForumPost.upsert_all(all_posts) unless all_posts.empty?
  end
end
