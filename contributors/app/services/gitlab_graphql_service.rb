# frozen_string_literal: true

require 'httparty'

class GitlabGraphqlService
  CI_API_GRAPHQL_URL = 'https://gitlab.com/api/graphql'
  MAX_TRIES = 5

  class << self
    def execute(query, with_retries: true)
      response = post_query_with_retries(query, max_tries: with_retries ? MAX_TRIES : 1)
      response.parsed_response
    end

    private

    def post_query_with_retries(query, max_tries:)
      attempt = 1
      begin
        response = post_query(query)
        return response if response.code == 200

        raise "Received a non-default error code #{response.code}"
      rescue => e # rubocop:disable Style/RescueStandardError
        raise if attempt >= max_tries

        Rails.logger.warn { "Fetch retry ##{attempt} due to error: #{e.message}" }

        sleep_time = [2**(attempt - 1), 30].min + rand(0.1..0.5)
        sleep(sleep_time)
        attempt += 1

        retry
      end
    end

    def post_query(query)
      HTTParty.post(
        graphql_api_url,
        body: { query: }.to_json,
        headers: {
          'Content-Type' => 'application/json',
          'Cookie' => cookies,
          'User-Agent' => 'contributorsDot'
        }.merge(authorization_header)
      )
    end

    def graphql_api_url
      ENV.fetch('CI_API_GRAPHQL_URL', CI_API_GRAPHQL_URL)
    end

    def authorization_header
      gitlab_api_token = ENV.fetch('GITLAB_API_TOKEN', nil)
      return {} if gitlab_api_token.nil?

      { 'Authorization' => "Bearer #{gitlab_api_token}" }
    end

    def cookies
      cookie_hash = HTTParty::CookieHash.new
      cookie_hash.add_cookies('gitlab_canary=true')
      cookie_hash.to_cookie_string
    end
  end
end
