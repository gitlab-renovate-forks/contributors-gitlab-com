# frozen_string_literal: true

module Events
  class CreateService
    AWARD_POINTS = 500

    def initialize(session_user, params)
      @session_user = session_user
      @params = params
    end

    # We don't update views_activity_summaries here because
    # this action is performed by regular user
    # and refreshing materialized view is high-cost operation
    # to allow any user on the platform execute it.
    def execute
      issue = create_issue
      award_bonus_points(issue)
      issue
    end

    private

    attr_reader :session_user, :params

    def create_issue
      GitlabRestService.new(session_user[:access_token])
        .create_event_issue(
          session_user[:id],
          params[:name],
          event_issue_description
        )
    end

    def event_issue_description
      <<~TEXT
        - Date: #{params[:date]}
        - Link: #{params[:link].presence || '-'}
        - Role: #{params[:role]}
        - Virtual: #{params[:virtual]}
        - Size: #{size_in_words}
        - Note: #{params[:note].presence || '-'}

        /cc @gitlab-org/developer-relations/contributor-success
      TEXT
    end

    def size_in_words
      {
        small: 'Small (< 20)',
        medium: 'Medium (20 - 100)',
        large: 'Large (100+)'
      }[params[:size].to_sym]
    end

    def award_bonus_points(issue)
      BonusPoints.create!(
        user_id: session_user[:id],
        points: AWARD_POINTS,
        reason: "Community event #{params[:role]}. Issue: #{issue[:web_url]}",
        activity_type: :event,
        awarded_by_user_id: session_user[:id]
      )
    end
  end
end
