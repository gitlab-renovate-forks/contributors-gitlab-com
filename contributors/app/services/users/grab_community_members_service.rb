# frozen_string_literal: true

module Users
  class GrabCommunityMembersService
    include GraphqlHelper

    def execute
      ActiveRecord::Base.transaction do
        CommunityMember.delete_all
        CommunityMember.insert_all(community_members)
      end
    end

    def community_members
      after = nil
      members = []

      loop do
        page = fetch_page(after)
        nodes = page['nodes']
        page_info = page['pageInfo']

        members += nodes.map { { user_id: id_from_graphql_id(it.dig('user', 'id')) } }
        break unless page_info['hasNextPage']

        after = page_info['endCursor']
      end

      members
    end

    def fetch_page(after)
      result = graphql_service.execute(query(after))
      group_members = result&.dig('data', 'group', 'groupMembers')
      return group_members unless group_members&.[]('nodes').nil?

      raise StandardError, result
    end

    def graphql_service
      @graphql_service ||= GitlabGraphqlService
    end

    def query(after)
      <<~GRAPHQL
        query {
          group(fullPath: "gitlab-community/community-members"){
            groupMembers(
              relations: DIRECT
              after: "#{after}"
            ) {
              nodes {
                user {
                  id
                }
              }
              pageInfo {
                hasNextPage
                endCursor
              }
            }
          }
        }
      GRAPHQL
    end
  end
end
