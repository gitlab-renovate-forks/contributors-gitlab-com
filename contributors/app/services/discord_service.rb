# frozen_string_literal: true

require 'httparty'

class DiscordService
  BASE_URL = 'https://discord.com/api/v10'

  def initialize
    token = ENV.fetch('DISCORD_API_TOKEN')
    @headers = {
      'Authorization' => "Bot #{token}",
      'Content-Type' => 'application/json'
    }
    @end_cursor = ''
    @added_date = ''
    @discord_users = User.where.not(discord_id: nil).pluck(:discord_id, :id).to_h
  end

  def fetch_all_messages(channel_id, weeks)
    channel = DiscordMessage::CHANNELS[channel_id]

    loop do
      messages = fetch_messages(channel_id)
      thread_count = messages.count { |msg| msg[:thread_id] }
      reply_count = messages.count - thread_count
      Rails.logger.info { "[#{channel}] Fetched #{thread_count} threads and #{reply_count} replies to #{@added_date}" }

      records_to_insert = messages.filter_map do |msg|
        next unless msg[:user_id]

        {
          user_id: msg[:user_id],
          channel_id: msg[:channel_id],
          added_date: msg[:added_date],
          id: msg[:id],
          reply: msg[:reply]
        }
      end

      DiscordMessage.upsert_all(records_to_insert)
      break if @added_date.nil? || Date.parse(@added_date) < (Time.zone.today - weeks.weeks)
    end
  end

  def fetch_messages(channel_id, limit: 10, threads: false)
    options = build_request_options(limit, threads)
    response = make_api_request(channel_id, options)
    messages = parse_messages(response, threads)

    return messages if threads

    last_record = response.parsed_response.last
    @end_cursor = last_record['id']
    @added_date = last_record['timestamp']

    fetch_thread_messages(messages)
  rescue StandardError
    Rails.logger.info { response }
  end

  def build_request_options(limit, threads)
    query = { limit: }
    query.merge!(before: @end_cursor) if @end_cursor.present? && !threads

    {
      headers: @headers,
      query:
    }
  end

  def make_api_request(channel_id, options)
    response = HTTParty.get("#{BASE_URL}/channels/#{channel_id}/messages", options)

    return response unless response.code == 429

    sleep(response.headers['retry-after'].to_i)
    make_api_request(channel_id, options)
  end

  def parse_messages(response, threads)
    response.parsed_response
      .map { |msg| build_message_hash(msg, threads) }
  end

  def build_message_hash(msg, threads)
    {
      id: msg['id'],
      channel_id: msg['channel_id'],
      user_id: @discord_users[msg['author']['id']],
      added_date: msg['timestamp'],
      thread_id: msg.dig('thread', 'id'),
      reply: threads,
      has_replies: msg['thread'] && msg.dig('thread', 'message_count') > 1
    }
  end

  def fetch_thread_messages(messages)
    thread_messages = messages.filter_map do |msg|
      fetch_messages(msg[:thread_id], threads: true) if msg[:thread_id] && msg[:has_replies]
    end
    messages + thread_messages.flatten
  end

  def discord_user_in_database(message)
    discord_id = message['author']['id']
    @discord_users.key?(discord_id)
  end
end
