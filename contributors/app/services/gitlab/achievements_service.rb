# frozen_string_literal: true

module Gitlab
  class AchievementsService
    class << self
      def award(user_id, achievement_gid)
        Rails.logger.info(
          "Awarding achievement with gid #{achievement_gid} to user with gid #{user_gid(user_id)}"
        )

        mutation = <<~GRAPHQL
          mutation {
            achievementsAward(
              input: { achievementId: "#{achievement_gid}", userId: "#{user_gid(user_id)}" }
            ) {
              errors
            }
          }
        GRAPHQL
        GitlabGraphqlService.execute(mutation)
      end

      def current(user_id)
        query = <<~EOGQL
          query {
            user(id: "#{user_gid(user_id)}") {
              userAchievements {
                nodes {
                  id
                  achievement {
                    id
                  }
                }
              }
            }
          }
        EOGQL

        response = GitlabGraphqlService.execute(query)
        response.dig('data', 'user', 'userAchievements', 'nodes') || []
      end

      def revoke(user_achievement_gid)
        Rails.logger.info("Revoking user achievement with gid #{user_achievement_gid}")

        mutation = <<~EOGQL
          mutation revokeAchievement {
            achievementsRevoke(input: { userAchievementId: "#{user_achievement_gid}" }) {
              errors
            }
          }
        EOGQL
        GitlabGraphqlService.execute(mutation)
      end

      def user_gid(user_id)
        "gid://gitlab/User/#{user_id}"
      end
    end
  end
end
