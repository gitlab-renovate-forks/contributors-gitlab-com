# frozen_string_literal: true

require 'httparty'
require 'securerandom'

class BrilliantService
  BASE_URL = 'https://api.brilliantmade.com/v4'

  def initialize
    token = ENV.fetch('BRILLIANT_API_TOKEN')
    @headers = {
      'Authorization' => "Basic #{token}",
      'Content-Type' => 'application/json'
    }
  end

  def create_storefront_gift_card(user_id, credits)
    return 'DUMMYGIFTCODE' if Rails.env.development?

    body = {
      amount: credits,
      name: user_id.to_s,
      uid: user_id,
      inviting_user_id: 61_318,
      email: "#{SecureRandom.uuid}@example.com"
    }.to_json

    response = HTTParty.post("#{BASE_URL}/storefront_gift_cards", headers: @headers, body:)
    return response.parsed_response.dig('data', 'gift_code') if response.success?

    Rails.logger.error { response.body }
  end
end
