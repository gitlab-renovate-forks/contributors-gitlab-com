# frozen_string_literal: true

class ApplicationQuery
  include ActiveRecord::Sanitization::ClassMethods

  protected

  def exec_query(query)
    ::ActiveRecord::Base.connection.execute(query).to_a
  end
end
