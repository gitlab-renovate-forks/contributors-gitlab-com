# frozen_string_literal: true

class OrganizationsQuery < ApplicationQuery
  DEFAULT_OPTIONS = {
    page_size: 20,
    page: 1
  }.freeze

  def initialize(options)
    super()
    @options = DEFAULT_OPTIONS.merge(options)
  end

  def execute
    relation = Organization
      .where(view_activity_summaries: { community_member: true,
                                        activity_date: options[:from_date]..options[:to_date] })
    unless options[:search].nil?
      relation = relation.where("organization ILIKE '#{sanitize_sql_like(options[:search])}%'")
    end
    relation
      .order('score DESC')
      .page(options[:page])
      .per(options[:page_size])
  end

  private

  attr_reader :options
end
