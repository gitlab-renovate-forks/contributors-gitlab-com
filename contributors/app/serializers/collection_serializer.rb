# frozen_string_literal: true

class CollectionSerializer
  def initialize(records, options = {})
    @records = records
    @current_user_id = options[:current_user_id]
    @options = options
  end

  delegate :total_count, to: :records

  private

  attr_reader :records, :current_user_id, :options
end
