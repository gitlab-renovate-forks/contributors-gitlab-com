# Platform data

All data imported into the platform is taken from public APIs, such as the:

- [GitLab API](https://docs.gitlab.com/ee/api/).
- [Discourse API](https://docs.discourse.org/).
- [Discord API](https://discord.com/developers/docs/intro).

## Recognizing contributing organizations

An organization’s total contributions are calculated by summing the contributions
of all users who have listed the organization in their public GitLab profile.
