# User guide

## Contributor levels

| Contributor Levels | Requirements | Benefits & Rewards |
| --- | --- | --- |
| Level 1 | 25 Contribution Points | Achievement on profile, 5 credits for the Contributor Success store |
| Level 2 | 500 Contribution Points | Achievement on profile, 50 credits for the Contributor Success store |
| Level 3 | 2500 Contribution Points | Achievement on profile, 150 credits for the Contributor Success store |
| Level 4 | 7500+ Contribution Points | Achievement on profile, 300 credits for the Contributor Success store |
| Core | After approval (election process) | Achievement on profile, 300 credits for the Contributor Success store, 1 GitLab Ultimate license for personal use, Slack access (requires an NDA to be signed), Developer permission for GitLab projects |

## Contribution points

Scope (groups): `gitlab-com`, `gitlab-org`, `gitlab-community`, `components`.

- **Merge request created:** 20 points.
- **Commit merged:** 20 points.
- **Merge request merged:** 60 points (30 additional points when linked to an issue).
- **Issue created:** 5 points.
- **Issue/merge request comment:** 1 point.
- **Discord message:** 1 point.
- **Discord reply:** 2 points.
- **Forum post:** 1 point.
- **Forum reply:** 2 points.
- **Event activity:** 500 points.

## Events

If you are organizing or speaking at an event or meetup, please submit the details
using the **[Add event](https://contributors.gitlab.com/users/me#add-event)** button
from the **Bonus points** section of your user profile.

## Authentication

The platform uses GitLab.com as an identity provider, meaning all GitLab.com users
can sign in using single sign on (SSO).

## Individual Leaderboard

**Visibility**: Public

View the leaderboard of individual contributors and their contributor point score
within a period.

Drill down on a user to view details of their contributions.

## Organizations Leaderboard

**Visibility**: Public

View the leaderboard of organizations based on data individual contributors share
in their public GitLab profile.

Drill down on an organization to view details of their contributors.

## Organization Leaderboard

**Visibility**: Public

View the leaderboard of individual contributors within a specific organization.

Drill down on a user to view details of their contributions.

## Rewards

**Visibility**: Public

View a list of rewards issued from the platform and a link to the reward store.

The **Issue rewards** button allows admins to bulk issue reward credits,
especially useful after events such as the Hackathon.

## User profile

**Visibility**: Public

### Rewards

**Visibility**: Admin (and self)

- Rewards come in the form of credits for the [Contributor Success Reward Store](https://rewards.gitlab.com/).
- Credits can be redeemed for physical swag or planting trees in
  [the GitLab Forest at tree-nation](https://tree-nation.com/profile/gitlab).
- Credits are pushed directly to the reward store using a GitLab user id (along
  with placeholders for name/e-mail).
- Contributors login to the reward store using GitLab SSO.
- On first login, the store profile is updated with the contributor email.

The **Issue reward** button allows admins to issue reward credits,
for notable contributions for example.

### Events

**Visibility**: Public

Details of events/meetups users have organized or spoken at.

#### Add event

The **Add event** button allows users to submit details of events/meetups they are
speaking at or organizing.
This creates an issue in the [events project](https://gitlab.com/gitlab-community/community-members/events)
and recognizes the contribution with bonus points.

### Bonus Points

**Visibility**: Public

Whilst visible publicly, only admins are able to award bonus points.
Points are awarded for activity which is not yet automatically scored.
For example: helping out on social platforms or closing issues with bounties.

#### Add points

The **Add points** button allows admins to issue contribution points,
for notable contributions for example.

## Shortcuts

- [`https://contributors.gitlab.com/leaderboard/me`](https://contributors.gitlab.com/leaderboard/me):
  Find yourself on the leaderboard.
- [`https://contributors.gitlab.com/users/me`](https://contributors.gitlab.com/users/me):
  View your user profile.
- [`https://contributors.gitlab.com/users/me#add-event`](https://contributors.gitlab.com/users/me#add-event):
  Submit details of an event you are organizing or speaking at.
- `https://contributors.gitlab.com/manage-merge-request/request-review?project_id=PROJECT_ID&merge_request_iid=MR_IID`:
  Request a merge request review.
- `https://contributors.gitlab.com/manage-merge-request/request-help?project_id=PROJECT_ID&merge_request_iid=MR_IID`:
  Request help in a merge request.
