# frozen_string_literal: true

Rails.application.routes.draw do
  root 'frontend#index'

  get 'up', to: 'rails/health#show'

  scope controller: :users do
    get 'login'
    get 'logout'
    get 'oauth_callback'
  end

  draw :api_routes

  scope 'manage-merge-request', controller: :manage_merge_request do
    get 'request-help', action: 'request_help'
    get 'request-review', action: 'request_review'
  end

  get 'leaderboard/me', to: 'leaderboard#me'

  # Catch-all for frontend
  get '*path', to: 'frontend#index'
end
