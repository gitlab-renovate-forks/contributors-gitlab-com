import js from '@eslint/js';
import stylistic from '@stylistic/eslint-plugin';
import stylisticJs from '@stylistic/eslint-plugin-js';
import importPlugin from 'eslint-plugin-import';
import eslintPluginUnicorn from 'eslint-plugin-unicorn';
import vitest from 'eslint-plugin-vitest';
import pluginVue from 'eslint-plugin-vue';
import globals from 'globals';

export default [
  js.configs.recommended,
  ...pluginVue.configs['flat/recommended'],
  stylistic.configs.customize({
    semi: true,
    braceStyle: '1tbs',
  }),
  {
    plugins: {
      '@stylistic/js': stylisticJs,
      'unicorn': eslintPluginUnicorn,
      'import': importPlugin,
    },
    rules: {
      'comma-dangle': ['error', 'always-multiline'],
      'quotes': ['error', 'single', { avoidEscape: true }],
      'vue/component-api-style': ['error', ['script-setup']],
      'vue/singleline-html-element-content-newline': 'off',
      'no-console': 'error',
      'import/no-relative-parent-imports': 'error',
      'import/order': [
        'error', {
          'newlines-between': 'always',
          'groups': ['builtin', 'external', 'internal', 'parent', 'sibling'],
          'pathGroups': [
            {
              pattern: '~/**',
              group: 'parent',
            },
          ],
          'alphabetize': {
            order: 'asc',
            caseInsensitive: true,
          },
        },
      ],
    },
    settings: {
      'import/internal-regex': '^@images/',
    },
    languageOptions: {
      globals: {
        ...globals.browser,
      },
    },
  },
  {
    files: ['app/javascript/**/*.vue'],
    rules: {
      'vue/component-name-in-template-casing': 'error',
      'unicorn/filename-case': [
        'error',
        {
          cases: {
            pascalCase: true,
          },
        },
      ],
    },
  },
  {
    files: ['app/javascript/**/*.spec.js'],
    plugins: {
      vitest,
    },
    rules: {
      ...vitest.configs.recommended.rules,
    },
    languageOptions: {
      globals: {
        ...vitest.environments.env.globals,
        global: true,
        __dirname: true,
      },
    },
  },
  {
    files: ['vite.config.js'],
    languageOptions: {
      globals: {
        __dirname: true,
      },
    },
  },
];
