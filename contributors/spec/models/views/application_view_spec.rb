# frozen_string_literal: true

RSpec.describe Views::ApplicationView do
  subject(:concrete_class) { Views::ActivitySummary }

  describe '.refresh' do
    it 'populates the materialized view' do
      concrete_class.refresh(concurrently: false)

      expect { concrete_class.count }.not_to raise_error
    end
  end

  it 'is read only' do
    expect(concrete_class.new.readonly?).to be(true)
    expect { concrete_class.create! }.to raise_error(ActiveRecord::ReadOnlyRecord)
  end
end
