# frozen_string_literal: true

require_relative 'activity_summary_shared_examples'

RSpec.describe Views::RecentActivitySummary do
  it_behaves_like 'activity summary view'

  it 'returns the same columns as ActivitySummary' do
    expect(described_class.columns).to eq(Views::ActivitySummary.columns)
  end

  it 'does not return activity older than 90 days' do
    create(:issue, opened_date: 100.days.ago)

    described_class.refresh(concurrently: false)

    expect(described_class.count).to eq(0)
  end
end
