# frozen_string_literal: true

RSpec.shared_examples 'activity summary view' do
  describe 'scopes' do
    let!(:top_user) { create(:user, id: 1, username: 'user1', community_member: true, contributor_level: 1) }
    let!(:user) { create(:user, id: 2, username: 'user2', community_member: true) }
    let!(:non_community_member) { create(:user, id: 3, username: 'user3', community_member: false) }

    before do
      merge_request = create(
        :merge_request,
        user: top_user,
        opened_date: 1.day.ago,
        merged_date: 1.day.ago,
        labels: ['linked-issue'],
        state_id: :merged
      )
      create(:commit, user: top_user, merge_request:)
      create(:commit, user: non_community_member, merge_request:)
      create(:issue, user:, opened_date: 1.day.ago)
      create(:note, user:, added_date: 1.day.ago, issue: Issue.all.sample)
      create(:note, user:, added_date: 1.day.ago, merge_request: MergeRequest.all.sample)
      create(:bonus_points, user: top_user, awarded_by_user: non_community_member, points: 100)

      if described_class == Views::CombinedActivitySummary
        Views::ActivitySummary.refresh(concurrently: false)
        Views::RecentActivitySummary.refresh(concurrently: false)
      else
        described_class.refresh(concurrently: false)
      end
    end

    describe 'user_counts' do
      it 'returns expected activity summary' do
        expect(described_class.user_counts.as_json).to contain_exactly(
          {
            'user_id' => 3,
            'username' => 'user3',
            'contributor_level' => 0,
            'opened_merge_requests' => 0,
            'merged_merge_requests' => 0,
            'merged_with_issues' => 0,
            'opened_issues' => 0,
            'merged_commits' => 1,
            'added_notes' => 0,
            'bonus_points' => 0,
            'score' => 20
          },
          {
            'user_id' => 1,
            'username' => 'user1',
            'contributor_level' => 1,
            'opened_merge_requests' => 1,
            'merged_merge_requests' => 1,
            'merged_with_issues' => 1,
            'opened_issues' => 0,
            'merged_commits' => 1,
            'added_notes' => 0,
            'bonus_points' => 100,
            'score' => 230
          },
          {
            'user_id' => 2,
            'username' => 'user2',
            'contributor_level' => 0,
            'opened_merge_requests' => 0,
            'merged_merge_requests' => 0,
            'merged_with_issues' => 0,
            'opened_issues' => 1,
            'merged_commits' => 0,
            'added_notes' => 2,
            'bonus_points' => 0,
            'score' => 7
          }
        )
      end
    end

    describe 'username_search' do
      subject(:username_search) { described_class.username_search(search).pluck(:user_id).uniq }

      context 'with exact match' do
        let(:search) { 'user1' }

        it 'returns expected user' do
          expect(username_search).to contain_exactly(1)
        end
      end

      context 'with matching start' do
        let(:search) { 'user' }

        it 'returns expected users' do
          expect(username_search).to contain_exactly(1, 2, 3)
        end
      end

      context 'with matching middle' do
        let(:search) { 'ser' }

        it 'returns expected users' do
          expect(username_search).to contain_exactly(1, 2, 3)
        end
      end

      context 'with matching end' do
        let(:search) { 'ser2' }

        it 'returns expected user' do
          expect(username_search).to contain_exactly(2)
        end
      end
    end

    describe 'between' do
      context 'with from_date' do
        it 'returns expected users' do
          expect(described_class.between(1.day.from_now, nil)).to be_empty
        end
      end

      context 'with to_date' do
        it 'returns expected users' do
          expect(described_class.between(nil, 1.month.ago)).to be_empty
        end
      end
    end

    describe 'wider_community' do
      it 'returns expected users' do
        expect(described_class.wider_community.count).to eq(3)
      end
    end
  end
end
