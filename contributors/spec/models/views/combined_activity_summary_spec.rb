# frozen_string_literal: true

require_relative 'activity_summary_shared_examples'

RSpec.describe Views::CombinedActivitySummary do
  it_behaves_like 'activity summary view'

  it 'returns activity older than 90 days' do
    create(:issue, opened_date: 100.days.ago)

    refresh_views

    expect(described_class.count).to eq(1)
  end

  it 'does not duplicate activity present in both materialized views' do
    create(:issue, opened_date: 30.days.ago)

    refresh_views

    expect(described_class.count).to eq(1)
  end

  def refresh_views
    Views::ActivitySummary.refresh(concurrently: false)
    Views::RecentActivitySummary.refresh(concurrently: false)
  end

  it 'combines records from both materialized views' do
    create(:issue, opened_date: 100.days.ago)
    Views::ActivitySummary.refresh(concurrently: false)

    create(:issue, opened_date: Time.zone.now)
    Views::RecentActivitySummary.refresh(concurrently: false)

    expect(Views::ActivitySummary.count).to eq(1)
    expect(Views::RecentActivitySummary.count).to eq(1)
    expect(described_class.count).to eq(2)
  end
end
