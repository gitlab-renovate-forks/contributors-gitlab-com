# frozen_string_literal: true

REQUIRED_FIELDS = %w[
  user_id
].freeze

RSpec.describe UserProfile, required_fields: REQUIRED_FIELDS do
  describe 'associations' do
    it { is_expected.to belong_to(:user).required }
  end

  describe 'validations' do
    it { is_expected.to allow_value([nil, true, false]).for(:community_member_override) }
  end

  describe 'user association' do
    let!(:user_profile) { create(:user_profile) }

    it 'persists after user deletion' do
      User.destroy_by(id: user_profile.user_id)

      expect { user_profile.reload }.not_to raise_error
      expect(user_profile.reload.user_id).not_to be_nil
    end
  end
end
