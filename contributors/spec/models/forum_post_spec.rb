# frozen_string_literal: true

REQUIRED_FIELDS = %w[
  added_date
  id
  post_number
  topic_id
  topic_title
  user_id
].freeze

RSpec.describe ForumPost, required_fields: REQUIRED_FIELDS do
  describe 'associations' do
    it { is_expected.to belong_to(:user).required }
  end

  describe 'validations' do
    it { is_expected.to allow_value([true, false]).for(:reply) }
    it { is_expected.not_to allow_value(nil).for(:reply) }
  end

  describe 'scopes' do
    describe 'added_between' do
      let!(:user) { create(:user) }
      let!(:post_one) { create(:forum_post, user:, added_date: 15.days.ago) }
      let!(:post_two) { create(:forum_post, user:, added_date: 10.days.ago) }
      let!(:post_three) { create(:forum_post, user:, added_date: 5.days.ago) }

      context 'with from_date' do
        it 'returns forum posts after the given dates' do
          expect(described_class.added_between(12.days.ago, nil)).to contain_exactly(post_two, post_three)
        end
      end

      context 'with to_date' do
        it 'returns forum posts before the given dates' do
          expect(described_class.added_between(nil, 7.days.ago)).to contain_exactly(post_one, post_two)
        end
      end

      context 'with to_date and from_date' do
        it 'returns forum posts between the given dates' do
          expect(described_class.added_between(12.days.ago, 7.days.ago)).to contain_exactly(post_two)
        end
      end
    end

    describe 'user_counts' do
      let(:users) { create_list(:user, 2) }

      before do
        create(:forum_post, user: users[0])
        create_list(:forum_post, 2, user: users[1])
      end

      it 'returns note counts grouped by user' do
        expect(described_class.user_counts).to eq({ users[0].id => 1, users[1].id => 2 })
      end
    end
  end

  describe '#web_url' do
    it 'returns the expected web_url' do
      post = create(:forum_post, topic_id: 789, post_number: 3)

      expect(post.web_url).to eq('https://forum.gitlab.com/t/789/3')
    end
  end
end
