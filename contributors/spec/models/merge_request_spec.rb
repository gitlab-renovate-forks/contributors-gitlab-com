# frozen_string_literal: true

REQUIRED_FIELDS = %w[
  id
  title
  state_id
  opened_date
  web_url
  upvotes
].freeze

RSpec.describe MergeRequest, required_fields: REQUIRED_FIELDS do
  describe 'associations' do
    it { is_expected.to belong_to(:project).optional }
    it { is_expected.to belong_to(:user).required }

    it { is_expected.to have_many(:notes).dependent(:delete_all) }
    it { is_expected.to have_many(:commits).dependent(:delete_all) }
  end

  describe 'enums' do
    it 'defines a state_id enum' do
      expect(described_class.state_ids.keys).to contain_exactly('opened', 'closed', 'locked', 'merged')
    end
  end

  describe 'scopes' do
    describe 'opened_between' do
      let!(:user) { create(:user) }
      let!(:mr_one) { create(:merge_request, user:, opened_date: 15.days.ago) }
      let!(:mr_two) { create(:merge_request, user:, opened_date: 10.days.ago) }
      let!(:mr_three) { create(:merge_request, user:, opened_date: 5.days.ago) }

      context 'with from_date' do
        it 'returns merge requests opened after the given dates' do
          expect(described_class.opened_between(12.days.ago, nil)).to contain_exactly(mr_two, mr_three)
        end
      end

      context 'with to_date' do
        it 'returns merge requests opened before the given dates' do
          expect(described_class.opened_between(nil, 7.days.ago)).to contain_exactly(mr_one, mr_two)
        end
      end

      context 'with to_date and from_date' do
        it 'returns merge requests opened between the given dates' do
          expect(described_class.opened_between(12.days.ago, 7.days.ago)).to contain_exactly(mr_two)
        end
      end
    end

    describe 'merged_between' do
      let!(:user) { create(:user) }
      let!(:mr_one) { create(:merge_request, user:, merged_date: 15.days.ago) }
      let!(:mr_two) { create(:merge_request, user:, merged_date: 10.days.ago) }
      let!(:mr_three) { create(:merge_request, user:, merged_date: 5.days.ago) }

      context 'with from_date' do
        it 'returns merge requests opened after the given dates' do
          expect(described_class.merged_between(12.days.ago, nil)).to contain_exactly(mr_two, mr_three)
        end
      end

      context 'with to_date' do
        it 'returns merge requests opened before the given dates' do
          expect(described_class.merged_between(nil, 7.days.ago)).to contain_exactly(mr_one, mr_two)
        end
      end

      context 'with to_date and from_date' do
        it 'returns merge requests opened between the given dates' do
          expect(described_class.merged_between(12.days.ago, 7.days.ago)).to contain_exactly(mr_two)
        end
      end
    end

    describe 'user_counts' do
      let(:users) { create_list(:user, 2) }

      before do
        create(:merge_request, user: users[0])
        create_list(:merge_request, 2, user: users[1])
      end

      it 'returns merge request counts grouped by user' do
        expect(described_class.user_counts).to eq({ users[0].id => 1, users[1].id => 2 })
      end
    end

    describe 'with_commits' do
      before do
        create(:commit)
      end

      it 'includes commit fields' do
        expect { described_class.with_commits.select('sha').first }.not_to raise_error
      end

      it 'avoids N+1 queries' do
        expect { described_class.with_commits.select('sha').first }.to make_database_queries(count: 1)
      end
    end
  end
end
