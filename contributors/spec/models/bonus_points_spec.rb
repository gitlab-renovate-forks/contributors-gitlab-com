# frozen_string_literal: true

REQUIRED_FIELDS = %w[
  awarded_by_user_id
  points
  reason
  user_id
].freeze

RSpec.describe BonusPoints, required_fields: REQUIRED_FIELDS do
  describe 'associations' do
    it { is_expected.to belong_to(:awarded_by_user).class_name('User').required }
    it { is_expected.to belong_to(:user).required }
  end

  describe 'user association' do
    let!(:bonus_points) { create(:bonus_points) }

    it 'persists after user deletion' do
      User.destroy_by(id: bonus_points.user_id)

      expect(bonus_points.reload.user_id).not_to be_nil
    end

    it 'persists after awarded_by_user deletion' do
      User.destroy_by(id: bonus_points.awarded_by_user_id)

      expect(bonus_points.reload.awarded_by_user_id).not_to be_nil
    end
  end

  describe 'scopes' do
    describe 'awarded_between' do
      let!(:user) { create(:user) }
      let!(:bp_one) { create(:bonus_points, user:, created_at: 15.days.ago) }
      let!(:bp_two) { create(:bonus_points, user:, created_at: 10.days.ago) }
      let!(:bp_three) { create(:bonus_points, user:, created_at: 5.days.ago) }

      context 'with from_date' do
        it 'returns bonus points awarded after the given dates' do
          expect(described_class.awarded_between(12.days.ago, nil)).to contain_exactly(bp_two, bp_three)
        end
      end

      context 'with to_date' do
        it 'returns bonus points awarded before the given dates' do
          expect(described_class.awarded_between(nil, 7.days.ago)).to contain_exactly(bp_one, bp_two)
        end
      end

      context 'with to_date and from_date' do
        it 'returns bonus points awarded between the given dates' do
          expect(described_class.awarded_between(12.days.ago, 7.days.ago)).to contain_exactly(bp_two)
        end
      end
    end

    describe 'user_totals' do
      let(:users) { create_list(:user, 2) }

      before do
        create(:bonus_points, user: users[0], points: 100)
        create_list(:bonus_points, 2, user: users[1], points: 10)
      end

      it 'returns bonus points totals grouped by user' do
        expect(described_class.user_totals).to eq({ users[0].id => 100, users[1].id => 20 })
      end
    end
  end

  describe '.activity_types' do
    it 'assigns the correct values to each activity type' do
      expect(described_class.activity_types).to eq({
        'other' => 0,
        'social' => 1,
        'event' => 2,
        'support' => 3,
        'content' => 4
      })
    end
  end
end
