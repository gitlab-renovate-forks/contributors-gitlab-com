# frozen_string_literal: true

REQUIRED_FIELDS = %w[
  awarded_by_user_id
  gift_code
  issue_iid
  credits
  reason
  user_id
].freeze

RSpec.describe Reward, required_fields: REQUIRED_FIELDS do
  describe 'associations' do
    it { is_expected.to belong_to(:awarded_by_user).class_name('User').required }
    it { is_expected.to belong_to(:user).required }
  end

  describe 'user association' do
    let!(:reward) { create(:reward) }

    it 'persists after user deletion' do
      User.destroy_by(id: reward.user_id)

      expect(reward.reload.user_id).not_to be_nil
    end

    it 'persists after awarded_by_user deletion' do
      User.destroy_by(id: reward.awarded_by_user_id)

      expect(reward.reload.awarded_by_user_id).not_to be_nil
    end
  end

  describe '#issue_web_url' do
    it 'returns the issue web url' do
      reward = build(:reward, issue_iid: 123)

      expect(reward.issue_web_url).to eq('https://gitlab.com/gitlab-org/developer-relations/contributor-success/rewards/-/issues/123')
    end
  end

  describe '.reasons' do
    it 'assigns the correct values to each reason' do
      expect(described_class.reasons).to eq({
        'other' => 0,
        'first_contribution' => 1,
        'hackathon' => 2,
        'notable_contributor' => 3,
        'level_up' => 4
      })
    end
  end
end
