# frozen_string_literal: true

REQUIRED_FIELDS = %w[
  id
  title
  opened_date
  web_url
  upvotes
].freeze

RSpec.describe Issue, required_fields: REQUIRED_FIELDS do
  describe 'associations' do
    it { is_expected.to belong_to(:project).optional }
    it { is_expected.to belong_to(:user).required }

    it { is_expected.to have_many(:notes).dependent(:delete_all) }
  end

  describe 'enums' do
    it 'defines a state_id enum' do
      expect(described_class.state_ids.keys).to contain_exactly('opened', 'closed')
    end
  end

  describe 'scopes' do
    describe 'opened_between' do
      let!(:user) { create(:user) }
      let!(:issue_one) { create(:issue, user:, opened_date: 15.days.ago) }
      let!(:issue_two) { create(:issue, user:, opened_date: 10.days.ago) }
      let!(:issue_three) { create(:issue, user:, opened_date: 5.days.ago) }

      context 'with from_date' do
        it 'returns issues opened after the given dates' do
          expect(described_class.opened_between(12.days.ago, nil)).to contain_exactly(issue_two, issue_three)
        end
      end

      context 'with to_date' do
        it 'returns issues opened before the given dates' do
          expect(described_class.opened_between(nil, 7.days.ago)).to contain_exactly(issue_one, issue_two)
        end
      end

      context 'with to_date and from_date' do
        it 'returns issues opened between the given dates' do
          expect(described_class.opened_between(12.days.ago, 7.days.ago)).to contain_exactly(issue_two)
        end
      end
    end

    describe 'user_counts' do
      let(:users) { create_list(:user, 2) }

      before do
        create(:issue, user: users[0])
        create_list(:issue, 2, user: users[1])
      end

      it 'returns issue counts grouped by user' do
        expect(described_class.user_counts).to eq({ users[0].id => 1, users[1].id => 2 })
      end
    end
  end
end
