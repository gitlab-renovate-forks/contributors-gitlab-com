# frozen_string_literal: true

REQUIRED_FIELDS = %w[
  id
].freeze

RSpec.describe Project, required_fields: REQUIRED_FIELDS do
  describe 'associations' do
    it { is_expected.to have_many(:issues).dependent(:restrict_with_exception) }
    it { is_expected.to have_many(:merge_requests).dependent(:restrict_with_exception) }
  end
end
