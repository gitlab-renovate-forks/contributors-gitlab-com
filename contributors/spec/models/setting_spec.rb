# frozen_string_literal: true

RSpec.describe Setting do
  defaults =
    {
      created_issue_points: 5,
      created_mr_author_points: 20,
      discord_message_points: 1,
      discord_reply_points: 2,
      forum_post_points: 1,
      forum_reply_points: 2,
      merged_mr_author_points: 60,
      merged_mr_committer_points: 20,
      merged_mr_with_issue_points: 30,
      note_points: 1
    }

  describe 'defaults' do
    defaults.each do |key, value|
      it "has the correct default value for #{key}" do
        expect(described_class.send(key)).to eq(value)
      end
    end
  end

  describe 'fields' do
    it 'has the expected fields' do
      expect(described_class.keys).to match_array(defaults.keys.map(&:to_s))
    end
  end
end
