# frozen_string_literal: true

REQUIRED_FIELDS = %w[
  committed_date
  merge_request_id
  sha
  user_id
].freeze

RSpec.describe Commit, required_fields: REQUIRED_FIELDS do
  describe 'associations' do
    it { is_expected.to belong_to(:merge_request).required }
    it { is_expected.to belong_to(:user).required }
  end

  describe 'validations' do
    subject { create(:commit) }

    it { is_expected.to validate_uniqueness_of(:sha) }
  end
end
