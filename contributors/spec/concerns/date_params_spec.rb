# frozen_string_literal: true

class DummyClass
  include DateParams

  attr_accessor :params

  def initialize(params)
    @params = params
  end
end

RSpec.describe DateParams, type: :module do
  let(:dummy_class) { DummyClass.new(params) }
  let(:current_time) { Time.zone.now }

  before do
    allow(Time.zone).to receive(:now).and_return(current_time)
  end

  describe '#from_date' do
    subject(:from_date) { dummy_class.send(:from_date) }

    context 'when from_date param is blank' do
      let(:params) { { from_date: nil } }

      it 'returns 90 days ago' do
        expect(from_date).to eq(90.days.ago)
      end
    end

    context 'when from_date param is present' do
      let(:params) { { from_date: '2024-01-01' } }

      it 'parses and returns the from_date param' do
        expect(from_date).to eq(Time.zone.parse('2024-01-01'))
      end
    end
  end

  describe '#to_date' do
    subject(:to_date) { dummy_class.send(:to_date) }

    context 'when to_date param is blank' do
      let(:params) { { to_date: nil } }

      it 'returns nil' do
        expect(to_date).to be_nil
      end
    end

    context 'when to_date param is present' do
      let(:params) { { to_date: '2024-01-01' } }

      it 'parses and returns the to_date param' do
        expect(to_date).to eq(Time.zone.parse('2024-01-01'))
      end
    end
  end
end
