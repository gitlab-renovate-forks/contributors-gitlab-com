# frozen_string_literal: true

FactoryBot.define do
  factory :forum_post do
    sequence(:id) # rubocop:disable FactoryBot/IdSequence
    added_date { rand(1..100).days.ago }
    post_number { rand(1..10) }
    reply { [true, false].sample }
    topic_id { rand(100_000..199_999) }
    topic_title { "Topic #{topic_id}" }
    user
  end
end
