# frozen_string_literal: true

module FactoryBotHelpers
  module Labels
    def self.random_labels
      type = ['type::bug', 'type::feature', 'type::maintenance', nil].sample
      group = ['group::frontend', 'group::backend', 'group::devops', nil].sample
      linked_issue = ['linked-issue', nil].sample
      documentation = ['documentation', nil].sample

      [type, group, linked_issue, documentation].compact.join(', ')
    end
  end
end
