# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:id) # rubocop:disable FactoryBot/IdSequence
    sequence(:username) { |n| "user#{n}" }
    community_member { [true, false].sample }

    trait(:with_profile) do
      user_profile
    end
  end
end
