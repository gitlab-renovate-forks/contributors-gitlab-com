# frozen_string_literal: true

FactoryBot.define do
  factory :team_member do
    sequence(:username) { |n| "user#{n}" }
    mr_coach { [true, false].sample }
    available { [true, false].sample }
  end
end
