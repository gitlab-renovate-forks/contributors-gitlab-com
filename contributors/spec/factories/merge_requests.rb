# frozen_string_literal: true

FactoryBot.define do
  factory :merge_request do
    sequence(:id) # rubocop:disable FactoryBot/IdSequence
    opened_date { rand(1..100).days.ago }
    # project
    state_id { 1 }
    sequence(:title) { |n| "Merge Request #{n}" }
    upvotes { 0 }
    user
    web_url { "https://gitlab.example.com/group/project/issues/#{id}" }
    labels { FactoryBotHelpers::Labels.random_labels }

    after(:build) do |merge_request, evaluator|
      if evaluator.merged?
        merge_request.merged_date = evaluator.merged_date || rand(evaluator.opened_date..Time.zone.now)
      end
    end
  end
end
