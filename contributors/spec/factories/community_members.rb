# frozen_string_literal: true

FactoryBot.define do
  factory :community_member do
    user
  end
end
