# frozen_string_literal: true

FactoryBot.define do
  factory :commit do
    sha { SecureRandom.hex(20) }
    committed_date { rand(1..100).days.ago }
    merge_request
    user
  end
end
