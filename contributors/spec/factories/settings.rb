# frozen_string_literal: true

FactoryBot.define do
  factory :setting do
    var { 'default' }
    value { 1 }
  end
end
