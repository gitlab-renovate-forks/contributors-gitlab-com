# frozen_string_literal: true

RSpec.describe LeaderboardQuery do
  subject(:query) { described_class.new(options) }

  let(:options) do
    {
      from_date: 1.year.ago.to_date,
      to_date: Time.zone.now.to_date,
      community_only: false,
      page: 1,
      page_size: 50
    }
  end

  let!(:top_alice) { create(:user, username: 'Alice', community_member: true) }
  let!(:bob) { create(:user, username: 'Bob', community_member: true) }
  let!(:non_community_andrew) do
    create(:user, username: 'Andrew', community_member: false, contributor_level: 2)
  end

  before do
    merge_request =
      create(:merge_request, user: top_alice, opened_date: 1.day.ago, merged_date: 1.day.ago, state_id: :merged)
    create(:commit, user: top_alice, merge_request:)
    create(:commit, user: non_community_andrew, merge_request:)
    create(:issue, user: bob, opened_date: 1.day.ago)
    create(:note, user: bob, added_date: 1.day.ago, issue: Issue.all.sample)
    create(:note, user: bob, added_date: 1.day.ago, merge_request: MergeRequest.all.sample)

    Views::ActivitySummary.refresh(concurrently: false)
    Views::RecentActivitySummary.refresh(concurrently: false)
  end

  it 'returns paginated statistic and score' do
    records = query.execute

    expect(records.total_count).to eq(3)
    expect(records.to_a.pluck(:user_id, :score))
      .to eq([[top_alice.id, 100], [non_community_andrew.id, 20], [bob.id, 7]])
  end

  it 'calls PointsService with expected options' do
    allow(PointsService).to receive(:new).and_call_original

    query.execute

    expect(PointsService)
      .to have_received(:new)
      .with(
        from_date: 1.year.ago.to_date,
        to_date: Time.zone.now.to_date,
        community_only: false
      )
  end

  context 'with search' do
    let(:options) do
      {
        from_date: 1.year.ago.to_date,
        to_date: Time.zone.now.to_date,
        community_only: false,
        search: 'al',
        page: 1,
        page_size: 50
      }
    end

    it 'returns expected contributors' do
      records = query.execute

      expect(records.total_count).to eq(1)
      expect(records.to_a.pluck(:user_id, :score)).to eq([[top_alice.id, 100]])
    end
  end

  context 'with non-default page_size and page' do
    let(:options) do
      { from_date: nil, to_date: nil, community_only: false, page: 2, page_size: 1 }
    end

    it 'returns paginated statistic and score' do
      records = query.execute

      expect(records.total_count).to eq(3)
      expect(records.to_a.pluck(:user_id, :score)).to eq([[non_community_andrew.id, 20]])
    end
  end
end
