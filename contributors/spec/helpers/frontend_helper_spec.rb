# frozen_string_literal: true

RSpec.describe FrontendHelper do
  let(:session) { {} }

  before do
    allow(helper).to receive(:session).and_return(session)
  end

  describe '#user_admin?' do
    context 'when not logged in' do
      it 'returns false' do
        expect(helper.user_admin?).to be(false)
      end
    end

    context 'when logged in as a user' do
      let(:session) { { admin: false } }

      it 'returns false' do
        expect(helper.user_admin?).to be(false)
      end
    end

    context 'when logged in as an admin' do
      let(:session) { { admin: true } }

      it 'returns true' do
        expect(helper.user_admin?).to be(true)
      end
    end
  end

  describe '#username' do
    context 'when not logged in' do
      it 'is nil' do
        expect(helper.username).to be_nil
      end
    end

    context 'when logged in' do
      let(:session) { { username: 'lee' } }

      it 'returns logged in username' do
        expect(helper.username).to eq('lee')
      end
    end
  end

  describe '#contributor_level' do
    context 'when not logged in' do
      it 'is 0' do
        expect(helper.contributor_level).to eq(0)
      end
    end

    context 'when logged in' do
      let(:session) { { user_id: 123 } }

      it 'returns logged in contributor_level' do
        create(:user, id: 123, contributor_level: 2)

        expect(helper.contributor_level).to eq(2)
      end
    end
  end

  describe '#discord_messages?' do
    context 'when not logged in' do
      it 'is false' do
        expect(helper.discord_messages?).to be(false)
      end
    end

    context 'when logged in' do
      let(:user) { create(:user) }
      let(:session) { { user_id: user.id } }

      context 'when user has discord messages' do
        it 'returns true' do
          create(:discord_message, user:)

          expect(helper.discord_messages?).to be(true)
        end
      end

      context 'when user has no discord messages' do
        it 'returns false' do
          expect(helper.discord_messages?).to be(false)
        end
      end
    end
  end

  describe '#vue_data' do
    let(:session) { { user_id: 1_000_000, admin: false, username: 'lee' } }
    let!(:user) { create(:user, id: 1_000_000, contributor_level: 4, points: 7_501) }

    it 'returns expected vue data' do
      expect(helper.vue_data).to eq(
        {
          contributorLevel: user.contributor_level,
          contributorPoints: user.points,
          hasDiscordMessages: false,
          userAdmin: false,
          userId: 1_000_000,
          username: 'lee'
        }
      )
    end

    context 'when not logged in' do
      let(:session) { {} }

      it 'returns expected vue data' do
        expect(helper.vue_data).to eq(
          {
            contributorLevel: 0,
            contributorPoints: 0,
            hasDiscordMessages: false,
            userAdmin: false,
            userId: nil,
            username: nil
          }
        )
      end
    end
  end
end
