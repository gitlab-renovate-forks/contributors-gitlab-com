# frozen_string_literal: true

RSpec.describe FrontendController do
  describe 'GET #index' do
    it 'renders index' do
      get root_path

      expect(response).to have_http_status(:ok)
      expect(response).to render_template(:index, layout: 'vue')
    end

    it_behaves_like 'sets last activity user timestamp' do
      subject(:http_request) { get root_path }
    end
  end
end
