# frozen_string_literal: true

RSpec.describe Api::V1::LeaderboardController do
  describe 'GET #index' do
    let!(:top_alice) { create(:user, username: 'Alice', community_member: true, contributor_level: 1) }
    let!(:bobby) { create(:user, username: 'Bobby', community_member: true) }
    let!(:non_community_barth) { create(:user, username: 'Barth', community_member: false) }

    before do
      merge_request = create(
        :merge_request,
        user: top_alice,
        opened_date: 1.day.ago,
        merged_date: 1.day.ago,
        labels: ['linked-issue'],
        state_id: :merged
      )
      create(:commit, user: top_alice, merge_request:)
      create(:commit, user: non_community_barth, merge_request:)
      create(:issue, user: bobby, opened_date: 1.day.ago)
      create(:note, user: bobby, added_date: 1.day.ago, issue: Issue.all.sample)
      create(:note, user: bobby, added_date: 1.day.ago, merge_request: MergeRequest.all.sample)
      create(:bonus_points, user: top_alice, awarded_by_user: non_community_barth, points: 100)
      Views::RecentActivitySummary.refresh(concurrently: false)
    end

    it 'returns expected users/data' do
      sign_in(top_alice.id)

      get api_v1_leaderboard_path

      users, metadata = json_body.values_at('records', 'metadata')
      expect(metadata['total_count']).to eq(2)
      expect(users.count).to eq(2)
      expect(users.first).to eq({
        'user_id' => top_alice.id,
        'username' => top_alice.username,
        'merged_merge_requests' => 1,
        'merged_with_issues' => 1,
        'opened_merge_requests' => 1,
        'opened_issues' => 0,
        'merged_commits' => 1,
        'added_notes' => 0,
        'contributor_level' => 1,
        'score' => 230,
        'bonus_points' => 100,
        'current_user' => true
      })
      expect(users.last).to eq({
        'user_id' => bobby.id,
        'username' => bobby.username,
        'merged_merge_requests' => 0,
        'merged_with_issues' => 0,
        'opened_merge_requests' => 0,
        'opened_issues' => 1,
        'merged_commits' => 0,
        'added_notes' => 2,
        'contributor_level' => 0,
        'score' => 7,
        'bonus_points' => 0
      })
    end

    context 'with from_date' do
      it 'returns expected users' do
        get api_v1_leaderboard_path, params: { from_date: 1.day.from_now }

        users, metadata = json_body.values_at('records', 'metadata')
        expect(metadata['total_count']).to eq(0)
        expect(users).to be_empty
      end
    end

    context 'with to_date' do
      it 'returns expected users' do
        get api_v1_leaderboard_path, params: { to_date: 1.month.ago }

        users, metadata = json_body.values_at('records', 'metadata')
        expect(metadata['total_count']).to eq(0)
        expect(users).to be_empty
      end
    end

    context 'with community_only false' do
      it 'returns expected users' do
        get api_v1_leaderboard_path, params: { community_only: false }

        users, metadata = json_body.values_at('records', 'metadata')
        expect(metadata['total_count']).to eq(3)
        expect(users.count).to eq(3)
      end
    end

    context 'with search' do
      it 'returns expected users' do
        get api_v1_leaderboard_path, params: { search: 'b', community_only: false }

        users, metadata = json_body.values_at('records', 'metadata')
        expect(metadata['total_count']).to eq(2)
        expect(users.count).to eq(2)
        expect(users.pluck('user_id')).to contain_exactly(non_community_barth.id, bobby.id)
      end
    end

    it_behaves_like 'a paginated endpoint', key_attr_json: 'user_id', key_attr_record: 'id' do
      let(:endpoint) { api_v1_leaderboard_path }
      let(:first_record) { top_alice }
      let(:second_record) { bobby }
    end

    it_behaves_like 'sets last activity user timestamp' do
      subject(:http_request) { get api_v1_leaderboard_path }
    end
  end
end
