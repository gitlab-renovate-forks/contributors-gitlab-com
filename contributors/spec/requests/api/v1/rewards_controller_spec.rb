# frozen_string_literal: true

RSpec.describe Api::V1::RewardsController do
  let(:token) { 'brilliant-api-token' }
  let(:service) { described_class.new }

  before do
    stub_env('BRILLIANT_API_TOKEN', token)
    allow(SecureRandom).to receive(:uuid).and_return('9029b216-9296-4ad7-a777-405b4a864370')
  end

  describe 'POST #issue', :vcr do
    subject(:issue_reward) { post issue_api_v1_rewards_path, params: { user_id:, credits:, reason:, note: 'See https://gitlab.com/relevant_issue' } }

    let(:user_id) { 1_933_526 }
    let(:credits) { 3 }
    let(:reason) { 'notable_contributor' }

    before do
      create(:user, id: user_id, username: 'leetickett')
      create(:user, id: 9)
    end

    it_behaves_like 'an endpoint requiring admin' do
      before do
        create(:user, id: 1)
      end
    end

    it 'returns success response and creates reward record' do
      sign_in(9, admin: true)

      issue_reward

      expect(response).to have_http_status(:ok)
      expect(Reward.last).to have_attributes(
        user_id:,
        credits:,
        reason:,
        gift_code: 'TFIGBXMKGQWNALQR',
        awarded_by_user_id: 9,
        issue_iid: 955
      )
    end
  end

  describe 'GET #index' do
    subject(:get_rewards) { get api_v1_rewards_path(params:) }

    let(:params) { {} }
    let(:records) { json_body['records'] }

    context 'when no rewards have been issued' do
      it 'returns an empty array' do
        get_rewards

        expect(records).to be_empty
      end
    end

    context 'when rewards have been issued' do
      let!(:rewards) do
        [create(:reward, created_at: '2024-03-01'), create(:reward, created_at: '2024-02-01')]
      end

      it_behaves_like 'a paginated endpoint', key_attr_json: 'user_id' do
        let(:endpoint) { api_v1_rewards_path }
        let(:first_record) { rewards[0] }
        let(:second_record) { rewards[1] }
      end

      it 'does not include the gift code' do
        get_rewards

        expect(records.first).not_to have_key('gift_code')
      end

      context 'when signed in as an admin' do
        before do
          sign_in(admin: true)
        end

        it 'includes the gift code' do
          get_rewards

          expect(records.first).to have_key('gift_code')
        end
      end
    end
  end

  describe 'POST #bulk_issue' do
    subject(:issue_reward) { post bulk_issue_api_v1_rewards_path, params: }

    let(:params) do
      {
        usernames: %w[taucher2003 zillemarco],
        credits: 3,
        reason: 'notable_contributor',
        note: 'See https://gitlab.com/relevant_issue'
      }
    end

    before do
      allow(GitlabOauthService).to receive(:refresh).and_return(
        { gitlab_access_token: 'token' }
      )
      allow(BulkIssueRewardsJob).to receive(:perform_later)
    end

    it_behaves_like 'an endpoint requiring admin'

    context 'when logged in as an admin' do
      before do
        sign_in(9, admin: true)

        issue_reward
      end

      it 'calls expected methods/services' do
        expect(BulkIssueRewardsJob).to have_received(:perform_later).with(
          'token',
          %w[taucher2003 zillemarco],
          3,
          'notable_contributor',
          'See https://gitlab.com/relevant_issue',
          9
        )
      end

      context 'with too many usernames' do
        let(:params) { { usernames: Array.new(101) { |i| "user#{i}" } } }

        it 'returns an error' do
          expect(response).to have_http_status(:bad_request)
          expect(json_body).to eq('error' => 'Please provide 100 usernames or less')
        end
      end
    end
  end
end
