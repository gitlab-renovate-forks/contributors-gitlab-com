# frozen_string_literal: true

RSpec.describe FetchContributorSuccessUsersService do
  let(:query_response) do
    { 'data' => {
      'group' => {
        'groupMembers' => {
          'nodes' =>
            [
              { 'user' => { 'id' => 'gid://gitlab/User/1' } },
              { 'user' => { 'id' => 'gid://gitlab/User/2' } },
              { 'user' => { 'id' => 'gid://gitlab/User/3' } }
            ]
        }
      }
    } }
  end

  before do
    allow(GitlabGraphqlService).to receive(:execute).and_return(query_response)
  end

  it 'returns the ids of the users in the contributor success group' do
    expect(described_class.execute).to eq([1, 2, 3])
  end
end
