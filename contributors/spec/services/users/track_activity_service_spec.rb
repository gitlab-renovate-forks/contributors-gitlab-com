# frozen_string_literal: true

RSpec.describe Users::TrackActivityService do
  subject(:track_activity) { described_class.new(session, cookies) }

  describe '.track_first_login!' do
    context 'when user does not exist' do
      it "doesn't fail" do
        expect { described_class.track_first_login!(0) }.not_to raise_error
      end
    end

    context 'when user exists' do
      context 'with profile' do
        let(:user) { create(:user, :with_profile) }

        context "when it's not a first login" do
          before do
            user.user_profile.update!(platform_first_login_at: 1.week.ago)
          end

          it "doesn't change timestamp" do
            expect { described_class.track_first_login!(user.id) }
              .not_to change(user.user_profile, :platform_first_login_at)
          end
        end

        context "when it's a first login" do
          it 'sets timestamp' do
            described_class.track_first_login!(user.id)

            expect(user.reload_user_profile.platform_first_login_at)
              .to be_within(2.seconds).of(Time.zone.now)
          end
        end
      end

      context 'without profile' do
        let(:user) { create(:user) }

        it 'creates profile and sets first_login_at' do
          described_class.track_first_login!(user.id)

          expect(user.reload_user_profile.platform_first_login_at)
            .to be_within(2.seconds).of(Time.zone.now)
        end
      end
    end
  end

  describe '#execute' do
    shared_examples 'does not track activity' do
      it do
        session_before = session.dup

        if defined?(user)
          expect { track_activity.execute }
            .not_to change(user.user_profile, :platform_last_activity_at)
        end

        expect(session).to eq(session_before)
      end
    end

    shared_examples 'tracks activity' do
      it do
        track_activity.execute

        user_profile = user.reload_user_profile

        expect(user_profile.platform_last_activity_at)
          .to be_within(2.seconds).of(Time.zone.now)
        expect(user_profile.platform_first_login_at)
          .to be_within(2.seconds).of(Time.zone.now)
        expect(session[:last_activity_at]).to be_within(2).of(Time.zone.now.to_i)
      end
    end

    shared_examples 'updates contributor_platform_user cookie' do |cookie_value|
      it do
        track_activity.execute

        expect(cookies[:contributor_platform_user]).to eq(cookie_value)
      end
    end

    context 'when user is not logged in' do
      let(:session) { {} }
      let(:cookies) { {} }

      it_behaves_like 'does not track activity'
      it_behaves_like 'updates contributor_platform_user cookie', false
    end

    context 'when user is logged in' do
      let(:user) { create(:user, :with_profile) }
      let(:session) { { user_id: user.id } }
      let(:cookies) { {} }

      it_behaves_like 'tracks activity'
      it_behaves_like 'updates contributor_platform_user cookie', true

      context 'with tracked activity less than hour ago' do
        let(:session) { { user_id: user.id, last_activity_at: 59.minutes.ago.to_i } }

        it_behaves_like 'does not track activity'
        it_behaves_like 'updates contributor_platform_user cookie', true
      end

      context 'with tacked activity more than hour ago' do
        let(:session) { { user_id: user.id, last_activity_at: 61.minutes.ago.to_i } }

        it_behaves_like 'tracks activity'
        it_behaves_like 'updates contributor_platform_user cookie', true
      end

      context 'without user profile' do
        let(:user) { create(:user) }

        it_behaves_like 'tracks activity'
        it_behaves_like 'updates contributor_platform_user cookie', true
      end

      context "with user that isn't yet on the platform" do
        let(:session) { { user_id: 0 } }

        it "doesn't fail" do
          expect { track_activity.execute }.not_to raise_error
        end
      end
    end
  end
end
