# frozen_string_literal: true

RSpec.describe Users::GrabTeamMembersService do
  subject(:instance) { described_class.new }

  describe '.execute', vcr: { cassette_name: 'Users_GrabTeamMembersService/execute' } do
    let!(:old_team_member) { create(:team_member) }

    it 'grabs the expected number of community members' do
      expect { instance.execute }.to change(TeamMember, :count).from(1).to(586)
    end

    it 'grabs the expected community members' do
      instance.execute

      checksum = Digest::SHA256.hexdigest(
        TeamMember.order(:username).pluck(:username, :mr_coach, :available, :departments, :projects).to_json
      )

      expect(checksum).to eq('8a06e1c2d1552267fc951a78f844689fed1856c290b98d2eb3d326311a061aff')
    end

    it 'removes the old community member' do
      instance.execute

      expect { old_team_member.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
