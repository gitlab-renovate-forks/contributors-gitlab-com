# frozen_string_literal: true

RSpec.describe UserUpdateService do
  let(:user_update_service) { described_class.new(page_size: 3) }

  describe '#achievement_contributor_level' do
    context 'when user has no achievements' do
      let(:user_achievements) { [] }

      it 'returns 0' do
        expect(user_update_service.achievement_contributor_level(user_achievements)).to eq(0)
      end
    end

    context 'when user has other achievements' do
      let(:user_achievements) do
        [
          { 'achievement' => { 'id' => 'gid://gitlab/Achievements::Achievement/1' } },
          { 'achievement' => { 'id' => 'gid://gitlab/Achievements::Achievement/77' } }
        ]
      end

      it 'returns 0' do
        expect(user_update_service.achievement_contributor_level(user_achievements)).to eq(0)
      end
    end

    context 'when user achievements include level 1' do
      let(:user_achievements) do
        [
          { 'achievement' => { 'id' => 'gid://gitlab/Achievements::Achievement/1' } },
          { 'achievement' => { 'id' => 'gid://gitlab/Achievements::Achievement/59' } }
        ]
      end

      it 'returns 1' do
        expect(user_update_service.achievement_contributor_level(user_achievements)).to eq(1)
      end
    end

    context 'when user achievements include level 2' do
      let(:user_achievements) do
        [
          { 'achievement' => { 'id' => 'gid://gitlab/Achievements::Achievement/60' } },
          { 'achievement' => { 'id' => 'gid://gitlab/Achievements::Achievement/500' } }
        ]
      end

      it 'returns 2' do
        expect(user_update_service.achievement_contributor_level(user_achievements)).to eq(2)
      end
    end

    context 'when user achievements include level 3' do
      let(:user_achievements) do
        [
          { 'achievement' => { 'id' => 'gid://gitlab/Achievements::Achievement/61' } }
        ]
      end

      it 'returns 3' do
        expect(user_update_service.achievement_contributor_level(user_achievements)).to eq(3)
      end
    end

    context 'when user achievements include level 4' do
      let(:user_achievements) do
        [
          { 'achievement' => { 'id' => 'gid://gitlab/Achievements::Achievement/62' } }
        ]
      end

      it 'returns 4' do
        expect(user_update_service.achievement_contributor_level(user_achievements)).to eq(4)
      end
    end
  end

  describe '#users_query' do
    let(:ids) { [1, 20] }
    let(:expected_query) do
      <<~GRAPHQL
        query {
          user1:user(id: "gid://gitlab/User/1") {
            id
            username
            state
            discord
            organization
            userAchievements {
              nodes {
                achievement {
                  id
                }
              }
            }
          }
          user20:user(id: "gid://gitlab/User/20") {
            id
            username
            state
            discord
            organization
            userAchievements {
              nodes {
                achievement {
                  id
                }
              }
            }
          }
        }
      GRAPHQL
    end

    it 'returns the correct query' do
      expect(user_update_service.users_query(ids).gsub(/\s+/, ' ')).to eq(expected_query.gsub(/\s+/, ' '))
    end
  end

  describe '#execute' do
    let!(:users) do
      records = {
        unchanged_user: create(:user, id: 1, community_member: true),
        achievement_user: create(:user, id: 300, community_member: true, contributor_level: 1),
        blocked_user: create(:user, id: 4_000, community_member: true),
        discord_user: create(:user, id: 2),
        updated_username: create(:user, id: 3, username: 'original')
      }
      User.where(id: records.values.map(&:id)).update_all(updated_at: 2.days.ago)
      records
    end

    before do
      allow(GitlabGraphqlService).to receive(:execute).and_return(
        {
          'data' => {
            'user1' => {
              'id' => 'gid://gitlab/User/1',
              'username' => users[:unchanged_user].username,
              'state' => 'active',
              'discord' => '',
              'userAchievements' => {
                'nodes' => []
              }
            },
            'user300' => {
              'id' => 'gid://gitlab/User/300',
              'username' => users[:achievement_user].username,
              'state' => 'active',
              'discord' => nil,
              'userAchievements' => {
                'nodes' => [{ 'achievement' => { 'id' => 'gid://gitlab/Achievements::Achievement/62' } }]
              }
            },
            'user4000' => {
              'id' => 'gid://gitlab/User/4000',
              'username' => users[:blocked_user].username,
              'state' => 'blocked',
              'userAchievements' => {
                'nodes' => []
              }
            }
          }
        },
        {
          'data' => {
            'user2' => {
              'id' => 'gid://gitlab/User/2',
              'username' => users[:discord_user].username,
              'state' => 'active',
              'discord' => '693109493902606387',
              'userAchievements' => {
                'nodes' => []
              }
            },
            'user3' => {
              'id' => 'gid://gitlab/User/3',
              'username' => 'updated',
              'state' => 'active',
              'discord' => '',
              'userAchievements' => {
                'nodes' => []
              }
            }
          }
        }
      )
    end

    it 'does not update an unchanged user' do
      expect { user_update_service.execute }.not_to(change(users[:unchanged_user], :reload))
    end

    it 'updates users based on their achievements' do
      expect { user_update_service.execute }.to change { users[:achievement_user].reload.contributor_level }
        .from(1).to(4)
    end

    it 'updates users based on their discord id' do
      expect { user_update_service.execute }.to change { users[:discord_user].reload.discord_id }
        .from(nil).to('693109493902606387')
    end

    it 'updates users based on their username' do
      expect { user_update_service.execute }.to change { users[:updated_username].reload.username }
        .from('original').to('updated')
    end

    it 'deletes blocked users' do
      user_update_service.execute

      expect { users[:blocked_user].reload }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
