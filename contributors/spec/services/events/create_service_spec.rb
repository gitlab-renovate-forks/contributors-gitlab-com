# frozen_string_literal: true

RSpec.describe Events::CreateService, vcr: { cassette_name: 'Events_CreateService/execute' } do
  subject(:service) { described_class.new(session_user, params) }

  let(:user) { create(:user, id: 1_000_000) }
  let(:session_user) { { id: user.id, access_token: 'glpat-xxxxxxxxxxxxxxxxxxxx' } }
  let(:params) do
    {
      name: 'Community event #999',
      date: Date.new(2024, 12, 24).to_s,
      link: 'example.com',
      role: 'organizer',
      virtual: true,
      size: 'small',
      note: 'few test words about the event'
    }
  end

  let(:issue_url) { 'https://gitlab.com/knockfog-ext/test-members-1/-/issues/6' }

  it 'creates an issue' do
    issue = service.execute

    expect(issue[:issue_type]).to eq('issue')
    expect(issue[:web_url]).to eq(issue_url)
  end

  it 'creates expected bonus points' do
    expect { service.execute }.to change(BonusPoints, :count).by(1)

    bonus_points = user.bonus_points.last
    expect(bonus_points).to have_attributes(
      points: 500,
      activity_type: 'event',
      reason: "Community event #{params[:role]}. Issue: #{issue_url}"
    )
  end

  it 'calls API service with expected event params' do
    description = <<~TEXT
      - Date: 2024-12-24
      - Link: example.com
      - Role: organizer
      - Virtual: true
      - Size: Small (< 20)
      - Note: few test words about the event

      /cc @gitlab-org/developer-relations/contributor-success
    TEXT
    api_service_stub = instance_double(GitlabRestService, :create_event_issue)
    allow(GitlabRestService).to receive(:new)
      .with(session_user[:access_token]).and_return(api_service_stub)
    allow(api_service_stub).to receive(:create_event_issue).and_return({ web_url: 'test' })

    service.execute

    expect(api_service_stub).to have_received(:create_event_issue)
      .with(
        session_user[:id],
        params[:name],
        description
      )
  end
end
