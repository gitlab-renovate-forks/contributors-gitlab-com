# frozen_string_literal: true

RSpec.describe DiscourseService do
  let(:service) { described_class.new }
  let(:start_date) { '2023-01-01' }
  let(:end_date) { '2023-12-31' }

  describe '#fetch_posts' do
    let(:response) { instance_double(HTTParty::Response, code: 200, parsed_response: {}) }

    it 'makes a GET request to the correct URL with proper parameters' do
      expected_url = "#{DiscourseService::BASE_URL}/search.json"
      expected_query = {
        q: "after:#{start_date} before:#{end_date}",
        page: 0
      }
      allow(HTTParty).to receive(:get).and_return(response)

      service.fetch_posts(start_date, end_date)

      expect(HTTParty).to have_received(:get).with(expected_url, query: expected_query)
    end

    context 'when throttled' do
      let(:throttled_response) { instance_double(HTTParty::Response, code: 429, headers: { 'retry-after' => '5' }) }

      it 'sleeps and retries the request' do
        allow(HTTParty).to receive(:get).and_return(throttled_response, response)
        allow(service).to receive(:sleep)
        allow(service).to receive(:fetch_posts).and_call_original

        service.fetch_posts(start_date, end_date)

        expect(service).to have_received(:sleep).with(5)
        expect(service).to have_received(:fetch_posts).twice
      end
    end
  end

  describe '#fetch_user_id' do
    let(:username) { 'test_user' }
    let(:gitlab_username) { 'gitlab_user' }

    context 'when user is already in the map' do
      it 'returns the cached user_id' do
        service.instance_variable_get(:@user_map)[username] = 123

        expect(service.fetch_user_id(username)).to eq(123)
      end
    end

    context 'when user is not in the map' do
      before do
        parsed_response = { 'user' => { 'user_fields' => { '2' => gitlab_username } } }
        allow(HTTParty).to receive(:get).and_return(instance_double(HTTParty::Response, parsed_response:))
      end

      it 'fetches the user data and returns the user_id' do
        user = create(:user, username: gitlab_username)

        expect(service.fetch_user_id(username)).to eq(user.id)
      end

      it 'returns nil if the user is not found' do
        expect(service.fetch_user_id(username)).to be_nil
      end
    end
  end

  describe '#process_posts' do
    let(:posts) do
      [
        { 'id' => 1, 'username' => 'user1', 'topic_id' => 8, 'created_at' => '2023-01-15T12:00:00Z',
          'post_number' => 2 },
        { 'id' => 2, 'username' => 'user2', 'topic_id' => 9, 'created_at' => '2023-01-16T12:00:00Z',
          'post_number' => 1 }
      ]
    end
    let(:topics_map) { { 8 => 'Topic', 9 => 'Topic 2' } }

    before do
      allow(service).to receive(:fetch_user_id).with('user1').and_return(1)
      allow(service).to receive(:fetch_user_id).with('user2').and_return(2)
    end

    it 'processes posts and returns formatted data' do
      result = service.process_posts(posts, topics_map)

      expect(result).to eq(
        [
          { id: 1, user_id: 1, topic_id: 8, topic_title: 'Topic', added_date: DateTime.parse('2023-01-15T12:00:00Z'),
            reply: true, post_number: 2 },
          { id: 2, user_id: 2, topic_id: 9, topic_title: 'Topic 2', added_date: DateTime.parse('2023-01-16T12:00:00Z'),
            reply: false, post_number: 1 }
        ]
      )
    end

    it 'skips posts with no matching user_id' do
      allow(service).to receive(:fetch_user_id).with('user2').and_return(nil)

      result = service.process_posts(posts, topics_map)

      expect(result.size).to eq(1)
      expect(result.first[:id]).to eq(1)
    end
  end

  describe '#fetch_all_posts' do
    let(:added_date) { DateTime.parse('2023-01-15T12:00:00Z') }

    before do
      posts = [
        { 'id' => 1, 'username' => 'user1', 'topic_id' => 8, 'created_at' => '2023-01-15T12:00:00Z' },
        { 'id' => 2, 'username' => 'user2', 'topic_id' => 9, 'created_at' => '2023-01-16T12:00:00Z' }
      ]
      topics = [
        { 'id' => 8, 'title' => 'Topic' },
        { 'id' => 9, 'title' => 'Topic 2' }
      ]
      page_one_posts = {
        'posts' => posts, 'topics' => topics, 'grouped_search_result' => { 'more_full_page_results' => true }
      }
      page_two_posts = {
        'posts' => [], 'topics' => [], 'grouped_search_result' => { 'more_full_page_results' => false }
      }
      process_posts = [{ id: 1, user_id: 1, topic_id: 8, topic_title: 'Topic', added_date:, reply: true }]

      allow(service).to receive(:fetch_posts).and_return(page_one_posts, page_two_posts)
      allow(service).to receive(:process_posts).and_return(process_posts)
    end

    it 'fetches and processes all posts' do
      allow(ForumPost).to receive(:upsert_all)

      service.fetch_all_posts(start_date, end_date)

      expect(ForumPost).to have_received(:upsert_all).with(
        [
          { id: 1, user_id: 1, topic_id: 8, topic_title: 'Topic', added_date:, reply: true }
        ]
      )
    end

    it 'does not call upsert_all if no posts are found' do
      allow(ForumPost).to receive(:upsert_all)
      allow(service).to receive(:process_posts).and_return([])

      service.fetch_all_posts(start_date, end_date)

      expect(ForumPost).not_to have_received(:upsert_all)
    end
  end
end
