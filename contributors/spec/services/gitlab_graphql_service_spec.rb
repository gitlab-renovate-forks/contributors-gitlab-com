# frozen_string_literal: true

RSpec.describe GitlabGraphqlService do
  subject(:service) { described_class }

  let(:grapql_options) do
    cookies = HTTParty::CookieHash.new
    cookies.add_cookies('gitlab_canary=true')
    cookies = cookies.to_cookie_string
    {
      body: { query: 'test_query' }.to_json,
      headers: {
        'Content-Type' => 'application/json',
        'Cookie' => cookies,
        'User-Agent' => 'contributorsDot'
      }
    }
  end
  let(:response) do
    instance_double(HTTParty::Response, code: 200, parsed_response: 'test')
  end

  before do
    allow(described_class).to receive(:sleep)
  end

  shared_examples 'succeeds' do |with_retries: true|
    before do
      allow(HTTParty).to receive(:post)
        .with(
          described_class::CI_API_GRAPHQL_URL,
          grapql_options
        ).and_return(response)
    end

    it 'makes a single post request returns parsed_response' do
      expect(service.execute('test_query', with_retries:)).to eq(response.parsed_response)
      expect(described_class).not_to have_received(:sleep)
      expect(HTTParty).to have_received(:post).once
    end
  end

  shared_examples 'fails' do |with_retries: true|
    before do
      allow(HTTParty).to receive(:post)
        .with(
          described_class::CI_API_GRAPHQL_URL,
          grapql_options
        ).and_raise(StandardError)
    end

    it 'raises an error and retries if instructed' do
      post_requests = with_retries ? described_class::MAX_TRIES : 1

      expect { service.execute('test_query', with_retries:) }.to raise_error(StandardError)

      expect(described_class).to have_received(:sleep).exactly(post_requests - 1).times
      expect(HTTParty).to have_received(:post).exactly(post_requests).times
    end
  end

  context 'with retry' do
    context 'without any issues' do
      it_behaves_like 'succeeds'
    end

    context 'when the last request succeeds (and all others fail)' do
      it 'returns response' do
        attempts = 1
        allow(HTTParty).to receive(:post)
          .with(
            described_class::CI_API_GRAPHQL_URL,
            grapql_options
          ) do
            if attempts < described_class::MAX_TRIES
              attempts += 1
              raise(StandardError)
            end

            response
          end

        expect(service.execute('test_query')).to eq(response.parsed_response)
        expect(HTTParty).to have_received(:post).exactly(described_class::MAX_TRIES).times
      end
    end

    context 'when retries limit hits' do
      it_behaves_like 'fails'
    end
  end

  context 'without retry' do
    context 'without any issues' do
      it_behaves_like 'succeeds', with_retries: false
    end

    context 'when single request fails' do
      it_behaves_like 'fails', with_retries: false
    end
  end
end
