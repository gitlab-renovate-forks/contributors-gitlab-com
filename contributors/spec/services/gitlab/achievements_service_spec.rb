# frozen_string_literal: true

RSpec.describe Gitlab::AchievementsService, :vcr do
  let(:user_id) { 12_687_636 }

  before do
    stub_env('GITLAB_API_TOKEN', 'glpat-xxxxxxxxxxxxxxxxxxxx')
  end

  describe '.current' do
    it 'makes expected GraphQL query' do
      current = described_class.current(user_id)

      expect(current.count).to be(6)
      expect(current.first).to match hash_including(
        'id' => 'gid://gitlab/Achievements::UserAchievement/1',
        'achievement' => {
          'id' => 'gid://gitlab/Achievements::Achievement/1'
        }
      )
    end
  end

  describe '.award' do
    it 'makes expected GraphQL mutation' do
      achievement_gid = 'gid://gitlab/Achievements::Achievement/1000069'

      result = described_class.award(user_id, achievement_gid)

      expect(result.dig('data', 'achievementsAward', 'errors')).to be_empty
    end
  end

  describe '.revoke' do
    it 'makes expected GraphQL mutation' do
      user_achievement_gid = 'gid://gitlab/Achievements::UserAchievement/1005766'

      result = described_class.revoke(user_achievement_gid)

      expect(result.dig('data', 'achievementsRevoke', 'errors')).to be_empty
    end
  end
end
