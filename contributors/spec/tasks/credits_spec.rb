# frozen_string_literal: true

RSpec.describe 'credits', type: :task do
  before do
    Rake.application.rake_require 'tasks/credits'
    Rake::Task.define_task(:environment)
  end

  describe 'award_for_first_contribution' do
    let(:credits_service) { instance_double(Credits::FirstContributionService) }

    it 'executes the task and calls the correct methods' do
      allow(Credits::FirstContributionService).to receive(:new).and_return(credits_service)
      allow(credits_service).to receive(:execute)

      run_rake_task('credits:award_for_first_contribution')

      expect(credits_service).to have_received(:execute)
    end
  end

  describe 'award_for_level_up' do
    let(:credits_service) { instance_double(Credits::LevelUpService) }

    it 'executes the task and calls the correct methods' do
      allow(Credits::LevelUpService).to receive(:new).and_return(credits_service)
      allow(credits_service).to receive(:execute)

      run_rake_task('credits:award_for_level_up')

      expect(credits_service).to have_received(:execute)
    end
  end
end
