# frozen_string_literal: true

RSpec.describe 'data', type: :task do
  before :all do # rubocop:disable RSpec/BeforeAfterAll
    Rake.application.rake_require 'tasks/data'
  end

  describe 'counts' do
    let(:task) { run_rake_task('data:counts') }

    it 'prints the data counts' do
      counts = [
        [Commit, 1],
        [Issue, 2],
        [MergeRequest, 3],
        [Note, 4],
        [ProcessedJob, 5],
        [Project, 6],
        [User, 7]
      ]
      counts.each do |model, count|
        allow(model).to receive(:count).and_return(count)
      end

      expect { task }.to output(
        "Commits: 1\nIssues: 2\nMerge requests: 3\nNotes: 4\nProcessed jobs: 5\nProjects: 6\nUsers: 7\n"
      ).to_stdout
    end
  end

  describe 'grab' do
    let(:grab_data_service) { instance_double(GrabDataService) }

    before do
      allow(GrabDataService).to receive(:new).and_return(grab_data_service)
    end

    context 'without any arguments' do
      let(:task) { run_rake_task('data:grab') }

      it 'calls GrabDataService.new.execute' do
        stub_env('GITLAB_USER_ID', 123)
        stub_env('GITLAB_API_TOKEN', 'glpat-xxxxxxxxxxxxx')
        Views::RecentActivitySummary.refresh(concurrently: false)
        allow(grab_data_service).to receive(:execute)

        task

        expect(grab_data_service).to have_received(:execute)
      end
    end

    context 'with a group' do
      let(:task) { run_rake_task('data:grab', 'group') }

      it 'calls GrabDataService.new.process_group with expected group argument' do
        allow(grab_data_service).to receive(:process_group)

        task

        expect(grab_data_service).to have_received(:process_group).with('group')
      end
    end
  end

  describe 'update_points' do
    let(:task) { run_rake_task('data:update_points') }

    it 'calls PointsService.new.fetch_data' do
      points_service = instance_double(PointsService)
      allow(PointsService).to receive(:new).and_return(points_service)
      allow(points_service).to receive(:fetch_data)

      task

      expect(PointsService).to have_received(:new).with(from_date: nil, to_date: nil, community_only: false)
      expect(points_service).to have_received(:fetch_data).with(save: true)
    end
  end

  describe 'update_users' do
    let(:task) { run_rake_task('data:update_users') }

    it 'calls UserUpdateService.new.execute' do
      service = instance_double(UserUpdateService)
      allow(UserUpdateService).to receive(:new).and_return(service)
      allow(service).to receive(:execute)

      task

      expect(UserUpdateService).to have_received(:new).with(no_args)
      expect(service).to have_received(:execute).with(no_args)
    end
  end

  describe 'clear' do
    before do
      create(:commit)
      create(:note, issue: create(:issue))
      create(:project)
      create(:processed_job)
      create(:reward)
      create(:discord_message)
      create(:forum_post)
    end

    let(:task) { run_rake_task('data:clear') }

    it 'clears all data' do
      expect { task }.to change(Commit, :count).to(0)
        .and change(Issue, :count).to(0)
        .and change(MergeRequest, :count).to(0)
        .and change(Note, :count).to(0)
        .and change(ProcessedJob, :count).to(0)
        .and change(Project, :count).to(0)
        .and change(User, :count).to(0)
        .and change(Reward, :count).to(0)
        .and change(DiscordMessage, :count).to(0)
        .and change(ForumPost, :count).to(0)
    end
  end

  describe 'seed' do
    let(:task) { run_rake_task('data:seed', '2') }

    it 'seeds all data' do
      Views::ActivitySummary.refresh(concurrently: false)

      expect { task }.to change(User, :count).to(2)
        .and change(MergeRequest, :count).to(20)
        .and change(Issue, :count).to(20)
        .and change(Note, :count).to(200)
        .and change(Commit, :count).to(20)
        .and change(Reward, :count).to(2)
        .and change(DiscordMessage, :count).to(2)
        .and change(ForumPost, :count).to(2)
    end
  end

  describe 'discord' do
    let(:task) { run_rake_task('data:discord', '2') }

    it 'calls DiscordService.process_messages with expected args' do
      service = instance_double(DiscordService)
      allow(DiscordService).to receive(:new).and_return(service)
      allow(service).to receive(:fetch_all_messages)

      task

      expect(service).to have_received(:fetch_all_messages).exactly(4).times
      4.times do |i|
        expect(service).to have_received(:fetch_all_messages).with(DiscordMessage::CHANNELS.keys[i], 2)
      end
    end
  end

  describe 'forum' do
    let(:task) { run_rake_task('data:forum', '2024-01-01', '2024-02-01') }

    it 'calls DiscourseService.fetch_all_posts with expected args' do
      service = instance_double(DiscourseService)
      allow(DiscourseService).to receive(:new).and_return(service)
      allow(service).to receive(:fetch_all_posts)

      task

      expect(service).to have_received(:fetch_all_posts).with('2024-01-01', '2024-02-01')
    end
  end

  describe 'grab_team_members' do
    let(:task) { run_rake_task('data:grab_team_members') }

    it 'calls Users::GrabTeamMembersService.execute' do
      service = instance_double(Users::GrabTeamMembersService)
      allow(Users::GrabTeamMembersService).to receive(:new).and_return(service)
      allow(service).to receive(:execute)

      task

      expect(service).to have_received(:execute)
    end
  end

  describe 'grab_community_members' do
    let(:task) { run_rake_task('data:grab_community_members') }

    it 'calls Users::GrabCommunityMembersService.execute' do
      service = instance_double(Users::GrabCommunityMembersService)
      allow(Users::GrabCommunityMembersService).to receive(:new).and_return(service)
      allow(service).to receive(:execute)

      task

      expect(service).to have_received(:execute)
    end
  end
end
