# frozen_string_literal: true

module ModelAssertions
  def self.included(base)
    base.class_eval do
      let(:instance) { create(described_class.model_name.singular) }

      it 'has a valid factory' do
        expect(instance).to be_valid
      end

      describe 'validations' do
        required_fields = base.metadata[:required_fields] || []
        required_fields.each do |attribute|
          it { is_expected.to validate_presence_of(attribute) }
        end

        (described_class.attribute_names - required_fields).each do |attribute|
          it { is_expected.not_to validate_presence_of(attribute) }
        end
      end
    end
  end
end
