# frozen_string_literal: true

RSpec.shared_examples 'sets last activity user timestamp' do
  it do
    stub = instance_double(Users::TrackActivityService, execute: true)
    allow(Users::TrackActivityService).to receive(:new).and_return(stub)

    http_request

    expect(stub).to have_received(:execute)
  end
end
