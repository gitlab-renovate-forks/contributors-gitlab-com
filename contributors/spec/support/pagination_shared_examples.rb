# frozen_string_literal: true

RSpec.shared_examples 'a paginated endpoint' do |key_attr_json: 'id', key_attr_record: nil|
  it 'responds with expected metadata' do
    get endpoint

    meta = json_body['metadata']
    expect(meta['current_page']).to eq(1)
    expect(meta['per_page']).to eq(20)
    expect(meta['total_count']).to eq(2)
  end

  context 'with page param' do
    let(:params) { { page_size: 1, page: 2 } }

    it 'responds expected records' do
      get(endpoint, params:)

      records, metadata = json_body.values_at('records', 'metadata')
      expect(metadata['total_count']).to eq(2)
      expect(metadata['per_page']).to eq(1)
      expect(metadata['current_page']).to eq(2)
      expect(records.size).to eq(1)
      expect(records.dig(0, key_attr_json)).to eq(second_record[key_attr_record || key_attr_json])
    end
  end

  describe 'with page_size param' do
    context 'when not provided' do
      let(:params) { {} }

      before do
        stub_const('PaginationConcern::DEFAULT_PAGE_SIZE', 1)
      end

      it 'responds with default limit' do
        get(endpoint, params:)

        records, metadata = json_body.values_at('records', 'metadata')
        expect(metadata['total_count']).to eq(2)
        expect(metadata['per_page']).to eq(1)
        expect(records.size).to eq(1)
        expect(records.dig(0, key_attr_json)).to eq(first_record[key_attr_record || key_attr_json])
      end
    end

    context 'when provided an invalid string' do
      let(:params) { { page_size: 'ten' } }

      before do
        stub_const('PaginationConcern::DEFAULT_PAGE_SIZE', 1)
      end

      it 'responds with the default page size' do
        get(endpoint, params:)

        records, metadata = json_body.values_at('records', 'metadata')
        expect(metadata['total_count']).to eq(2)
        expect(metadata['per_page']).to eq(1)
        expect(records.size).to eq(1)
        expect(records.dig(0, key_attr_json)).to eq(first_record[key_attr_record || key_attr_json])
      end
    end

    context 'when provided smaller number' do
      let(:params) { { page_size: '1' } }

      before do
        stub_const('PaginationConcern::DEFAULT_PAGE_SIZE', 0)
      end

      it 'responds with the correct page size' do
        get(endpoint, params:)

        records, metadata = json_body.values_at('records', 'metadata')
        expect(metadata['total_count']).to eq(2)
        expect(metadata['per_page']).to eq(1)
        expect(records.size).to eq(1)
        expect(records.dig(0, key_attr_json)).to eq(first_record[key_attr_record || key_attr_json])
      end
    end

    context 'when provided larger number' do
      let(:params) { { page_size: '3' } }

      before do
        stub_const('PaginationConcern::DEFAULT_PAGE_SIZE', 0)
      end

      it 'responds with the correct page size' do
        get(endpoint, params:)

        records, metadata = json_body.values_at('records', 'metadata')
        expect(metadata['total_count']).to eq(2)
        expect(metadata['per_page']).to eq(3)
        expect(records.size).to eq(2)
        expect(records.dig(0, key_attr_json)).to eq(first_record[key_attr_record || key_attr_json])
      end
    end
  end
end
