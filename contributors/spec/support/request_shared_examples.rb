# frozen_string_literal: true

RSpec.shared_examples 'an endpoint requiring login' do
  context 'when not logged in' do
    it 'returns an unauthorized status' do
      subject

      expect(response).to have_http_status(:unauthorized)
    end
  end

  context 'when logged in' do
    before do
      defined?(user) ? sign_in(user.id) : sign_in
    end

    it 'returns an ok status' do
      subject

      expect(response).to have_http_status(:ok)
    end
  end
end

RSpec.shared_examples 'an endpoint requiring admin' do |success_response|
  context 'when not logged in' do
    it 'returns an unauthorized status' do
      subject

      expect(response).to have_http_status(:unauthorized)
    end
  end

  context 'when logged in as a regular user' do
    before do
      sign_in
    end

    it 'returns a forbidden status' do
      subject

      expect(response).to have_http_status(:forbidden)
    end
  end

  context 'when logged in as an admin' do
    before do
      sign_in(admin: true)
    end

    it 'returns an ok status' do
      subject

      expect(response).to have_http_status(success_response || :ok)
    end
  end
end
