# frozen_string_literal: true

RSpec.describe Api::V1::ApiApplicationController do
  describe '.ensure_logged_in' do
    controller do
      before_action :ensure_logged_in

      def index
        render plain: 'success'
      end
    end

    context 'when the user is logged in' do
      it 'allows access to the action' do
        session[:username] = 'test_user'

        get :index

        expect(response).to have_http_status(:ok)
        expect(response.body).to eq('success')
      end
    end

    context 'when the user is not logged in' do
      it 'returns unauthorized response' do
        get :index

        expect(response).to have_http_status(:unauthorized)
        expect(json_body).to eq({ 'error' => 'Unauthorized' })
      end
    end
  end
end
