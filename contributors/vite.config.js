import { resolve } from 'path';

import graphql from '@rollup/plugin-graphql';
import vue from '@vitejs/plugin-vue';
import IconsResolve from 'unplugin-icons/resolver';
import Icons from 'unplugin-icons/vite';
import Components from 'unplugin-vue-components/vite';
import { defineConfig } from 'vite';
import RubyPlugin from 'vite-plugin-ruby';

export default defineConfig({
  plugins: [
    graphql(),
    RubyPlugin(),
    vue(),
    Components({
      resolvers: [IconsResolve()],
      dts: true,
    }),
    Icons({
      compiler: 'vue3',
      autoInstall: true,
    }),
  ],
  resolve: {
    alias: {
      '~': resolve(__dirname, 'app/javascript'),
      '@images': resolve(__dirname, 'app/assets/images'),
    },
  },
  test: {
    include: [resolve(__dirname, 'app/javascript/**/*.spec.js')],
    globals: true,
    environment: 'jsdom',
    coverage: {
      exclude: ['**/UserEditPage.vue'],
      provider: 'istanbul',
      reporter: ['cobertura', 'text'],
    },
    reporters: ['junit', 'default'],
    setupFiles: ['../vitest.setup.js'],
    outputFile: './coverage/junit.json',
  },
});
