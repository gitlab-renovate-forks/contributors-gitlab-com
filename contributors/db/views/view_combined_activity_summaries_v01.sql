SELECT
    *
FROM
    view_recent_activity_summaries
WHERE
    activity_date >= DATE_TRUNC('day', NOW())
UNION ALL
SELECT
    *
FROM
    view_activity_summaries
WHERE
    activity_date < DATE_TRUNC('day', NOW())
