SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET transaction_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: audits; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.audits (
    id bigint NOT NULL,
    auditable_id integer,
    auditable_type character varying,
    associated_id integer,
    associated_type character varying,
    user_id integer,
    user_type character varying,
    username character varying,
    action character varying,
    audited_changes text,
    version integer DEFAULT 0,
    comment character varying,
    remote_address character varying,
    request_uuid character varying,
    created_at timestamp(6) without time zone
);


--
-- Name: audits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.audits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: audits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.audits_id_seq OWNED BY public.audits.id;


--
-- Name: bonus_points; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bonus_points (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    awarded_by_user_id integer NOT NULL,
    points integer NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    reason character varying NOT NULL,
    activity_type smallint DEFAULT 0 NOT NULL
);


--
-- Name: bonus_points_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bonus_points_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bonus_points_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bonus_points_id_seq OWNED BY public.bonus_points.id;


--
-- Name: commits; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.commits (
    id bigint NOT NULL,
    sha bytea NOT NULL,
    merge_request_id bigint NOT NULL,
    user_id bigint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    committed_date timestamp(6) without time zone NOT NULL
);


--
-- Name: commits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.commits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: commits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.commits_id_seq OWNED BY public.commits.id;


--
-- Name: community_members; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.community_members (
    user_id bigint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: discord_messages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.discord_messages (
    user_id integer NOT NULL,
    reply boolean DEFAULT false NOT NULL,
    added_date timestamp without time zone NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    id character varying NOT NULL,
    channel_id character varying NOT NULL
);


--
-- Name: forum_posts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.forum_posts (
    id integer NOT NULL,
    user_id integer NOT NULL,
    topic_id integer NOT NULL,
    added_date timestamp without time zone NOT NULL,
    topic_title character varying NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    reply boolean DEFAULT false NOT NULL,
    post_number integer NOT NULL
);


--
-- Name: issues; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.issues (
    id integer NOT NULL,
    user_id bigint NOT NULL,
    project_id bigint,
    title character varying NOT NULL,
    state_id smallint NOT NULL,
    opened_date timestamp without time zone NOT NULL,
    closed_date timestamp without time zone,
    web_url character varying NOT NULL,
    upvotes smallint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    labels jsonb
);


--
-- Name: merge_requests; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.merge_requests (
    id integer NOT NULL,
    user_id bigint,
    project_id bigint,
    title character varying NOT NULL,
    state_id smallint NOT NULL,
    opened_date timestamp without time zone NOT NULL,
    merged_date timestamp without time zone,
    web_url character varying NOT NULL,
    upvotes smallint NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    labels jsonb
);


--
-- Name: notes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.notes (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    merge_request_id bigint,
    issue_id bigint,
    added_date timestamp without time zone NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: processed_jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.processed_jobs (
    id bigint NOT NULL,
    "group" character varying NOT NULL,
    issuable_type character varying NOT NULL,
    processed_records integer NOT NULL,
    elapsed integer NOT NULL,
    activity_date date NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: processed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.processed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: processed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.processed_jobs_id_seq OWNED BY public.processed_jobs.id;


--
-- Name: projects; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.projects (
    id integer NOT NULL,
    group_path character varying,
    name character varying,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: rewards; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.rewards (
    id bigint NOT NULL,
    user_id integer NOT NULL,
    awarded_by_user_id integer NOT NULL,
    credits integer NOT NULL,
    issue_iid integer NOT NULL,
    reason smallint NOT NULL,
    gift_code character varying NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: rewards_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.rewards_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rewards_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.rewards_id_seq OWNED BY public.rewards.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


--
-- Name: settings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.settings (
    id bigint NOT NULL,
    var character varying NOT NULL,
    value text,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.settings_id_seq OWNED BY public.settings.id;


--
-- Name: team_members; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.team_members (
    username character varying NOT NULL,
    mr_coach boolean DEFAULT false NOT NULL,
    available boolean DEFAULT true NOT NULL,
    departments jsonb DEFAULT '[]'::jsonb NOT NULL,
    projects jsonb DEFAULT '{}'::jsonb NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL
);


--
-- Name: user_profiles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_profiles (
    user_id bigint NOT NULL,
    community_member_override boolean,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    platform_first_login_at timestamp without time zone,
    platform_last_activity_at timestamp without time zone
);


--
-- Name: user_profiles_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_profiles_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_profiles_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_profiles_user_id_seq OWNED BY public.user_profiles.user_id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    username character varying NOT NULL,
    community_member boolean DEFAULT true NOT NULL,
    created_at timestamp(6) without time zone NOT NULL,
    updated_at timestamp(6) without time zone NOT NULL,
    points integer DEFAULT 0 NOT NULL,
    contributor_level integer DEFAULT 0 NOT NULL,
    discord_id character varying,
    organization character varying
);


--
-- Name: view_activity_summaries; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.view_activity_summaries AS
 WITH merge_requests_agg AS (
         SELECT merge_requests.user_id,
            date(merge_requests.opened_date) AS activity_date,
            count(1) AS opened_mrs
           FROM public.merge_requests
          GROUP BY merge_requests.user_id, (date(merge_requests.opened_date))
        ), merged_merge_requests_agg AS (
         SELECT merge_requests.user_id,
            date(merge_requests.merged_date) AS activity_date,
            count(1) AS merged_mrs,
            count(
                CASE
                    WHEN (merge_requests.labels @> '["linked-issue"]'::jsonb) THEN 1
                    ELSE NULL::integer
                END) AS merged_mrs_with_linked_issues
           FROM public.merge_requests
          WHERE (merge_requests.state_id = 3)
          GROUP BY merge_requests.user_id, (date(merge_requests.merged_date))
        ), issues_agg AS (
         SELECT issues.user_id,
            date(issues.opened_date) AS activity_date,
            count(1) AS opened_issues
           FROM public.issues
          GROUP BY issues.user_id, (date(issues.opened_date))
        ), commits_agg AS (
         SELECT commits.user_id,
            date(merge_requests.merged_date) AS activity_date,
            count(DISTINCT commits.merge_request_id) AS merged_commits
           FROM (public.merge_requests
             JOIN public.commits ON ((commits.merge_request_id = merge_requests.id)))
          WHERE (merge_requests.state_id = 3)
          GROUP BY commits.user_id, (date(merge_requests.merged_date))
        ), notes_agg AS (
         SELECT notes.user_id,
            date(notes.added_date) AS activity_date,
            count(1) AS added_notes
           FROM public.notes
          GROUP BY notes.user_id, (date(notes.added_date))
        ), bonus_points_agg AS (
         SELECT bonus_points.user_id,
            date(bonus_points.created_at) AS activity_date,
            sum(bonus_points.points) AS bonus_points
           FROM public.bonus_points
          GROUP BY bonus_points.user_id, (date(bonus_points.created_at))
        ), discord_messages_agg AS (
         SELECT discord_messages.user_id,
            date(discord_messages.added_date) AS activity_date,
            sum(
                CASE
                    WHEN (discord_messages.reply = false) THEN 1
                    ELSE 0
                END) AS discord_messages,
            sum(
                CASE
                    WHEN (discord_messages.reply = true) THEN 1
                    ELSE 0
                END) AS discord_replies
           FROM public.discord_messages
          GROUP BY discord_messages.user_id, (date(discord_messages.added_date))
        ), forum_posts_agg AS (
         SELECT forum_posts.user_id,
            date(forum_posts.added_date) AS activity_date,
            sum(
                CASE
                    WHEN (forum_posts.reply = false) THEN 1
                    ELSE 0
                END) AS forum_posts,
            sum(
                CASE
                    WHEN (forum_posts.reply = true) THEN 1
                    ELSE 0
                END) AS forum_replies
           FROM public.forum_posts
          GROUP BY forum_posts.user_id, (date(forum_posts.added_date))
        ), activity_dates AS (
         SELECT merge_requests_agg.user_id,
            merge_requests_agg.activity_date
           FROM merge_requests_agg
        UNION
         SELECT merged_merge_requests_agg.user_id,
            merged_merge_requests_agg.activity_date
           FROM merged_merge_requests_agg
        UNION
         SELECT issues_agg.user_id,
            issues_agg.activity_date
           FROM issues_agg
        UNION
         SELECT commits_agg.user_id,
            commits_agg.activity_date
           FROM commits_agg
        UNION
         SELECT notes_agg.user_id,
            notes_agg.activity_date
           FROM notes_agg
        UNION
         SELECT bonus_points_agg.user_id,
            bonus_points_agg.activity_date
           FROM bonus_points_agg
        UNION
         SELECT discord_messages_agg.user_id,
            discord_messages_agg.activity_date
           FROM discord_messages_agg
        UNION
         SELECT forum_posts_agg.user_id,
            forum_posts_agg.activity_date
           FROM forum_posts_agg
        ), counts AS (
         SELECT u.id AS user_id,
            u.username,
            COALESCE(p.community_member_override, u.community_member) AS community_member,
            u.contributor_level,
            d.activity_date,
            (COALESCE(mr.opened_mrs, (0)::bigint))::integer AS opened_mrs,
            (COALESCE(mmr.merged_mrs, (0)::bigint))::integer AS merged_mrs,
            (COALESCE(mmr.merged_mrs_with_linked_issues, (0)::bigint))::integer AS merged_mrs_with_linked_issues,
            (COALESCE(i.opened_issues, (0)::bigint))::integer AS opened_issues,
            (COALESCE(c.merged_commits, (0)::bigint))::integer AS merged_commits,
            (COALESCE(n.added_notes, (0)::bigint))::integer AS added_notes,
            (COALESCE(b.bonus_points, (0)::bigint))::integer AS bonus_points,
            (COALESCE(dm.discord_messages, (0)::bigint))::integer AS discord_messages,
            (COALESCE(dm.discord_replies, (0)::bigint))::integer AS discord_replies,
            (COALESCE(fp.forum_posts, (0)::bigint))::integer AS forum_posts,
            (COALESCE(fp.forum_replies, (0)::bigint))::integer AS forum_replies
           FROM ((((((((((public.users u
             JOIN activity_dates d ON ((u.id = d.user_id)))
             LEFT JOIN public.user_profiles p ON ((p.user_id = u.id)))
             LEFT JOIN merge_requests_agg mr ON (((u.id = mr.user_id) AND (d.activity_date = mr.activity_date))))
             LEFT JOIN merged_merge_requests_agg mmr ON (((u.id = mmr.user_id) AND (d.activity_date = mmr.activity_date))))
             LEFT JOIN issues_agg i ON (((u.id = i.user_id) AND (d.activity_date = i.activity_date))))
             LEFT JOIN commits_agg c ON (((u.id = c.user_id) AND (d.activity_date = c.activity_date))))
             LEFT JOIN notes_agg n ON (((u.id = n.user_id) AND (d.activity_date = n.activity_date))))
             LEFT JOIN bonus_points_agg b ON (((u.id = b.user_id) AND (d.activity_date = b.activity_date))))
             LEFT JOIN discord_messages_agg dm ON (((u.id = dm.user_id) AND (d.activity_date = dm.activity_date))))
             LEFT JOIN forum_posts_agg fp ON (((u.id = fp.user_id) AND (d.activity_date = fp.activity_date))))
        ), points AS (
         SELECT COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'created_issue_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 5) AS created_issue_points,
            COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'created_mr_author_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 20) AS created_mr_author_points,
            COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'merged_mr_author_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 60) AS merged_mr_author_points,
            COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'merged_mr_committer_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 20) AS merged_mr_committer_points,
            COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'merged_mr_with_issue_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 30) AS merged_mr_with_issue_points,
            COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'note_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 1) AS note_points,
            COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'discord_message_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 1) AS discord_message_points,
            COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'discord_reply_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 2) AS discord_reply_points,
            COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'forum_post_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 1) AS forum_post_points,
            COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'forum_reply_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 2) AS forum_reply_points
           FROM public.settings
          WHERE ((settings.var)::text ~~ '%_points'::text)
        )
 SELECT counts.user_id,
    counts.username,
    counts.community_member,
    counts.contributor_level,
    counts.activity_date,
    counts.opened_mrs,
    counts.merged_mrs,
    counts.merged_mrs_with_linked_issues,
    counts.opened_issues,
    counts.merged_commits,
    counts.added_notes,
    counts.bonus_points,
    counts.discord_messages,
    counts.discord_replies,
    counts.forum_posts,
    counts.forum_replies,
    points.created_issue_points,
    points.created_mr_author_points,
    points.merged_mr_author_points,
    points.merged_mr_committer_points,
    points.merged_mr_with_issue_points,
    points.note_points,
    points.discord_message_points,
    points.discord_reply_points,
    points.forum_post_points,
    points.forum_reply_points,
    (((((((((((counts.opened_issues * points.created_issue_points) + (counts.opened_mrs * points.created_mr_author_points)) + (counts.merged_mrs * points.merged_mr_author_points)) + (counts.merged_mrs_with_linked_issues * points.merged_mr_with_issue_points)) + (counts.merged_commits * points.merged_mr_committer_points)) + (counts.added_notes * points.note_points)) + counts.bonus_points) + (counts.discord_messages * points.discord_message_points)) + (counts.discord_replies * points.discord_reply_points)) + (counts.forum_posts * points.forum_post_points)) + (counts.forum_replies * points.forum_reply_points)) AS points
   FROM (counts
     CROSS JOIN points)
  WITH NO DATA;


--
-- Name: view_recent_activity_summaries; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.view_recent_activity_summaries AS
 WITH date_param AS (
         SELECT (date_trunc('day'::text, now()) - '90 days'::interval) AS cutoff_date
        ), merge_requests_agg AS (
         SELECT merge_requests.user_id,
            date(merge_requests.opened_date) AS activity_date,
            count(1) AS opened_mrs
           FROM public.merge_requests
          WHERE (merge_requests.opened_date >= ( SELECT date_param.cutoff_date
                   FROM date_param))
          GROUP BY merge_requests.user_id, (date(merge_requests.opened_date))
        ), merged_merge_requests_agg AS (
         SELECT merge_requests.user_id,
            date(merge_requests.merged_date) AS activity_date,
            count(1) AS merged_mrs,
            count(
                CASE
                    WHEN (merge_requests.labels @> '["linked-issue"]'::jsonb) THEN 1
                    ELSE NULL::integer
                END) AS merged_mrs_with_linked_issues
           FROM public.merge_requests
          WHERE ((merge_requests.state_id = 3) AND (merge_requests.merged_date >= ( SELECT date_param.cutoff_date
                   FROM date_param)))
          GROUP BY merge_requests.user_id, (date(merge_requests.merged_date))
        ), issues_agg AS (
         SELECT issues.user_id,
            date(issues.opened_date) AS activity_date,
            count(1) AS opened_issues
           FROM public.issues
          WHERE (issues.opened_date >= ( SELECT date_param.cutoff_date
                   FROM date_param))
          GROUP BY issues.user_id, (date(issues.opened_date))
        ), commits_agg AS (
         SELECT commits.user_id,
            date(merge_requests.merged_date) AS activity_date,
            count(DISTINCT commits.merge_request_id) AS merged_commits
           FROM (public.merge_requests
             JOIN public.commits ON ((commits.merge_request_id = merge_requests.id)))
          WHERE ((merge_requests.state_id = 3) AND (merge_requests.merged_date >= ( SELECT date_param.cutoff_date
                   FROM date_param)))
          GROUP BY commits.user_id, (date(merge_requests.merged_date))
        ), notes_agg AS (
         SELECT notes.user_id,
            date(notes.added_date) AS activity_date,
            count(1) AS added_notes
           FROM public.notes
          WHERE (notes.added_date >= ( SELECT date_param.cutoff_date
                   FROM date_param))
          GROUP BY notes.user_id, (date(notes.added_date))
        ), bonus_points_agg AS (
         SELECT bonus_points.user_id,
            date(bonus_points.created_at) AS activity_date,
            sum(bonus_points.points) AS bonus_points
           FROM public.bonus_points
          WHERE (bonus_points.created_at >= ( SELECT date_param.cutoff_date
                   FROM date_param))
          GROUP BY bonus_points.user_id, (date(bonus_points.created_at))
        ), discord_messages_agg AS (
         SELECT discord_messages.user_id,
            date(discord_messages.added_date) AS activity_date,
            sum(
                CASE
                    WHEN (discord_messages.reply = false) THEN 1
                    ELSE 0
                END) AS discord_messages,
            sum(
                CASE
                    WHEN (discord_messages.reply = true) THEN 1
                    ELSE 0
                END) AS discord_replies
           FROM public.discord_messages
          WHERE (discord_messages.added_date >= ( SELECT date_param.cutoff_date
                   FROM date_param))
          GROUP BY discord_messages.user_id, (date(discord_messages.added_date))
        ), forum_posts_agg AS (
         SELECT forum_posts.user_id,
            date(forum_posts.added_date) AS activity_date,
            sum(
                CASE
                    WHEN (forum_posts.reply = false) THEN 1
                    ELSE 0
                END) AS forum_posts,
            sum(
                CASE
                    WHEN (forum_posts.reply = true) THEN 1
                    ELSE 0
                END) AS forum_replies
           FROM public.forum_posts
          WHERE (forum_posts.added_date >= ( SELECT date_param.cutoff_date
                   FROM date_param))
          GROUP BY forum_posts.user_id, (date(forum_posts.added_date))
        ), activity_dates AS (
         SELECT merge_requests_agg.user_id,
            merge_requests_agg.activity_date
           FROM merge_requests_agg
        UNION
         SELECT merged_merge_requests_agg.user_id,
            merged_merge_requests_agg.activity_date
           FROM merged_merge_requests_agg
        UNION
         SELECT issues_agg.user_id,
            issues_agg.activity_date
           FROM issues_agg
        UNION
         SELECT commits_agg.user_id,
            commits_agg.activity_date
           FROM commits_agg
        UNION
         SELECT notes_agg.user_id,
            notes_agg.activity_date
           FROM notes_agg
        UNION
         SELECT bonus_points_agg.user_id,
            bonus_points_agg.activity_date
           FROM bonus_points_agg
        UNION
         SELECT discord_messages_agg.user_id,
            discord_messages_agg.activity_date
           FROM discord_messages_agg
        UNION
         SELECT forum_posts_agg.user_id,
            forum_posts_agg.activity_date
           FROM forum_posts_agg
        ), counts AS (
         SELECT u.id AS user_id,
            u.username,
            COALESCE(p.community_member_override, u.community_member) AS community_member,
            u.contributor_level,
            d.activity_date,
            (COALESCE(mr.opened_mrs, (0)::bigint))::integer AS opened_mrs,
            (COALESCE(mmr.merged_mrs, (0)::bigint))::integer AS merged_mrs,
            (COALESCE(mmr.merged_mrs_with_linked_issues, (0)::bigint))::integer AS merged_mrs_with_linked_issues,
            (COALESCE(i.opened_issues, (0)::bigint))::integer AS opened_issues,
            (COALESCE(c.merged_commits, (0)::bigint))::integer AS merged_commits,
            (COALESCE(n.added_notes, (0)::bigint))::integer AS added_notes,
            (COALESCE(b.bonus_points, (0)::bigint))::integer AS bonus_points,
            (COALESCE(dm.discord_messages, (0)::bigint))::integer AS discord_messages,
            (COALESCE(dm.discord_replies, (0)::bigint))::integer AS discord_replies,
            (COALESCE(fp.forum_posts, (0)::bigint))::integer AS forum_posts,
            (COALESCE(fp.forum_replies, (0)::bigint))::integer AS forum_replies
           FROM ((((((((((public.users u
             JOIN activity_dates d ON ((u.id = d.user_id)))
             LEFT JOIN public.user_profiles p ON ((p.user_id = u.id)))
             LEFT JOIN merge_requests_agg mr ON (((u.id = mr.user_id) AND (d.activity_date = mr.activity_date))))
             LEFT JOIN merged_merge_requests_agg mmr ON (((u.id = mmr.user_id) AND (d.activity_date = mmr.activity_date))))
             LEFT JOIN issues_agg i ON (((u.id = i.user_id) AND (d.activity_date = i.activity_date))))
             LEFT JOIN commits_agg c ON (((u.id = c.user_id) AND (d.activity_date = c.activity_date))))
             LEFT JOIN notes_agg n ON (((u.id = n.user_id) AND (d.activity_date = n.activity_date))))
             LEFT JOIN bonus_points_agg b ON (((u.id = b.user_id) AND (d.activity_date = b.activity_date))))
             LEFT JOIN discord_messages_agg dm ON (((u.id = dm.user_id) AND (d.activity_date = dm.activity_date))))
             LEFT JOIN forum_posts_agg fp ON (((u.id = fp.user_id) AND (d.activity_date = fp.activity_date))))
        ), points AS (
         SELECT COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'created_issue_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 5) AS created_issue_points,
            COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'created_mr_author_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 20) AS created_mr_author_points,
            COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'merged_mr_author_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 60) AS merged_mr_author_points,
            COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'merged_mr_committer_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 20) AS merged_mr_committer_points,
            COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'merged_mr_with_issue_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 30) AS merged_mr_with_issue_points,
            COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'note_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 1) AS note_points,
            COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'discord_message_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 1) AS discord_message_points,
            COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'discord_reply_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 2) AS discord_reply_points,
            COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'forum_post_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 1) AS forum_post_points,
            COALESCE(max(
                CASE
                    WHEN ((settings.var)::text = 'forum_reply_points'::text) THEN (replace(settings.value, '---'::text, ''::text))::integer
                    ELSE NULL::integer
                END), 2) AS forum_reply_points
           FROM public.settings
          WHERE ((settings.var)::text ~~ '%_points'::text)
        )
 SELECT counts.user_id,
    counts.username,
    counts.community_member,
    counts.contributor_level,
    counts.activity_date,
    counts.opened_mrs,
    counts.merged_mrs,
    counts.merged_mrs_with_linked_issues,
    counts.opened_issues,
    counts.merged_commits,
    counts.added_notes,
    counts.bonus_points,
    counts.discord_messages,
    counts.discord_replies,
    counts.forum_posts,
    counts.forum_replies,
    points.created_issue_points,
    points.created_mr_author_points,
    points.merged_mr_author_points,
    points.merged_mr_committer_points,
    points.merged_mr_with_issue_points,
    points.note_points,
    points.discord_message_points,
    points.discord_reply_points,
    points.forum_post_points,
    points.forum_reply_points,
    (((((((((((counts.opened_issues * points.created_issue_points) + (counts.opened_mrs * points.created_mr_author_points)) + (counts.merged_mrs * points.merged_mr_author_points)) + (counts.merged_mrs_with_linked_issues * points.merged_mr_with_issue_points)) + (counts.merged_commits * points.merged_mr_committer_points)) + (counts.added_notes * points.note_points)) + counts.bonus_points) + (counts.discord_messages * points.discord_message_points)) + (counts.discord_replies * points.discord_reply_points)) + (counts.forum_posts * points.forum_post_points)) + (counts.forum_replies * points.forum_reply_points)) AS points
   FROM (counts
     CROSS JOIN points)
  WITH NO DATA;


--
-- Name: view_combined_activity_summaries; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.view_combined_activity_summaries AS
 SELECT view_recent_activity_summaries.user_id,
    view_recent_activity_summaries.username,
    view_recent_activity_summaries.community_member,
    view_recent_activity_summaries.contributor_level,
    view_recent_activity_summaries.activity_date,
    view_recent_activity_summaries.opened_mrs,
    view_recent_activity_summaries.merged_mrs,
    view_recent_activity_summaries.merged_mrs_with_linked_issues,
    view_recent_activity_summaries.opened_issues,
    view_recent_activity_summaries.merged_commits,
    view_recent_activity_summaries.added_notes,
    view_recent_activity_summaries.bonus_points,
    view_recent_activity_summaries.discord_messages,
    view_recent_activity_summaries.discord_replies,
    view_recent_activity_summaries.forum_posts,
    view_recent_activity_summaries.forum_replies,
    view_recent_activity_summaries.created_issue_points,
    view_recent_activity_summaries.created_mr_author_points,
    view_recent_activity_summaries.merged_mr_author_points,
    view_recent_activity_summaries.merged_mr_committer_points,
    view_recent_activity_summaries.merged_mr_with_issue_points,
    view_recent_activity_summaries.note_points,
    view_recent_activity_summaries.discord_message_points,
    view_recent_activity_summaries.discord_reply_points,
    view_recent_activity_summaries.forum_post_points,
    view_recent_activity_summaries.forum_reply_points,
    view_recent_activity_summaries.points
   FROM public.view_recent_activity_summaries
  WHERE (view_recent_activity_summaries.activity_date >= date_trunc('day'::text, now()))
UNION ALL
 SELECT view_activity_summaries.user_id,
    view_activity_summaries.username,
    view_activity_summaries.community_member,
    view_activity_summaries.contributor_level,
    view_activity_summaries.activity_date,
    view_activity_summaries.opened_mrs,
    view_activity_summaries.merged_mrs,
    view_activity_summaries.merged_mrs_with_linked_issues,
    view_activity_summaries.opened_issues,
    view_activity_summaries.merged_commits,
    view_activity_summaries.added_notes,
    view_activity_summaries.bonus_points,
    view_activity_summaries.discord_messages,
    view_activity_summaries.discord_replies,
    view_activity_summaries.forum_posts,
    view_activity_summaries.forum_replies,
    view_activity_summaries.created_issue_points,
    view_activity_summaries.created_mr_author_points,
    view_activity_summaries.merged_mr_author_points,
    view_activity_summaries.merged_mr_committer_points,
    view_activity_summaries.merged_mr_with_issue_points,
    view_activity_summaries.note_points,
    view_activity_summaries.discord_message_points,
    view_activity_summaries.discord_reply_points,
    view_activity_summaries.forum_post_points,
    view_activity_summaries.forum_reply_points,
    view_activity_summaries.points
   FROM public.view_activity_summaries
  WHERE (view_activity_summaries.activity_date < date_trunc('day'::text, now()));


--
-- Name: audits id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.audits ALTER COLUMN id SET DEFAULT nextval('public.audits_id_seq'::regclass);


--
-- Name: bonus_points id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bonus_points ALTER COLUMN id SET DEFAULT nextval('public.bonus_points_id_seq'::regclass);


--
-- Name: commits id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.commits ALTER COLUMN id SET DEFAULT nextval('public.commits_id_seq'::regclass);


--
-- Name: processed_jobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.processed_jobs ALTER COLUMN id SET DEFAULT nextval('public.processed_jobs_id_seq'::regclass);


--
-- Name: rewards id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rewards ALTER COLUMN id SET DEFAULT nextval('public.rewards_id_seq'::regclass);


--
-- Name: settings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.settings ALTER COLUMN id SET DEFAULT nextval('public.settings_id_seq'::regclass);


--
-- Name: user_profiles user_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_profiles ALTER COLUMN user_id SET DEFAULT nextval('public.user_profiles_user_id_seq'::regclass);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: audits audits_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.audits
    ADD CONSTRAINT audits_pkey PRIMARY KEY (id);


--
-- Name: bonus_points bonus_points_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bonus_points
    ADD CONSTRAINT bonus_points_pkey PRIMARY KEY (id);


--
-- Name: commits commits_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.commits
    ADD CONSTRAINT commits_pkey PRIMARY KEY (id);


--
-- Name: community_members community_members_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.community_members
    ADD CONSTRAINT community_members_pkey PRIMARY KEY (user_id);


--
-- Name: discord_messages discord_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.discord_messages
    ADD CONSTRAINT discord_messages_pkey PRIMARY KEY (id);


--
-- Name: forum_posts forum_posts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.forum_posts
    ADD CONSTRAINT forum_posts_pkey PRIMARY KEY (id);


--
-- Name: issues issues_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues
    ADD CONSTRAINT issues_pkey PRIMARY KEY (id);


--
-- Name: merge_requests merge_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.merge_requests
    ADD CONSTRAINT merge_requests_pkey PRIMARY KEY (id);


--
-- Name: notes notes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notes
    ADD CONSTRAINT notes_pkey PRIMARY KEY (id);


--
-- Name: processed_jobs processed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.processed_jobs
    ADD CONSTRAINT processed_jobs_pkey PRIMARY KEY (id);


--
-- Name: projects projects_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (id);


--
-- Name: rewards rewards_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rewards
    ADD CONSTRAINT rewards_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: settings settings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: team_members team_members_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.team_members
    ADD CONSTRAINT team_members_pkey PRIMARY KEY (username);


--
-- Name: user_profiles user_profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_profiles
    ADD CONSTRAINT user_profiles_pkey PRIMARY KEY (user_id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: associated_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX associated_index ON public.audits USING btree (associated_type, associated_id);


--
-- Name: auditable_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auditable_index ON public.audits USING btree (auditable_type, auditable_id, version);


--
-- Name: idx_on_activity_date_user_id_0500248e27; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX idx_on_activity_date_user_id_0500248e27 ON public.view_recent_activity_summaries USING btree (activity_date, user_id);


--
-- Name: idx_on_community_member_activity_date_0921da6fa0; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_on_community_member_activity_date_0921da6fa0 ON public.view_recent_activity_summaries USING btree (community_member, activity_date);


--
-- Name: idx_on_community_member_activity_date_ecd69ee17b; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_on_community_member_activity_date_ecd69ee17b ON public.view_activity_summaries USING btree (community_member, activity_date);


--
-- Name: idx_on_group_issuable_type_activity_date_dccdd7e1b8; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_on_group_issuable_type_activity_date_dccdd7e1b8 ON public.processed_jobs USING btree ("group", issuable_type, activity_date DESC);


--
-- Name: index_audits_on_created_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_audits_on_created_at ON public.audits USING btree (created_at);


--
-- Name: index_audits_on_request_uuid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_audits_on_request_uuid ON public.audits USING btree (request_uuid);


--
-- Name: index_bonus_points_on_activity_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_bonus_points_on_activity_type ON public.bonus_points USING btree (activity_type);


--
-- Name: index_bonus_points_on_awarded_by_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_bonus_points_on_awarded_by_user_id ON public.bonus_points USING btree (awarded_by_user_id);


--
-- Name: index_bonus_points_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_bonus_points_on_user_id ON public.bonus_points USING btree (user_id);


--
-- Name: index_commits_on_committed_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_commits_on_committed_date ON public.commits USING btree (committed_date);


--
-- Name: index_commits_on_merge_request_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_commits_on_merge_request_id ON public.commits USING btree (merge_request_id);


--
-- Name: index_commits_on_sha; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_commits_on_sha ON public.commits USING btree (sha);


--
-- Name: index_commits_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_commits_on_user_id ON public.commits USING btree (user_id);


--
-- Name: index_discord_messages_on_added_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_discord_messages_on_added_date ON public.discord_messages USING btree (added_date);


--
-- Name: index_discord_messages_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_discord_messages_on_user_id ON public.discord_messages USING btree (user_id);


--
-- Name: index_forum_posts_on_added_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_forum_posts_on_added_date ON public.forum_posts USING btree (added_date);


--
-- Name: index_forum_posts_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_forum_posts_on_user_id ON public.forum_posts USING btree (user_id);


--
-- Name: index_issues_on_opened_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_issues_on_opened_date ON public.issues USING btree (opened_date);


--
-- Name: index_issues_on_project_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_issues_on_project_id ON public.issues USING btree (project_id);


--
-- Name: index_issues_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_issues_on_user_id ON public.issues USING btree (user_id);


--
-- Name: index_merge_requests_on_opened_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_merge_requests_on_opened_date ON public.merge_requests USING btree (opened_date);


--
-- Name: index_merge_requests_on_project_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_merge_requests_on_project_id ON public.merge_requests USING btree (project_id);


--
-- Name: index_merge_requests_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_merge_requests_on_user_id ON public.merge_requests USING btree (user_id);


--
-- Name: index_notes_on_added_date; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_notes_on_added_date ON public.notes USING btree (added_date);


--
-- Name: index_notes_on_issue_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_notes_on_issue_id ON public.notes USING btree (issue_id);


--
-- Name: index_notes_on_merge_request_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_notes_on_merge_request_id ON public.notes USING btree (merge_request_id);


--
-- Name: index_notes_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_notes_on_user_id ON public.notes USING btree (user_id);


--
-- Name: index_rewards_on_reason; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_rewards_on_reason ON public.rewards USING btree (reason);


--
-- Name: index_rewards_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_rewards_on_user_id ON public.rewards USING btree (user_id);


--
-- Name: index_settings_on_var; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_settings_on_var ON public.settings USING btree (var);


--
-- Name: index_team_members_on_mr_coach; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_team_members_on_mr_coach ON public.team_members USING btree (mr_coach);


--
-- Name: index_user_profiles_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_user_profiles_on_user_id ON public.user_profiles USING btree (user_id);


--
-- Name: index_users_on_contributor_level; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_contributor_level ON public.users USING btree (contributor_level);


--
-- Name: index_users_on_discord_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_discord_id ON public.users USING btree (discord_id);


--
-- Name: index_users_on_lowercase_username; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_lowercase_username ON public.users USING btree (lower((username)::text));


--
-- Name: index_users_on_points; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_points ON public.users USING btree (points);


--
-- Name: index_view_activity_summaries_on_activity_date_and_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_view_activity_summaries_on_activity_date_and_user_id ON public.view_activity_summaries USING btree (activity_date, user_id);


--
-- Name: user_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_index ON public.audits USING btree (user_id, user_type);


--
-- Name: notes fk_rails_1484ba33f2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notes
    ADD CONSTRAINT fk_rails_1484ba33f2 FOREIGN KEY (merge_request_id) REFERENCES public.merge_requests(id) ON DELETE CASCADE;


--
-- Name: commits fk_rails_409a66d7e3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.commits
    ADD CONSTRAINT fk_rails_409a66d7e3 FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: merge_requests fk_rails_51d2c55ea2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.merge_requests
    ADD CONSTRAINT fk_rails_51d2c55ea2 FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: notes fk_rails_707b60f3ed; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notes
    ADD CONSTRAINT fk_rails_707b60f3ed FOREIGN KEY (issue_id) REFERENCES public.issues(id) ON DELETE CASCADE;


--
-- Name: notes fk_rails_7f2323ad43; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notes
    ADD CONSTRAINT fk_rails_7f2323ad43 FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: issues fk_rails_899c8f3231; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues
    ADD CONSTRAINT fk_rails_899c8f3231 FOREIGN KEY (project_id) REFERENCES public.projects(id);


--
-- Name: merge_requests fk_rails_8b877f6e15; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.merge_requests
    ADD CONSTRAINT fk_rails_8b877f6e15 FOREIGN KEY (project_id) REFERENCES public.projects(id);


--
-- Name: commits fk_rails_acd7a5243c; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.commits
    ADD CONSTRAINT fk_rails_acd7a5243c FOREIGN KEY (merge_request_id) REFERENCES public.merge_requests(id) ON DELETE CASCADE;


--
-- Name: issues fk_rails_f8f1052133; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.issues
    ADD CONSTRAINT fk_rails_f8f1052133 FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20250222193307'),
('20250221212244'),
('20250207130802'),
('20250130203829'),
('20250127205730'),
('20250103115937'),
('20241223211351'),
('20241211183052'),
('20241108202047'),
('20241101093318'),
('20241017221723'),
('20241015120318'),
('20241015082445'),
('20241014213420'),
('20241009105939'),
('20241003212615'),
('20241001122334'),
('20240930071900'),
('20240926143556'),
('20240925213853'),
('20240923125529'),
('20240920215701'),
('20240917135617'),
('20240917132216'),
('20240915200534'),
('20240913195921'),
('20240912090925'),
('20240912090914'),
('20240902134832'),
('20240902133507'),
('20240617102559'),
('20240617101832'),
('20240611192636'),
('20240611144806'),
('20240420210641'),
('20240418205648'),
('20240417193203'),
('20240417192045'),
('20240417191818'),
('20240410130855');

