# frozen_string_literal: true

class AddPointsToUsers < ActiveRecord::Migration[7.2]
  def change
    add_column :users, :points, :integer, default: 0, null: false

    reversible do |migration|
      migration.up { add_index :users, :points }
    end
  end
end
