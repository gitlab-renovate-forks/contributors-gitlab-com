# frozen_string_literal: true

class AddCommittedDate < ActiveRecord::Migration[7.1]
  def up
    Commit.delete_all
    add_column :commits, :committed_date, :datetime, null: false # rubocop:disable Rails/NotNullColumn

    add_index :commits, :committed_date
  end

  def down
    remove_column :commits, :committed_date
  end
end
