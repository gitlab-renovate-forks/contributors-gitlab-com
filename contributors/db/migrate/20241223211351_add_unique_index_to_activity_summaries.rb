# frozen_string_literal: true

class AddUniqueIndexToActivitySummaries < ActiveRecord::Migration[8.0]
  def change
    add_index :view_activity_summaries, %i[activity_date user_id], unique: true
  end
end
