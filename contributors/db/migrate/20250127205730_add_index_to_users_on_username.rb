# frozen_string_literal: true

class AddIndexToUsersOnUsername < ActiveRecord::Migration[8.0]
  def change
    add_index :users, 'LOWER(username)', name: 'index_users_on_lowercase_username', unique: true
  end
end
