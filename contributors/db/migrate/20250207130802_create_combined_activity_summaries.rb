# frozen_string_literal: true

class CreateCombinedActivitySummaries < ActiveRecord::Migration[8.0]
  def change
    create_view :view_combined_activity_summaries
  end
end
