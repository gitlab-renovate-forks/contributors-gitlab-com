# frozen_string_literal: true

class AddDateIndexes < ActiveRecord::Migration[7.1]
  def change
    # Add index on created_date to issues and merge_requests tables
    add_index :issues, :opened_date
    add_index :merge_requests, :opened_date
    add_index :notes, :added_date
  end
end
