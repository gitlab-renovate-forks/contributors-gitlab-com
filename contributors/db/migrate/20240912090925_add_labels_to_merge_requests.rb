# frozen_string_literal: true

class AddLabelsToMergeRequests < ActiveRecord::Migration[7.2]
  def change
    add_column :merge_requests, :labels, :jsonb
  end
end
