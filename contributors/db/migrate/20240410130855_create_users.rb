# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[7.1]
  def change
    create_table :users, id: false do |t|
      t.integer :id, null: false # rubocop:disable Rails/DangerousColumnNames
      t.string :username, null: false
      t.boolean :community_member, null: false, default: true

      t.timestamps
    end

    reversible do |migration|
      migration.up { execute 'ALTER TABLE users ADD PRIMARY KEY (id);' }
    end
  end
end
