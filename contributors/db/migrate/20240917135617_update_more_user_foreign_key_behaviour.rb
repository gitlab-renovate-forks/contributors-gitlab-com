# frozen_string_literal: true

class UpdateMoreUserForeignKeyBehaviour < ActiveRecord::Migration[7.2]
  def change
    remove_foreign_key :notes, :merge_requests
    add_foreign_key :notes, :merge_requests, on_delete: :cascade

    remove_foreign_key :notes, :issues
    add_foreign_key :notes, :issues, on_delete: :cascade

    remove_foreign_key :commits, :merge_requests
    add_foreign_key :commits, :merge_requests, on_delete: :cascade
  end
end
