# frozen_string_literal: true

class CreateCommits < ActiveRecord::Migration[7.1]
  def change
    create_table :commits do |t|
      t.binary :sha, null: false
      t.references :merge_request, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end

    reversible do |migration|
      migration.up do
        add_index :commits, :sha, unique: true
      end
    end
  end
end
