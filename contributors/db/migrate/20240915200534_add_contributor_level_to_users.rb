# frozen_string_literal: true

class AddContributorLevelToUsers < ActiveRecord::Migration[7.2]
  def change
    add_column :users, :contributor_level, :integer, default: 0, null: false

    reversible do |migration|
      migration.up { add_index :users, :contributor_level }
    end
  end
end
