# frozen_string_literal: true

class CreateViewActivitySummaries < ActiveRecord::Migration[7.2]
  def change
    create_view :view_activity_summaries, materialized: true

    reversible do |migration|
      migration.up do
        add_index :view_activity_summaries, %i[community_member activity_date]
      end
    end
  end
end
