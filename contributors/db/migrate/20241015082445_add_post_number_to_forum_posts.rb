# frozen_string_literal: true

class AddPostNumberToForumPosts < ActiveRecord::Migration[7.2]
  def change
    add_column :forum_posts, :post_number, :integer

    reversible do |migration|
      migration.up do
        execute 'DELETE FROM forum_posts;'
        change_column_null :forum_posts, :post_number, false
      end
    end
  end
end
