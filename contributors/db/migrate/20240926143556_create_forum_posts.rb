# frozen_string_literal: true

class CreateForumPosts < ActiveRecord::Migration[7.2]
  def change
    create_table :forum_posts, id: false do |t|
      t.integer :id, null: false # rubocop:disable Rails/DangerousColumnNames
      t.integer :user_id, null: false
      t.integer :topic_id, null: false
      t.timestamp :added_date, null: false
      t.string :topic_title, null: false

      t.timestamps
    end

    add_index :forum_posts, :user_id
    add_index :forum_posts, :added_date

    reversible do |migration|
      migration.up { execute 'ALTER TABLE forum_posts ADD PRIMARY KEY (id);' }
    end
  end
end
