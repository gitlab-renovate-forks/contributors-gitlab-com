# frozen_string_literal: true

class CreateRewards < ActiveRecord::Migration[7.2]
  def change
    create_table :rewards do |t|
      t.integer :user_id, null: false
      t.integer :awarded_by_user_id, null: false
      t.integer :points, null: false
      t.integer :issue_iid, null: false
      t.integer :reason, limit: 2, null: false
      t.string :gift_code, null: false

      t.timestamps
    end

    reversible do |migration|
      migration.up do
        add_index :rewards, :user_id
        add_index :rewards, :reason
      end
    end
  end
end
