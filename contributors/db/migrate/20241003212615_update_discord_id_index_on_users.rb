# frozen_string_literal: true

class UpdateDiscordIdIndexOnUsers < ActiveRecord::Migration[7.2]
  def change
    remove_index :users, :discord_id

    add_index :users, :discord_id
  end
end
