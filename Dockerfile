FROM ruby:3.4.2

ENV DB_INSTANCE_CONNECTION_NAME=$DB_INSTANCE_CONNECTION_NAME
ENV DB_NAME=$DB_NAME
ENV DB_USERNAME=$DB_USERNAME

RUN wget -O cloud-sql-proxy https://storage.googleapis.com/cloud-sql-connectors/cloud-sql-proxy/v2.10.1/cloud-sql-proxy.linux.amd64
RUN chmod +x cloud-sql-proxy

COPY start.sh .

WORKDIR /contributors

COPY contributors /contributors
COPY *.md /contributors/public/docs/

RUN bundle install

CMD /start.sh
