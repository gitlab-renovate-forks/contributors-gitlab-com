# GitLab contributor platform

[https://contributors.gitlab.com](https://contributors.gitlab.com)

The contributor platform is a central application for contributors to track/manage
their contributions, and GitLab team members to recognize and reward contributions.
It tracks activity across GitLab and social platforms, calculating contribution
points that determine contributor levels and rewards.

The platform is comprised of a Ruby on Rails backend, a Postgres database,
and a VueJS Single Page Application (SPA) frontend.

## User guide

Read the [user guide](https://contributors.gitlab.com/docs/user-guide).

## Development environment

The development environment runs in a [Dev Container](https://code.visualstudio.com/docs/devcontainers/containers).

### Prerequisites

- A container runtime. e.g. Docker, Rancher Desktop or CoLima.
- An IDE which supports Dev Containers.
  VSCode is recommended.
- `ssh-agent` configured.
  - Run `ssh-add -l` to confirm your GitLab SSH key is configured.
  - Run `ssh-add --apple-use-keychain ~/.ssh/id_ed25519` to add a key to `ssh-agent`.

### Set up environment variables

- Copy and rename `docker/.env.docker.example` to `docker/.env.docker`.
- Optionally [create a personal OAuth app](#create-personal-oauth-app-to-login-optional)
  and update `.env.docker` file.

### Run

1. Open the repository in VSCode.
1. If prompted, select **install the recommended extensions**.
1. When prompted, select **Reopen folder to develop inside a container**  
   or select **Dev Containers: Reopen in Container** in the Command Palette.

The app is available on [http://localhost:3030](http://localhost:3030).

To run rails console:

```bash
rails c
```

To seed the database, run:

```bash
rake data:seed
```

### Create personal OAuth app to login (optional)

Use the following instructions to [create a GitLab OAuth application](https://docs.gitlab.com/ee/integration/oauth_provider.html#create-a-user-owned-application).
Set the redirect URI to: `http://localhost:3030/oauth_callback`
and enable the `api` scope.
Update `APP_ID` and `APP_SECRET` in `./docker/.env.docker`.

## Tests

### Ruby

```bash
bundle exec rspec
```

### VueJS

```shell
npm run test
```

## Lint

### Ruby

```shell
rubocop
```

Rules can be fully disabled in `contributors/.rubocop.yml`.
Or disabled in individual files by running:

```shell
rubocop --require ./.rubocop/formatter/todo_formatter --format RuboCop::Formatter::TodoFormatter
```

### VueJS

```shell
npm run lint
```

#### Import Paths

- Use relative paths to import from the same or child folders.
- Use absolute paths to import from parent folders.
- Use the `@images` alias to import images.

```javascript
// Contributors app folder structure
// ├── app
// │   ├── assets
// |       └── images
// │           ├── logo.png
// |           └── sample.png
// │   └── javascript
// |       ├── common
// │       |   ├── Filter.vue
// │       |   └── Navigation.vue
// |       └── pages
// │           ├── HomePage.vue
// │           ├── UserPage.vue
// |           └── dashboard
// |               └── Component.vue

// To import methods into pages/UserPage.vue

// Bad
import { filter } from "../common/Filter.vue"; // parent folder
import { home } from "~/pages/HomePage.vue"; // same folder
import { component } from "~/pages/dashboard/Component.vue"; // child folder
import { logo } from "../../assets/images/logo.png"; // images

// Good
import { filter } from "~/common/Filter.vue"; // parent folder
import { home } from "./HomePage.vue"; // same folder
import { component } from "./dashboard/Component.vue"; // child folder
import { logo } from "@images/logo.png"; // images
```

## Commit messages

Use a changelog trailer to generate release notes.

- `Changelog: Feature`.
- `Changelog: Bug`.
- `Changelog: Maintenance`.

You can preview the changelog for unreleased changes using
[the changelog API](https://gitlab.com/api/v4/projects/56614952/repository/changelog?version=v999.999.999).

## Deployment

A docker container is built in CI and deployed to Google Cloud using
[Runway](https://runway-docs-4jdf82.runway.gitlab.net/).

- [Staging](https://contributors-xpcoll.staging.runway.gitlab.net/).
- [Production](https://contributors.gitlab.com/).

The release/deployment process is initiated when tagging a commit.

## Automation (scheduled pipelines)

Scheduled pipelines are used for automation in the platform using rake tasks.

### Data collection

For performance reasons data is aggregated in two materialized views:

- `view_activity_summaries`: all activity, updated daily.
- `view_recent_activity_summaries`: activity in the last 90 days, updated every
  5 minutes.

The materialized views are used to generate leaderboards and for automations.

#### GitLab

[`grab_data_service.rb`](https://gitlab.com/gitlab-org/developer-relations/contributor-success/contributors-gitlab-com/-/blob/main/contributors/app/services/grab_data_service.rb)

- GraphQL queries are used to pull issues and merge requests (based on update date),
  along with all related commits, notes and users.
- Public APIs are used to ensure only public data is imported.
- The first run each day uses yesterday's date to ensure nothing is missed.

[`user_update_service.rb`](https://gitlab.com/gitlab-org/developer-relations/contributor-success/contributors-gitlab-com/-/blob/main/contributors/app/services/user_update_service.rb)

- A GraphQL query checks whether a user has updated in GitLab,
  and updates the associated platform user.
- Any users who have been blocked in GitLab are deleted from the platform.

[`grab_team_members_service.rb`](https://gitlab.com/gitlab-org/developer-relations/contributor-success/contributors-gitlab-com/-/blob/main/contributors/app/services/users/grab_team_members_service.rb)

- Team member JSON data is imported from [Reviewer Roulette](https://gitlab-org.gitlab.io/gitlab-roulette).
- This includes flags to identify merge request coaches and availability.

[`grab_community_members_service.rb`](https://gitlab.com/gitlab-org/developer-relations/contributor-success/contributors-gitlab-com/-/blob/main/contributors/app/services/users/grab_community_members_service.rb)

- A GraphQL query retrieves a list of approved members from [@gitlab-community/community-members](https://gitlab.com/gitlab-community/community-members).

Note: This rake task is triggered by a webhook in [@gitlab-community/community-members](https://gitlab.com/gitlab-community/community-members)
to ensure the data is always up to date.

#### Forum (Discourse)

[`discourse_service.rb`](https://gitlab.com/gitlab-org/developer-relations/contributor-success/contributors-gitlab-com/-/blob/main/contributors/app/services/discourse_service.rb)

- A REST API is used to pull forum posts based on date.
- Only forum posts for users who exist in the platform,
  and who have entered their GitLab username into Discourse are imported.
- Public APIs are used to ensure only public data is imported.

#### Discord

[`discord_service.rb`](https://gitlab.com/gitlab-org/developer-relations/contributor-success/contributors-gitlab-com/-/blob/main/contributors/app/services/discord_service.rb)

- A REST API is used to pull messages in all channels defined in [`contributors/app/models/discord_message.rb`](https://gitlab.com/gitlab-org/developer-relations/contributor-success/contributors-gitlab-com/-/blob/main/contributors/app/models/discord_message.rb)
  based on original thread date.
- Only messages for users who exist in the platform,
  and who have entered their Discord user id into GitLab are imported.
- Public APIs are used to ensure only public data is imported (a token is required).

### Contributor rewards

#### First contribution

[`first_contribution_service.rb`](https://gitlab.com/gitlab-org/developer-relations/contributor-success/contributors-gitlab-com/-/blob/main/contributors/app/services/credits/first_contribution_service.rb)

- Award credits for contributors after merging their first MR.

#### Level up

[`level_up_service.rb`](https://gitlab.com/gitlab-org/developer-relations/contributor-success/contributors-gitlab-com/-/blob/main/contributors/app/services/credits/level_up_service.rb)

- Award credits and achievements for contributors after moving up contributor level.
- Called immediately after each data import from GitLab.

## Static content (docs)

Static content (Markdown) is placed in [`/contributors/public/docs`](https://gitlab.com/gitlab-org/developer-relations/contributor-success/contributors-gitlab-com/-/blob/main/contributors/public/docs)
and rendered by visiting `/docs/markdown-file-name` (without the `.md` extension).
For example: [Levels and Points](https://contributors.gitlab.com/docs/user-guide#contributor-levels).

A custom Vue component [`DocumentationPage`](contributors/app/javascript/pages/DocumentationPage.vue)
retrieves the Markdown and renders it using [marked](https://github.com/markedjs/marked).

A [custom marked renderer](https://gitlab.com/gitlab-org/developer-relations/contributor-success/contributors-gitlab-com/-/blob/main/contributors/app/javascript/pages/documentation-page/markedRenderer.js)
updates image paths and relative links to work seamlessly in the platform.

The `BLink` Vue component can be used to link to documentation pages
(by passing the `page` param).
For example:

```html
<BLink :to="{ name: 'documentation', params: { page: 'user-guide' } }">
  User guide
</BLink>
```

## Connecting to the production/staging database

### Google Console

[Google's SQL Cloud Studio](https://console.cloud.google.com/sql/instances/contributors-sandbox/studio?project=mktg-contrib-rewards-44102dfe)
can be used with the `contributors-sandbox GCP PostgreSQL` credentials from 1Password.

### Google Cloud Proxy

- Download and install [Google Cloud SQL Proxy](https://cloud.google.com/sql/docs/mysql/connect-instance-auth-proxy#install-proxy).
- Download the service account `mktg-contrib-rewards-44102dfe-085c3badea73` credentials
  from 1Password.
- Launch Cloud SQL Proxy:

```bash
 ./cloud-sql-proxy mktg-contrib-rewards-44102dfe:us-central1:contributors-sandbox --credentials-file ~/Downloads/mktg-contrib-rewards-44102dfe-085c3badea73.json --auto-iam-authn &
```

You can now execute rake tasks for example:

```bash
RAILS_ENV=production DB_HOSTNAME=127.0.0.1 POSTGRES_USER=contributors-dot@mktg-contrib-rewards-44102dfe.iam rake data:grab
```

### Restoring the production database locally

```bash
pg_dump -Fc -U contributors-dot@mktg-contrib-rewards-44102dfe.iam -h 127.0.0.1 contributors > backup.dump
pg_restore --clean --no-acl --no-owner -h db -U postgres -d contributors_development backup.dump
```

## Troubleshooting

If you are experiencing issues, try recreating the Dev Container.
Use the Docker Compose command line (usually `docker compose` or `docker-compose`
depending on your container runtime).

```shell
docker-compose down -v
docker-compose build
docker-compose up
```

Then, reopen the Dev Container.

## Contributing

Yes please!
