#!/bin/bash -e

# If running the rails server then create or migrate existing database
if [ "${1}" == "bin/rails" ] && [ "${2}" == "s" ]; then
  ./bin/rails db:prepare
fi

# If running vite then install dependencies
if [ "${1}" == "bin/vite" ] && [ "${2}" == "dev" ]; then
  npm i
fi

if [ -f tmp/pids/server.pid ]; then
  rm tmp/pids/server.pid
fi

exec "${@}"
