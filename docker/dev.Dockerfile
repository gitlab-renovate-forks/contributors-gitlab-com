FROM ruby:3.4.2
RUN curl -fsSL https://deb.nodesource.com/setup_20.x | bash -
RUN echo "deb https://apt.postgresql.org/pub/repos/apt bookworm-pgdg main" > /etc/apt/sources.list.d/pgdg.list
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client-17
RUN git config --global --add safe.directory /repo
WORKDIR /app
COPY contributors/Gemfile contributors/Gemfile.lock contributors/package.json contributors/package-lock.json /app/
RUN bundle install

COPY ./docker/entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3030

CMD ["rails", "s", "-b", "0.0.0.0"]
